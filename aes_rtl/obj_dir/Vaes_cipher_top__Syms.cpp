// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Symbol table implementation internals

#include "Vaes_cipher_top__Syms.h"
#include "Vaes_cipher_top.h"
#include "Vaes_cipher_top_aes_sbox.h"

// FUNCTIONS
Vaes_cipher_top__Syms::Vaes_cipher_top__Syms(Vaes_cipher_top* topp, const char* namep)
	// Setup locals
	: __Vm_namep(namep)
	, __Vm_activity(false)
	, __Vm_didInit(false)
	// Setup submodule names
	, TOP__v__DOT__u0__DOT__u0       (Verilated::catName(topp->name(),"v.u0.u0"))
	, TOP__v__DOT__u0__DOT__u1       (Verilated::catName(topp->name(),"v.u0.u1"))
	, TOP__v__DOT__u0__DOT__u2       (Verilated::catName(topp->name(),"v.u0.u2"))
	, TOP__v__DOT__u0__DOT__u3       (Verilated::catName(topp->name(),"v.u0.u3"))
	, TOP__v__DOT__us00              (Verilated::catName(topp->name(),"v.us00"))
	, TOP__v__DOT__us01              (Verilated::catName(topp->name(),"v.us01"))
	, TOP__v__DOT__us02              (Verilated::catName(topp->name(),"v.us02"))
	, TOP__v__DOT__us03              (Verilated::catName(topp->name(),"v.us03"))
	, TOP__v__DOT__us10              (Verilated::catName(topp->name(),"v.us10"))
	, TOP__v__DOT__us11              (Verilated::catName(topp->name(),"v.us11"))
	, TOP__v__DOT__us12              (Verilated::catName(topp->name(),"v.us12"))
	, TOP__v__DOT__us13              (Verilated::catName(topp->name(),"v.us13"))
	, TOP__v__DOT__us20              (Verilated::catName(topp->name(),"v.us20"))
	, TOP__v__DOT__us21              (Verilated::catName(topp->name(),"v.us21"))
	, TOP__v__DOT__us22              (Verilated::catName(topp->name(),"v.us22"))
	, TOP__v__DOT__us23              (Verilated::catName(topp->name(),"v.us23"))
	, TOP__v__DOT__us30              (Verilated::catName(topp->name(),"v.us30"))
	, TOP__v__DOT__us31              (Verilated::catName(topp->name(),"v.us31"))
	, TOP__v__DOT__us32              (Verilated::catName(topp->name(),"v.us32"))
	, TOP__v__DOT__us33              (Verilated::catName(topp->name(),"v.us33"))
{
    // Pointer to top level
    TOPp = topp;
    // Setup each module's pointers to their submodules
    TOPp->__PVT__v__DOT__u0__DOT__u0  = &TOP__v__DOT__u0__DOT__u0;
    TOPp->__PVT__v__DOT__u0__DOT__u1  = &TOP__v__DOT__u0__DOT__u1;
    TOPp->__PVT__v__DOT__u0__DOT__u2  = &TOP__v__DOT__u0__DOT__u2;
    TOPp->__PVT__v__DOT__u0__DOT__u3  = &TOP__v__DOT__u0__DOT__u3;
    TOPp->__PVT__v__DOT__us00       = &TOP__v__DOT__us00;
    TOPp->__PVT__v__DOT__us01       = &TOP__v__DOT__us01;
    TOPp->__PVT__v__DOT__us02       = &TOP__v__DOT__us02;
    TOPp->__PVT__v__DOT__us03       = &TOP__v__DOT__us03;
    TOPp->__PVT__v__DOT__us10       = &TOP__v__DOT__us10;
    TOPp->__PVT__v__DOT__us11       = &TOP__v__DOT__us11;
    TOPp->__PVT__v__DOT__us12       = &TOP__v__DOT__us12;
    TOPp->__PVT__v__DOT__us13       = &TOP__v__DOT__us13;
    TOPp->__PVT__v__DOT__us20       = &TOP__v__DOT__us20;
    TOPp->__PVT__v__DOT__us21       = &TOP__v__DOT__us21;
    TOPp->__PVT__v__DOT__us22       = &TOP__v__DOT__us22;
    TOPp->__PVT__v__DOT__us23       = &TOP__v__DOT__us23;
    TOPp->__PVT__v__DOT__us30       = &TOP__v__DOT__us30;
    TOPp->__PVT__v__DOT__us31       = &TOP__v__DOT__us31;
    TOPp->__PVT__v__DOT__us32       = &TOP__v__DOT__us32;
    TOPp->__PVT__v__DOT__us33       = &TOP__v__DOT__us33;
    // Setup each module's pointer back to symbol table (for public functions)
    TOPp->__Vconfigure(this, true);
    TOP__v__DOT__u0__DOT__u0.__Vconfigure(this, true);
    TOP__v__DOT__u0__DOT__u1.__Vconfigure(this, false);
    TOP__v__DOT__u0__DOT__u2.__Vconfigure(this, false);
    TOP__v__DOT__u0__DOT__u3.__Vconfigure(this, false);
    TOP__v__DOT__us00.__Vconfigure(this, false);
    TOP__v__DOT__us01.__Vconfigure(this, false);
    TOP__v__DOT__us02.__Vconfigure(this, false);
    TOP__v__DOT__us03.__Vconfigure(this, false);
    TOP__v__DOT__us10.__Vconfigure(this, false);
    TOP__v__DOT__us11.__Vconfigure(this, false);
    TOP__v__DOT__us12.__Vconfigure(this, false);
    TOP__v__DOT__us13.__Vconfigure(this, false);
    TOP__v__DOT__us20.__Vconfigure(this, false);
    TOP__v__DOT__us21.__Vconfigure(this, false);
    TOP__v__DOT__us22.__Vconfigure(this, false);
    TOP__v__DOT__us23.__Vconfigure(this, false);
    TOP__v__DOT__us30.__Vconfigure(this, false);
    TOP__v__DOT__us31.__Vconfigure(this, false);
    TOP__v__DOT__us32.__Vconfigure(this, false);
    TOP__v__DOT__us33.__Vconfigure(this, false);
    // Setup scope names
}

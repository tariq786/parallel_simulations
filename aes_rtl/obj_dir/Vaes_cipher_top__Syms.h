// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Symbol table internal header
//
// Internal details; most calling programs do not need this header

#ifndef _Vaes_cipher_top__Syms_H_
#define _Vaes_cipher_top__Syms_H_

#include "verilated.h"

// INCLUDE MODULE CLASSES
#include "Vaes_cipher_top.h"
#include "Vaes_cipher_top_aes_sbox.h"

// SYMS CLASS
class Vaes_cipher_top__Syms : public VerilatedSyms {
  public:
    
    // LOCAL STATE
    const char* __Vm_namep;
    bool	__Vm_activity;		///< Used by trace routines to determine change occurred
    bool	__Vm_didInit;
    //char	__VpadToAlign10[6];
    
    // SUBCELL STATE
    Vaes_cipher_top*               TOPp;
    Vaes_cipher_top_aes_sbox       TOP__v__DOT__u0__DOT__u0;
    Vaes_cipher_top_aes_sbox       TOP__v__DOT__u0__DOT__u1;
    Vaes_cipher_top_aes_sbox       TOP__v__DOT__u0__DOT__u2;
    Vaes_cipher_top_aes_sbox       TOP__v__DOT__u0__DOT__u3;
    Vaes_cipher_top_aes_sbox       TOP__v__DOT__us00;
    Vaes_cipher_top_aes_sbox       TOP__v__DOT__us01;
    Vaes_cipher_top_aes_sbox       TOP__v__DOT__us02;
    Vaes_cipher_top_aes_sbox       TOP__v__DOT__us03;
    Vaes_cipher_top_aes_sbox       TOP__v__DOT__us10;
    Vaes_cipher_top_aes_sbox       TOP__v__DOT__us11;
    Vaes_cipher_top_aes_sbox       TOP__v__DOT__us12;
    Vaes_cipher_top_aes_sbox       TOP__v__DOT__us13;
    Vaes_cipher_top_aes_sbox       TOP__v__DOT__us20;
    Vaes_cipher_top_aes_sbox       TOP__v__DOT__us21;
    Vaes_cipher_top_aes_sbox       TOP__v__DOT__us22;
    Vaes_cipher_top_aes_sbox       TOP__v__DOT__us23;
    Vaes_cipher_top_aes_sbox       TOP__v__DOT__us30;
    Vaes_cipher_top_aes_sbox       TOP__v__DOT__us31;
    Vaes_cipher_top_aes_sbox       TOP__v__DOT__us32;
    Vaes_cipher_top_aes_sbox       TOP__v__DOT__us33;
    
    // COVERAGE
    
    // SCOPE NAMES
    
    // CREATORS
    Vaes_cipher_top__Syms(Vaes_cipher_top* topp, const char* namep);
    ~Vaes_cipher_top__Syms() {};
    
    // METHODS
    inline const char* name() { return __Vm_namep; }
    inline bool getClearActivity() { bool r=__Vm_activity; __Vm_activity=false; return r;}
    
} VL_ATTR_ALIGNED(64);

#endif  /*guard*/

// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_vcd_c.h"
#include "Vaes_cipher_top__Syms.h"


//======================

void Vaes_cipher_top::trace (VerilatedVcdC* tfp, int, int) {
    tfp->spTrace()->addCallback (&Vaes_cipher_top::traceInit, &Vaes_cipher_top::traceFull, &Vaes_cipher_top::traceChg, this);
}
void Vaes_cipher_top::traceInit(VerilatedVcd* vcdp, void* userthis, uint32_t code) {
    // Callback from vcd->open()
    Vaes_cipher_top* t=(Vaes_cipher_top*)userthis;
    Vaes_cipher_top__Syms* __restrict vlSymsp = t->__VlSymsp; // Setup global symbol table
    if (!Verilated::calcUnusedSigs()) vl_fatal(__FILE__,__LINE__,__FILE__,"Turning on wave traces requires Verilated::traceEverOn(true) call before time 0.");
    vcdp->scopeEscape(' ');
    t->traceInitThis (vlSymsp, vcdp, code);
    vcdp->scopeEscape('.');
}
void Vaes_cipher_top::traceFull(VerilatedVcd* vcdp, void* userthis, uint32_t code) {
    // Callback from vcd->dump()
    Vaes_cipher_top* t=(Vaes_cipher_top*)userthis;
    Vaes_cipher_top__Syms* __restrict vlSymsp = t->__VlSymsp; // Setup global symbol table
    t->traceFullThis (vlSymsp, vcdp, code);
}

//======================


void Vaes_cipher_top::traceInitThis(Vaes_cipher_top__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    vcdp->module(vlSymsp->name()); // Setup signal names
    // Body
    {
	vlTOPp->traceInitThis__1(vlSymsp, vcdp, code);
    }
}

void Vaes_cipher_top::traceFullThis(Vaes_cipher_top__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vlTOPp->traceFullThis__1(vlSymsp, vcdp, code);
    }
    // Final
    vlTOPp->__Vm_traceActivity = 0;
}

void Vaes_cipher_top::traceInitThis__1(Vaes_cipher_top__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vcdp->declBit  (c+92,"clk",-1);
	vcdp->declBit  (c+93,"rst",-1);
	vcdp->declBit  (c+94,"ld",-1);
	vcdp->declBit  (c+95,"done",-1);
	vcdp->declArray(c+96,"key",-1,127,0);
	vcdp->declArray(c+100,"text_in",-1,127,0);
	vcdp->declArray(c+104,"text_out",-1,127,0);
	vcdp->declBit  (c+92,"v clk",-1);
	vcdp->declBit  (c+93,"v rst",-1);
	vcdp->declBit  (c+94,"v ld",-1);
	vcdp->declBit  (c+95,"v done",-1);
	vcdp->declArray(c+96,"v key",-1,127,0);
	vcdp->declArray(c+100,"v text_in",-1,127,0);
	vcdp->declArray(c+104,"v text_out",-1,127,0);
	vcdp->declBus  (c+1,"v w0",-1,31,0);
	vcdp->declBus  (c+2,"v w1",-1,31,0);
	vcdp->declBus  (c+3,"v w2",-1,31,0);
	vcdp->declBus  (c+4,"v w3",-1,31,0);
	vcdp->declArray(c+63,"v text_in_r",-1,127,0);
	vcdp->declBus  (c+67,"v sa00",-1,7,0);
	vcdp->declBus  (c+68,"v sa01",-1,7,0);
	vcdp->declBus  (c+69,"v sa02",-1,7,0);
	vcdp->declBus  (c+70,"v sa03",-1,7,0);
	vcdp->declBus  (c+71,"v sa10",-1,7,0);
	vcdp->declBus  (c+72,"v sa11",-1,7,0);
	vcdp->declBus  (c+73,"v sa12",-1,7,0);
	vcdp->declBus  (c+74,"v sa13",-1,7,0);
	vcdp->declBus  (c+75,"v sa20",-1,7,0);
	vcdp->declBus  (c+76,"v sa21",-1,7,0);
	vcdp->declBus  (c+77,"v sa22",-1,7,0);
	vcdp->declBus  (c+78,"v sa23",-1,7,0);
	vcdp->declBus  (c+79,"v sa30",-1,7,0);
	vcdp->declBus  (c+80,"v sa31",-1,7,0);
	vcdp->declBus  (c+81,"v sa32",-1,7,0);
	vcdp->declBus  (c+82,"v sa33",-1,7,0);
	vcdp->declBus  (c+5,"v sa00_next",-1,7,0);
	vcdp->declBus  (c+6,"v sa01_next",-1,7,0);
	vcdp->declBus  (c+7,"v sa02_next",-1,7,0);
	vcdp->declBus  (c+8,"v sa03_next",-1,7,0);
	vcdp->declBus  (c+9,"v sa10_next",-1,7,0);
	vcdp->declBus  (c+10,"v sa11_next",-1,7,0);
	vcdp->declBus  (c+11,"v sa12_next",-1,7,0);
	vcdp->declBus  (c+12,"v sa13_next",-1,7,0);
	vcdp->declBus  (c+13,"v sa20_next",-1,7,0);
	vcdp->declBus  (c+14,"v sa21_next",-1,7,0);
	vcdp->declBus  (c+15,"v sa22_next",-1,7,0);
	vcdp->declBus  (c+16,"v sa23_next",-1,7,0);
	vcdp->declBus  (c+17,"v sa30_next",-1,7,0);
	vcdp->declBus  (c+18,"v sa31_next",-1,7,0);
	vcdp->declBus  (c+19,"v sa32_next",-1,7,0);
	vcdp->declBus  (c+20,"v sa33_next",-1,7,0);
	vcdp->declBus  (c+21,"v sa00_sub",-1,7,0);
	vcdp->declBus  (c+22,"v sa01_sub",-1,7,0);
	vcdp->declBus  (c+23,"v sa02_sub",-1,7,0);
	vcdp->declBus  (c+24,"v sa03_sub",-1,7,0);
	vcdp->declBus  (c+25,"v sa10_sub",-1,7,0);
	vcdp->declBus  (c+26,"v sa11_sub",-1,7,0);
	vcdp->declBus  (c+27,"v sa12_sub",-1,7,0);
	vcdp->declBus  (c+28,"v sa13_sub",-1,7,0);
	vcdp->declBus  (c+29,"v sa20_sub",-1,7,0);
	vcdp->declBus  (c+30,"v sa21_sub",-1,7,0);
	vcdp->declBus  (c+31,"v sa22_sub",-1,7,0);
	vcdp->declBus  (c+32,"v sa23_sub",-1,7,0);
	vcdp->declBus  (c+33,"v sa30_sub",-1,7,0);
	vcdp->declBus  (c+34,"v sa31_sub",-1,7,0);
	vcdp->declBus  (c+35,"v sa32_sub",-1,7,0);
	vcdp->declBus  (c+36,"v sa33_sub",-1,7,0);
	vcdp->declBus  (c+21,"v sa00_sr",-1,7,0);
	vcdp->declBus  (c+22,"v sa01_sr",-1,7,0);
	vcdp->declBus  (c+23,"v sa02_sr",-1,7,0);
	vcdp->declBus  (c+24,"v sa03_sr",-1,7,0);
	vcdp->declBus  (c+26,"v sa10_sr",-1,7,0);
	vcdp->declBus  (c+27,"v sa11_sr",-1,7,0);
	vcdp->declBus  (c+28,"v sa12_sr",-1,7,0);
	vcdp->declBus  (c+25,"v sa13_sr",-1,7,0);
	vcdp->declBus  (c+31,"v sa20_sr",-1,7,0);
	vcdp->declBus  (c+32,"v sa21_sr",-1,7,0);
	vcdp->declBus  (c+29,"v sa22_sr",-1,7,0);
	vcdp->declBus  (c+30,"v sa23_sr",-1,7,0);
	vcdp->declBus  (c+36,"v sa30_sr",-1,7,0);
	vcdp->declBus  (c+33,"v sa31_sr",-1,7,0);
	vcdp->declBus  (c+34,"v sa32_sr",-1,7,0);
	vcdp->declBus  (c+35,"v sa33_sr",-1,7,0);
	vcdp->declBus  (c+37,"v sa00_mc",-1,7,0);
	vcdp->declBus  (c+38,"v sa01_mc",-1,7,0);
	vcdp->declBus  (c+39,"v sa02_mc",-1,7,0);
	vcdp->declBus  (c+40,"v sa03_mc",-1,7,0);
	vcdp->declBus  (c+41,"v sa10_mc",-1,7,0);
	vcdp->declBus  (c+42,"v sa11_mc",-1,7,0);
	vcdp->declBus  (c+43,"v sa12_mc",-1,7,0);
	vcdp->declBus  (c+44,"v sa13_mc",-1,7,0);
	vcdp->declBus  (c+45,"v sa20_mc",-1,7,0);
	vcdp->declBus  (c+46,"v sa21_mc",-1,7,0);
	vcdp->declBus  (c+47,"v sa22_mc",-1,7,0);
	vcdp->declBus  (c+48,"v sa23_mc",-1,7,0);
	vcdp->declBus  (c+49,"v sa30_mc",-1,7,0);
	vcdp->declBus  (c+50,"v sa31_mc",-1,7,0);
	vcdp->declBus  (c+51,"v sa32_mc",-1,7,0);
	vcdp->declBus  (c+52,"v sa33_mc",-1,7,0);
	vcdp->declBit  (c+83,"v ld_r",-1);
	vcdp->declBus  (c+84,"v dcnt",-1,3,0);
	vcdp->declBit  (c+92,"v u0 clk",-1);
	vcdp->declBit  (c+94,"v u0 kld",-1);
	vcdp->declArray(c+96,"v u0 key",-1,127,0);
	vcdp->declBus  (c+1,"v u0 wo_0",-1,31,0);
	vcdp->declBus  (c+2,"v u0 wo_1",-1,31,0);
	vcdp->declBus  (c+3,"v u0 wo_2",-1,31,0);
	vcdp->declBus  (c+4,"v u0 wo_3",-1,31,0);
	{int i; for (i=0; i<4; i++) {
		vcdp->declBus  (c+85+i*1,"v u0 w",(i+0),31,0);}}
	vcdp->declBus  (c+53,"v u0 tmp_w",-1,31,0);
	vcdp->declBus  (c+54,"v u0 subword",-1,31,0);
	vcdp->declBus  (c+89,"v u0 rcon",-1,31,0);
	vcdp->declBit  (c+92,"v u0 r0 clk",-1);
	vcdp->declBit  (c+94,"v u0 r0 kld",-1);
	vcdp->declBus  (c+89,"v u0 r0 out",-1,31,0);
	vcdp->declBus  (c+90,"v u0 r0 rcnt",-1,3,0);
	vcdp->declBus  (c+91,"v u0 r0 rcnt_next",-1,3,0);
	vcdp->declBus  (c+67,"v us00 a",-1,7,0);
	vcdp->declBus  (c+21,"v us00 d",-1,7,0);
	vcdp->declBus  (c+68,"v us01 a",-1,7,0);
	vcdp->declBus  (c+22,"v us01 d",-1,7,0);
	vcdp->declBus  (c+69,"v us02 a",-1,7,0);
	vcdp->declBus  (c+23,"v us02 d",-1,7,0);
	vcdp->declBus  (c+70,"v us03 a",-1,7,0);
	vcdp->declBus  (c+24,"v us03 d",-1,7,0);
	vcdp->declBus  (c+71,"v us10 a",-1,7,0);
	vcdp->declBus  (c+25,"v us10 d",-1,7,0);
	vcdp->declBus  (c+72,"v us11 a",-1,7,0);
	vcdp->declBus  (c+26,"v us11 d",-1,7,0);
	vcdp->declBus  (c+73,"v us12 a",-1,7,0);
	vcdp->declBus  (c+27,"v us12 d",-1,7,0);
	vcdp->declBus  (c+74,"v us13 a",-1,7,0);
	vcdp->declBus  (c+28,"v us13 d",-1,7,0);
	vcdp->declBus  (c+75,"v us20 a",-1,7,0);
	vcdp->declBus  (c+29,"v us20 d",-1,7,0);
	vcdp->declBus  (c+76,"v us21 a",-1,7,0);
	vcdp->declBus  (c+30,"v us21 d",-1,7,0);
	vcdp->declBus  (c+77,"v us22 a",-1,7,0);
	vcdp->declBus  (c+31,"v us22 d",-1,7,0);
	vcdp->declBus  (c+78,"v us23 a",-1,7,0);
	vcdp->declBus  (c+32,"v us23 d",-1,7,0);
	vcdp->declBus  (c+79,"v us30 a",-1,7,0);
	vcdp->declBus  (c+33,"v us30 d",-1,7,0);
	vcdp->declBus  (c+80,"v us31 a",-1,7,0);
	vcdp->declBus  (c+34,"v us31 d",-1,7,0);
	vcdp->declBus  (c+81,"v us32 a",-1,7,0);
	vcdp->declBus  (c+35,"v us32 d",-1,7,0);
	vcdp->declBus  (c+82,"v us33 a",-1,7,0);
	vcdp->declBus  (c+36,"v us33 d",-1,7,0);
	vcdp->declBus  (c+55,"v u0 u0 a",-1,7,0);
	vcdp->declBus  (c+56,"v u0 u0 d",-1,7,0);
	vcdp->declBus  (c+57,"v u0 u1 a",-1,7,0);
	vcdp->declBus  (c+58,"v u0 u1 d",-1,7,0);
	vcdp->declBus  (c+59,"v u0 u2 a",-1,7,0);
	vcdp->declBus  (c+60,"v u0 u2 d",-1,7,0);
	vcdp->declBus  (c+61,"v u0 u3 a",-1,7,0);
	vcdp->declBus  (c+62,"v u0 u3 d",-1,7,0);
    }
}

void Vaes_cipher_top::traceFullThis__1(Vaes_cipher_top__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vcdp->fullBus  (c+1,(vlTOPp->v__DOT__w0),32);
	vcdp->fullBus  (c+2,(vlTOPp->v__DOT__w1),32);
	vcdp->fullBus  (c+3,(vlTOPp->v__DOT__w2),32);
	vcdp->fullBus  (c+4,(vlTOPp->v__DOT__w3),32);
	vcdp->fullBus  (c+5,((0xff & ((IData)(vlTOPp->v__DOT__sa00_mc) 
				      ^ (vlTOPp->v__DOT__w0 
					 >> 0x18)))),8);
	vcdp->fullBus  (c+6,((0xff & ((IData)(vlTOPp->v__DOT__sa01_mc) 
				      ^ (vlTOPp->v__DOT__w1 
					 >> 0x18)))),8);
	vcdp->fullBus  (c+7,((0xff & ((IData)(vlTOPp->v__DOT__sa02_mc) 
				      ^ (vlTOPp->v__DOT__w2 
					 >> 0x18)))),8);
	vcdp->fullBus  (c+8,((0xff & ((IData)(vlTOPp->v__DOT__sa03_mc) 
				      ^ (vlTOPp->v__DOT__w3 
					 >> 0x18)))),8);
	vcdp->fullBus  (c+9,((0xff & ((IData)(vlTOPp->v__DOT__sa10_mc) 
				      ^ (vlTOPp->v__DOT__w0 
					 >> 0x10)))),8);
	vcdp->fullBus  (c+10,((0xff & ((IData)(vlTOPp->v__DOT__sa11_mc) 
				       ^ (vlTOPp->v__DOT__w1 
					  >> 0x10)))),8);
	vcdp->fullBus  (c+11,((0xff & ((IData)(vlTOPp->v__DOT__sa12_mc) 
				       ^ (vlTOPp->v__DOT__w2 
					  >> 0x10)))),8);
	vcdp->fullBus  (c+12,((0xff & ((IData)(vlTOPp->v__DOT__sa13_mc) 
				       ^ (vlTOPp->v__DOT__w3 
					  >> 0x10)))),8);
	vcdp->fullBus  (c+13,((0xff & ((IData)(vlTOPp->v__DOT__sa20_mc) 
				       ^ (vlTOPp->v__DOT__w0 
					  >> 8)))),8);
	vcdp->fullBus  (c+14,((0xff & ((IData)(vlTOPp->v__DOT__sa21_mc) 
				       ^ (vlTOPp->v__DOT__w1 
					  >> 8)))),8);
	vcdp->fullBus  (c+15,((0xff & ((IData)(vlTOPp->v__DOT__sa22_mc) 
				       ^ (vlTOPp->v__DOT__w2 
					  >> 8)))),8);
	vcdp->fullBus  (c+16,((0xff & ((IData)(vlTOPp->v__DOT__sa23_mc) 
				       ^ (vlTOPp->v__DOT__w3 
					  >> 8)))),8);
	vcdp->fullBus  (c+17,((0xff & ((IData)(vlTOPp->v__DOT__sa30_mc) 
				       ^ vlTOPp->v__DOT__w0))),8);
	vcdp->fullBus  (c+18,((0xff & ((IData)(vlTOPp->v__DOT__sa31_mc) 
				       ^ vlTOPp->v__DOT__w1))),8);
	vcdp->fullBus  (c+19,((0xff & ((IData)(vlTOPp->v__DOT__sa32_mc) 
				       ^ vlTOPp->v__DOT__w2))),8);
	vcdp->fullBus  (c+20,((0xff & ((IData)(vlTOPp->v__DOT__sa33_mc) 
				       ^ vlTOPp->v__DOT__w3))),8);
	vcdp->fullBus  (c+37,(vlTOPp->v__DOT__sa00_mc),8);
	vcdp->fullBus  (c+38,(vlTOPp->v__DOT__sa01_mc),8);
	vcdp->fullBus  (c+39,(vlTOPp->v__DOT__sa02_mc),8);
	vcdp->fullBus  (c+40,(vlTOPp->v__DOT__sa03_mc),8);
	vcdp->fullBus  (c+41,(vlTOPp->v__DOT__sa10_mc),8);
	vcdp->fullBus  (c+42,(vlTOPp->v__DOT__sa11_mc),8);
	vcdp->fullBus  (c+43,(vlTOPp->v__DOT__sa12_mc),8);
	vcdp->fullBus  (c+44,(vlTOPp->v__DOT__sa13_mc),8);
	vcdp->fullBus  (c+45,(vlTOPp->v__DOT__sa20_mc),8);
	vcdp->fullBus  (c+46,(vlTOPp->v__DOT__sa21_mc),8);
	vcdp->fullBus  (c+47,(vlTOPp->v__DOT__sa22_mc),8);
	vcdp->fullBus  (c+48,(vlTOPp->v__DOT__sa23_mc),8);
	vcdp->fullBus  (c+49,(vlTOPp->v__DOT__sa30_mc),8);
	vcdp->fullBus  (c+50,(vlTOPp->v__DOT__sa31_mc),8);
	vcdp->fullBus  (c+51,(vlTOPp->v__DOT__sa32_mc),8);
	vcdp->fullBus  (c+52,(vlTOPp->v__DOT__sa33_mc),8);
	vcdp->fullBus  (c+53,(vlTOPp->v__DOT__u0__DOT__tmp_w),32);
	vcdp->fullBus  (c+54,(vlTOPp->v__DOT__u0__DOT__subword),32);
	vcdp->fullBus  (c+21,(vlSymsp->TOP__v__DOT__us00.d),8);
	vcdp->fullBus  (c+22,(vlSymsp->TOP__v__DOT__us01.d),8);
	vcdp->fullBus  (c+23,(vlSymsp->TOP__v__DOT__us02.d),8);
	vcdp->fullBus  (c+24,(vlSymsp->TOP__v__DOT__us03.d),8);
	vcdp->fullBus  (c+25,(vlSymsp->TOP__v__DOT__us10.d),8);
	vcdp->fullBus  (c+26,(vlSymsp->TOP__v__DOT__us11.d),8);
	vcdp->fullBus  (c+27,(vlSymsp->TOP__v__DOT__us12.d),8);
	vcdp->fullBus  (c+28,(vlSymsp->TOP__v__DOT__us13.d),8);
	vcdp->fullBus  (c+29,(vlSymsp->TOP__v__DOT__us20.d),8);
	vcdp->fullBus  (c+30,(vlSymsp->TOP__v__DOT__us21.d),8);
	vcdp->fullBus  (c+31,(vlSymsp->TOP__v__DOT__us22.d),8);
	vcdp->fullBus  (c+32,(vlSymsp->TOP__v__DOT__us23.d),8);
	vcdp->fullBus  (c+33,(vlSymsp->TOP__v__DOT__us30.d),8);
	vcdp->fullBus  (c+34,(vlSymsp->TOP__v__DOT__us31.d),8);
	vcdp->fullBus  (c+35,(vlSymsp->TOP__v__DOT__us32.d),8);
	vcdp->fullBus  (c+36,(vlSymsp->TOP__v__DOT__us33.d),8);
	vcdp->fullBus  (c+55,((0xff & (vlTOPp->v__DOT__u0__DOT__tmp_w 
				       >> 0x10))),8);
	vcdp->fullBus  (c+56,(vlSymsp->TOP__v__DOT__u0__DOT__u0.d),8);
	vcdp->fullBus  (c+57,((0xff & (vlTOPp->v__DOT__u0__DOT__tmp_w 
				       >> 8))),8);
	vcdp->fullBus  (c+58,(vlSymsp->TOP__v__DOT__u0__DOT__u1.d),8);
	vcdp->fullBus  (c+59,((0xff & vlTOPp->v__DOT__u0__DOT__tmp_w)),8);
	vcdp->fullBus  (c+60,(vlSymsp->TOP__v__DOT__u0__DOT__u2.d),8);
	vcdp->fullBus  (c+61,((0xff & (vlTOPp->v__DOT__u0__DOT__tmp_w 
				       >> 0x18))),8);
	vcdp->fullBus  (c+62,(vlSymsp->TOP__v__DOT__u0__DOT__u3.d),8);
	vcdp->fullArray(c+63,(vlTOPp->v__DOT__text_in_r),128);
	vcdp->fullBus  (c+67,(vlTOPp->v__DOT__sa00),8);
	vcdp->fullBus  (c+68,(vlTOPp->v__DOT__sa01),8);
	vcdp->fullBus  (c+69,(vlTOPp->v__DOT__sa02),8);
	vcdp->fullBus  (c+70,(vlTOPp->v__DOT__sa03),8);
	vcdp->fullBus  (c+71,(vlTOPp->v__DOT__sa10),8);
	vcdp->fullBus  (c+72,(vlTOPp->v__DOT__sa11),8);
	vcdp->fullBus  (c+73,(vlTOPp->v__DOT__sa12),8);
	vcdp->fullBus  (c+74,(vlTOPp->v__DOT__sa13),8);
	vcdp->fullBus  (c+75,(vlTOPp->v__DOT__sa20),8);
	vcdp->fullBus  (c+76,(vlTOPp->v__DOT__sa21),8);
	vcdp->fullBus  (c+77,(vlTOPp->v__DOT__sa22),8);
	vcdp->fullBus  (c+78,(vlTOPp->v__DOT__sa23),8);
	vcdp->fullBus  (c+79,(vlTOPp->v__DOT__sa30),8);
	vcdp->fullBus  (c+80,(vlTOPp->v__DOT__sa31),8);
	vcdp->fullBus  (c+81,(vlTOPp->v__DOT__sa32),8);
	vcdp->fullBus  (c+82,(vlTOPp->v__DOT__sa33),8);
	vcdp->fullBit  (c+83,(vlTOPp->v__DOT__ld_r));
	vcdp->fullBus  (c+84,(vlTOPp->v__DOT__dcnt),4);
	vcdp->fullBus  (c+85,(vlTOPp->v__DOT__u0__DOT__w[0]),32);
	vcdp->fullBus  (c+86,(vlTOPp->v__DOT__u0__DOT__w[1]),32);
	vcdp->fullBus  (c+87,(vlTOPp->v__DOT__u0__DOT__w[2]),32);
	vcdp->fullBus  (c+88,(vlTOPp->v__DOT__u0__DOT__w[3]),32);
	vcdp->fullBus  (c+89,(vlTOPp->v__DOT__u0__DOT__rcon),32);
	vcdp->fullBus  (c+90,(vlTOPp->v__DOT__u0__DOT__r0__DOT__rcnt),4);
	vcdp->fullBus  (c+91,((0xf & ((IData)(1) + (IData)(vlTOPp->v__DOT__u0__DOT__r0__DOT__rcnt)))),4);
	vcdp->fullBit  (c+93,(vlTOPp->rst));
	vcdp->fullBit  (c+95,(vlTOPp->done));
	vcdp->fullArray(c+100,(vlTOPp->text_in),128);
	vcdp->fullArray(c+104,(vlTOPp->text_out),128);
	vcdp->fullArray(c+96,(vlTOPp->key),128);
	vcdp->fullBit  (c+92,(vlTOPp->clk));
	vcdp->fullBit  (c+94,(vlTOPp->ld));
    }
}

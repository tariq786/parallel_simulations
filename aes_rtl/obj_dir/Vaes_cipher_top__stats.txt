Verilator Statistics Report

Information:
  Verilator 3.854 devel rev verilator_3_853-15-gecfe028 (mod)
  Arguments: -Wall --cc -F file.f --exe sim_main.cpp -Wno-lint --trace --stats ./aes/aes_cipher_top.v ./aes/aes_key_expand_128.v ./aes/aes_rcon.v ./aes/aes_sbox.v

Global Statistics:

  Assertions, PSL asserts                               0
  Assertions, SystemVerilog asserts                     0
  Assertions, cover statements                          0
  Assertions, full/parallel case                        0
  Optimizations, Cases complex                          0
  Optimizations, Cases parallelized                    21
  Optimizations, Combined CFuncs                       21
  Optimizations, Delayed shared-sets                    0
  Optimizations, Gate inputs replaced                 233
  Optimizations, Gate sigs deduped                      0
  Optimizations, Gate sigs deleted                     94
  Optimizations, Inline unsupported                     0
  Optimizations, Inlined cells                          3
  Optimizations, Lifetime assign deletions              0
  Optimizations, Lifetime assign deletions              4
  Optimizations, Lifetime constant prop                 0
  Optimizations, Lifetime constant prop                 4
  Optimizations, Lifetime postassign deletions         21
  Optimizations, Split always                         162
  Optimizations, Split always                           0
  Optimizations, Substituted temps                      0
  Optimizations, Tables created                        20
  Optimizations, Unrolled Iterations                    0
  Optimizations, Unrolled Loops                         0
  Optimizations, Vars localized                         8
  Optimizations, isolate_assignments blocks             0
  Tracing, Ignored signals                              0
  Tracing, Traced signals                             157
  Tracing, Unique changing signals                     92
  Tracing, Unique trace codes                         107
  Tracing, Unique traced signals                       92
  Tristate, Tristate resolved nets                      0
  Unknowns, variables created                           0
  Warnings, Suppressed UNUSED                           4

Stage Statistics:
  Stat                                       Link       PreOrder   Scoped     Final      Final_Fast
  --------                                   -------    -------    -------    -------    -------  

  Branch prediction,                               6          8          5          5          5
  Branch prediction, VL_UNLIKELY                                         2          2

  Instruction count, TOTAL                      4160      31874      32963      32715       7667
  Instruction count, fast                          0       2710       9248       9408       7323

  Node count, ACTIVE                                         67
  Node count, ADD                                  1          3          4          4          2
  Node count, ALWAYS                              43        114
  Node count, ALWAYSPOST                                      1
  Node count, AND                                  3        258        260       1062        551
  Node count, ARG                                 25
  Node count, ARRAYSEL                                       64         64         44         39
  Node count, ASSIGN                             272        890        891        875        472
  Node count, ASSIGNDLY                           46         44         44         27         25
  Node count, ASSIGNPOST                                     23          2          2          2
  Node count, ASSIGNPRE                                      27          2          2          2
  Node count, ASSIGNW                             42         18         18         10          5
  Node count, ATTROF                             119
  Node count, BASICDTYPE                         138         12         11         16
  Node count, BEGIN                                1
  Node count, CASE                                 2
  Node count, CASEITEM                           267
  Node count, CCALL                                           1         50         50         23
  Node count, CCAST                                                              1569        826
  Node count, CELL                                22         20         20         20
  Node count, CFILE                                                                 8
  Node count, CFUNC                                           4         58         37         23
  Node count, CHANGEDET                                                  1          1
  Node count, COMMENT                                                  115         95         79
  Node count, CONCAT                              13        256        256
  Node count, COND                                20         33         33         33         33
  Node count, CONST                              993       2690       2743       2167       1036
  Node count, CSTMT                                                      2          2
  Node count, FUNC                                 3
  Node count, FUNCREF                             13
  Node count, IF                                   6          8          7          7          5
  Node count, INITARRAY                                       1          1          1
  Node count, LOGNOT                               2
  Node count, MODULE                               4          2          2          2
  Node count, NEGATE                                                              256        128
  Node count, NEQ                                                                   3          2
  Node count, NETLIST                              1          1          1          1
  Node count, NOT                                             2          4          4          3
  Node count, OR                                                         2        161         94
  Node count, PIN                                 50
  Node count, RANGE                              117          2          2          2
  Node count, REDOR                                3          3          3
  Node count, REPLICATE                            1        256        256
  Node count, SCOPE                                          21         21         21
  Node count, SEL                                           805        833
  Node count, SELBIT                              21
  Node count, SELEXTRACT                          98
  Node count, SENITEM                             43          3
  Node count, SENTREE                             43          3
  Node count, SHIFTL                                                              376        200
  Node count, SHIFTR                                                              352        180
  Node count, SUB                                  1          1          1          1          1
  Node count, TEXT                                                       2          2
  Node count, TOPSCOPE                                        1          1          1
  Node count, TRACEDECL                                     157        157        157
  Node count, TRACEINC                                      157        184        184
  Node count, TYPETABLE                            1          1          1          1
  Node count, UNPACKARRAYDTYPE                     1          2          2          2
  Node count, VAR                                125        443        420        420          8
  Node count, VARREF                             820       3108       3066       3123       1631
  Node count, VARSCOPE                                      481        458
  Node count, WORDSEL                                                              69         69
  Node count, XOR                                 79        846        862        862        446

  Node pairs, ACTIVE_ALWAYS                                  43
  Node pairs, ACTIVE_ALWAYSPOST                               1
  Node pairs, ACTIVE_ASSIGNPRE                               23
  Node pairs, ADD_CCAST                                                             8          4
  Node pairs, ADD_CONST                            1          3          4
  Node pairs, ADD_VARREF                           1          3          4
  Node pairs, ALWAYSPOST_IF                                   1
  Node pairs, ALWAYS_ASSIGN                                  73
  Node pairs, ALWAYS_ASSIGNDLY                    38         39
  Node pairs, ALWAYS_CASE                          1
  Node pairs, ALWAYS_IF                            4          2
  Node pairs, ALWAYS_SENTREE                      43
  Node pairs, AND_ADD                                                               3          1
  Node pairs, AND_AND                              1          1          1          2          2
  Node pairs, AND_CCAST                                                            14         13
  Node pairs, AND_COND                                                             17         17
  Node pairs, AND_CONST                            1        256        256       1058        548
  Node pairs, AND_LOGNOT                           2
  Node pairs, AND_NEGATE                                                          256        128
  Node pairs, AND_NOT                                         2          4          4          3
  Node pairs, AND_OR                                                                2
  Node pairs, AND_REPLICATE                        1        256        256
  Node pairs, AND_SEL                                         1          1
  Node pairs, AND_SELBIT                           1
  Node pairs, AND_SHIFTL                                                          256        128
  Node pairs, AND_SHIFTR                                                          290        144
  Node pairs, AND_SUB                                                               1          1
  Node pairs, AND_VARREF                                                 2        157         85
  Node pairs, AND_WORDSEL                                                          16         16
  Node pairs, AND_XOR                                                              48         16
  Node pairs, ARG_VARREF                          25
  Node pairs, ARRAYSEL_CCAST                                                       40         40
  Node pairs, ARRAYSEL_CONST                                 24         24         24         19
  Node pairs, ARRAYSEL_VARREF                               104        104         24         19
  Node pairs, ASSIGNDLY_AND                        1          1          1         19         19
  Node pairs, ASSIGNDLY_COND                      20         22         22          5          5
  Node pairs, ASSIGNDLY_CONST                      4          2          2          2
  Node pairs, ASSIGNDLY_FUNCREF                    1
  Node pairs, ASSIGNDLY_SEL                                  16         16
  Node pairs, ASSIGNDLY_SELBIT                     4
  Node pairs, ASSIGNDLY_SELEXTRACT                16
  Node pairs, ASSIGNDLY_SUB                        1          1          1
  Node pairs, ASSIGNDLY_VARREF                    29         30         30         28         26
  Node pairs, ASSIGNDLY_XOR                       16         16         16
  Node pairs, ASSIGNPOST_VARREF                              46          4          4          4
  Node pairs, ASSIGNPRE_CONST                                 4
  Node pairs, ASSIGNPRE_VARREF                               50          4          4          4
  Node pairs, ASSIGNW_ADD                          1
  Node pairs, ASSIGNW_ARRAYSEL                               10         10         10          5
  Node pairs, ASSIGNW_CONCAT                       4
  Node pairs, ASSIGNW_FUNCREF                      4
  Node pairs, ASSIGNW_SEL                                     8          8
  Node pairs, ASSIGNW_SELBIT                       5
  Node pairs, ASSIGNW_VARREF                      54         18         18         10          5
  Node pairs, ASSIGNW_XOR                         16
  Node pairs, ASSIGN_ADD                                      1          1
  Node pairs, ASSIGN_AND                                                           37         21
  Node pairs, ASSIGN_ARRAYSEL                                44         44         24         24
  Node pairs, ASSIGN_COND                                     1          1          1          1
  Node pairs, ASSIGN_CONST                       267          4          4          2
  Node pairs, ASSIGN_OR                                                           150         85
  Node pairs, ASSIGN_SEL                                    168        170
  Node pairs, ASSIGN_SELEXTRACT                    4
  Node pairs, ASSIGN_VARREF                      268       1178       1178       1256        661
  Node pairs, ASSIGN_WORDSEL                                                       24         24
  Node pairs, ASSIGN_XOR                           5        384        384        256        128
  Node pairs, ATTROF_VARREF                      119
  Node pairs, BASICDTYPE_RANGE                   116
  Node pairs, BEGIN_ASSIGN                         1
  Node pairs, CASEITEM_ASSIGN                    267
  Node pairs, CASEITEM_CONST                     266
  Node pairs, CASE_CASEITEM                        2
  Node pairs, CASE_VARREF                          2
  Node pairs, CCAST_AND                                                           256        128
  Node pairs, CCAST_CONST                                                           5          3
  Node pairs, CCAST_NEQ                                                             1          1
  Node pairs, CCAST_VARREF                                                       1307        694
  Node pairs, CELL_PIN                            22
  Node pairs, CFUNC_ASSIGN                                               3          4          2
  Node pairs, CFUNC_ASSIGNPRE                                            1          1          1
  Node pairs, CFUNC_ASSIGNW                                              3          1
  Node pairs, CFUNC_CCALL                                     1          3          3
  Node pairs, CFUNC_CHANGEDET                                            1          1
  Node pairs, CFUNC_COMMENT                                             41         21         21
  Node pairs, CFUNC_CSTMT                                                1          1
  Node pairs, CFUNC_IF                                                   2          2          1
  Node pairs, CFUNC_TRACEDECL                                 1          1          1
  Node pairs, CFUNC_TRACEINC                                  1          4          4
  Node pairs, CFUNC_VAR                                                             1          1
  Node pairs, CONCAT_CONCAT                        8
  Node pairs, CONCAT_CONST                         1        256        256
  Node pairs, CONCAT_SEL                                    256        256
  Node pairs, CONCAT_SELEXTRACT                    1
  Node pairs, CONCAT_VARREF                       16
  Node pairs, COND_ADD                                        1          1          1          1
  Node pairs, COND_AND                                                             11         11
  Node pairs, COND_CCAST                                                           22         22
  Node pairs, COND_COND                                      10         10         10         10
  Node pairs, COND_CONST                                     14         14         14         14
  Node pairs, COND_SEL                                       15         15
  Node pairs, COND_SELEXTRACT                      4
  Node pairs, COND_VARREF                         36         23         23          1          1
  Node pairs, COND_WORDSEL                                                          4          4
  Node pairs, COND_XOR                            20         36         36         36         36
  Node pairs, CSTMT_TEXT                                                 2          2
  Node pairs, FUNCREF_ARG                         13
  Node pairs, FUNC_VAR                             6
  Node pairs, IF_AND                                                     2          6          1
  Node pairs, IF_ASSIGN                                       4                     1          1
  Node pairs, IF_ASSIGNDLY                         8          4          4          3          1
  Node pairs, IF_CCALL                                                   3          3          1
  Node pairs, IF_IF                                2          2          2          2          2
  Node pairs, IF_NEQ                                                                2          1
  Node pairs, IF_OR                                                      2
  Node pairs, IF_REDOR                             2          2          2
  Node pairs, IF_SEL                                                     2
  Node pairs, IF_VARREF                           10         14          6          6          3
  Node pairs, INITARRAY_CONST                                 1          1          1
  Node pairs, LOGNOT_REDOR                         1
  Node pairs, LOGNOT_VARREF                        1
  Node pairs, MODULE_VAR                           4          2          2          2
  Node pairs, NEGATE_CCAST                                                        256        128
  Node pairs, NEQ_AND                                                               1          1
  Node pairs, NEQ_CCAST                                                             2          1
  Node pairs, NEQ_CONST                                                             3          2
  Node pairs, NETLIST_CFILE                                                         1
  Node pairs, NETLIST_MODULE                       1          1          1          1
  Node pairs, NETLIST_TYPETABLE                    1          1          1          1
  Node pairs, NOT_CCAST                                                             4          3
  Node pairs, NOT_REDOR                                       1          1
  Node pairs, NOT_VARREF                                      1          3
  Node pairs, OR_AND                                                              164        100
  Node pairs, OR_CCAST                                                              1          1
  Node pairs, OR_CONST                                                              2          1
  Node pairs, OR_SEL                                                     4
  Node pairs, OR_SHIFTL                                                           108         60
  Node pairs, OR_SHIFTR                                                            11          9
  Node pairs, OR_VARREF                                                             4          1
  Node pairs, OR_XOR                                                               32         16
  Node pairs, PIN_SELEXTRACT                       8
  Node pairs, PIN_VARREF                          42
  Node pairs, RANGE_CONST                        234          4          4          4
  Node pairs, REDOR_SEL                                       1          1
  Node pairs, REDOR_SELEXTRACT                     1
  Node pairs, REDOR_VARREF                         2          2          2
  Node pairs, REPLICATE_CONST                      1        256        256
  Node pairs, REPLICATE_SEL                                 256        256
  Node pairs, REPLICATE_SELBIT                     1
  Node pairs, SCOPE_ACTIVE                                   21
  Node pairs, SCOPE_CFUNC                                               21
  Node pairs, SCOPE_VARSCOPE                                 21         21
  Node pairs, SELBIT_ATTROF                       21
  Node pairs, SELBIT_CONST                        21
  Node pairs, SELBIT_VARREF                       21
  Node pairs, SELEXTRACT_ATTROF                   98
  Node pairs, SELEXTRACT_CONST                   196
  Node pairs, SELEXTRACT_VARREF                   98
  Node pairs, SEL_CONST                                    1610       1666
  Node pairs, SEL_VARREF                                    805        833
  Node pairs, SENITEM_VARREF                      43          1
  Node pairs, SENTREE_SENITEM                     43          3
  Node pairs, SHIFTL_CCAST                                                        271        143
  Node pairs, SHIFTL_CONST                                                        376        200
  Node pairs, SHIFTL_WORDSEL                                                        9          9
  Node pairs, SHIFTL_XOR                                                           96         48
  Node pairs, SHIFTR_CCAST                                                        257        129
  Node pairs, SHIFTR_CONST                                                        352        180
  Node pairs, SHIFTR_VARREF                                                        83         39
  Node pairs, SHIFTR_WORDSEL                                                       12         12
  Node pairs, SUB_CCAST                                                             2          2
  Node pairs, SUB_CONST                            1          1          1
  Node pairs, SUB_VARREF                           1          1          1
  Node pairs, TOPSCOPE_SCOPE                                  1          1          1
  Node pairs, TOPSCOPE_SENTREE                                1
  Node pairs, TRACEINC_ADD                                    1          2
  Node pairs, TRACEINC_AND                                                         42
  Node pairs, TRACEINC_SEL                                    4          8
  Node pairs, TRACEINC_VARREF                               136        142        142
  Node pairs, TRACEINC_XOR                                   16         32
  Node pairs, TYPETABLE_BASICDTYPE                 1          1          1          1
  Node pairs, UNPACKARRAYDTYPE_BASICDTYPE          1
  Node pairs, UNPACKARRAYDTYPE_RANGE               1          2          2          2
  Node pairs, VAR_BASICDTYPE                     124
  Node pairs, VAR_INITARRAY                                   1          1          1
  Node pairs, VAR_UNPACKARRAYDTYPE                 1
  Node pairs, WORDSEL_CONST                                                        69         69
  Node pairs, WORDSEL_VARREF                                                       69         69
  Node pairs, XOR_AND                              1        256        256        524        268
  Node pairs, XOR_ARRAYSEL                                   10         10         10         10
  Node pairs, XOR_CCAST                                                           692        340
  Node pairs, XOR_CONCAT                           1        256        256
  Node pairs, XOR_FUNCREF                          8
  Node pairs, XOR_OR                                                                9          9
  Node pairs, XOR_SEL                                        80         96
  Node pairs, XOR_SELBIT                          10
  Node pairs, XOR_SELEXTRACT                      64
  Node pairs, XOR_SHIFTL                                                           12         12
  Node pairs, XOR_SHIFTR                                                           51         27
  Node pairs, XOR_VARREF                          52        696        712         28         20
  Node pairs, XOR_WORDSEL                                                           4          4
  Node pairs, XOR_XOR                             22        394        394        394        202

  Var space, non-arrays, bytes                     0        618        565        565         23
  Var space, scoped, bytes                                  656        603

  Vars, clock attribute                            0          1          1          1          0
  Vars, unpacked arrayed                           0          2          2          2          0
  Vars, width    1                                           11          6          6
  Vars, width    4                                            5          5          5          3
  Vars, width    8                                          391        375        375
  Vars, width   32                                           30         30         30          5
  Vars, width  128                                            6          4          4

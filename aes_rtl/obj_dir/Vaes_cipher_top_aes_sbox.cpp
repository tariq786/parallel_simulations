// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vaes_cipher_top.h for the primary calling header

#include "Vaes_cipher_top_aes_sbox.h" // For This
#include "Vaes_cipher_top__Syms.h"

//--------------------
// STATIC VARIABLES

VL_ST_SIG8(Vaes_cipher_top_aes_sbox::__Vtable1_d[256],7,0);

//--------------------

VL_CTOR_IMP(Vaes_cipher_top_aes_sbox) {
    // Reset internal values
    // Reset structure values
    a = VL_RAND_RESET_I(8);
    d = VL_RAND_RESET_I(8);
    __Vtableidx1 = VL_RAND_RESET_I(8);
    __Vtable1_d[0] = 0x63;
    __Vtable1_d[1] = 0x7c;
    __Vtable1_d[2] = 0x77;
    __Vtable1_d[3] = 0x7b;
    __Vtable1_d[4] = 0xf2;
    __Vtable1_d[5] = 0x6b;
    __Vtable1_d[6] = 0x6f;
    __Vtable1_d[7] = 0xc5;
    __Vtable1_d[8] = 0x30;
    __Vtable1_d[9] = 1;
    __Vtable1_d[10] = 0x67;
    __Vtable1_d[11] = 0x2b;
    __Vtable1_d[12] = 0xfe;
    __Vtable1_d[13] = 0xd7;
    __Vtable1_d[14] = 0xab;
    __Vtable1_d[15] = 0x76;
    __Vtable1_d[16] = 0xca;
    __Vtable1_d[17] = 0x82;
    __Vtable1_d[18] = 0xc9;
    __Vtable1_d[19] = 0x7d;
    __Vtable1_d[20] = 0xfa;
    __Vtable1_d[21] = 0x59;
    __Vtable1_d[22] = 0x47;
    __Vtable1_d[23] = 0xf0;
    __Vtable1_d[24] = 0xad;
    __Vtable1_d[25] = 0xd4;
    __Vtable1_d[26] = 0xa2;
    __Vtable1_d[27] = 0xaf;
    __Vtable1_d[28] = 0x9c;
    __Vtable1_d[29] = 0xa4;
    __Vtable1_d[30] = 0x72;
    __Vtable1_d[31] = 0xc0;
    __Vtable1_d[32] = 0xb7;
    __Vtable1_d[33] = 0xfd;
    __Vtable1_d[34] = 0x93;
    __Vtable1_d[35] = 0x26;
    __Vtable1_d[36] = 0x36;
    __Vtable1_d[37] = 0x3f;
    __Vtable1_d[38] = 0xf7;
    __Vtable1_d[39] = 0xcc;
    __Vtable1_d[40] = 0x34;
    __Vtable1_d[41] = 0xa5;
    __Vtable1_d[42] = 0xe5;
    __Vtable1_d[43] = 0xf1;
    __Vtable1_d[44] = 0x71;
    __Vtable1_d[45] = 0xd8;
    __Vtable1_d[46] = 0x31;
    __Vtable1_d[47] = 0x15;
    __Vtable1_d[48] = 4;
    __Vtable1_d[49] = 0xc7;
    __Vtable1_d[50] = 0x23;
    __Vtable1_d[51] = 0xc3;
    __Vtable1_d[52] = 0x18;
    __Vtable1_d[53] = 0x96;
    __Vtable1_d[54] = 5;
    __Vtable1_d[55] = 0x9a;
    __Vtable1_d[56] = 7;
    __Vtable1_d[57] = 0x12;
    __Vtable1_d[58] = 0x80;
    __Vtable1_d[59] = 0xe2;
    __Vtable1_d[60] = 0xeb;
    __Vtable1_d[61] = 0x27;
    __Vtable1_d[62] = 0xb2;
    __Vtable1_d[63] = 0x75;
    __Vtable1_d[64] = 9;
    __Vtable1_d[65] = 0x83;
    __Vtable1_d[66] = 0x2c;
    __Vtable1_d[67] = 0x1a;
    __Vtable1_d[68] = 0x1b;
    __Vtable1_d[69] = 0x6e;
    __Vtable1_d[70] = 0x5a;
    __Vtable1_d[71] = 0xa0;
    __Vtable1_d[72] = 0x52;
    __Vtable1_d[73] = 0x3b;
    __Vtable1_d[74] = 0xd6;
    __Vtable1_d[75] = 0xb3;
    __Vtable1_d[76] = 0x29;
    __Vtable1_d[77] = 0xe3;
    __Vtable1_d[78] = 0x2f;
    __Vtable1_d[79] = 0x84;
    __Vtable1_d[80] = 0x53;
    __Vtable1_d[81] = 0xd1;
    __Vtable1_d[82] = 0;
    __Vtable1_d[83] = 0xed;
    __Vtable1_d[84] = 0x20;
    __Vtable1_d[85] = 0xfc;
    __Vtable1_d[86] = 0xb1;
    __Vtable1_d[87] = 0x5b;
    __Vtable1_d[88] = 0x6a;
    __Vtable1_d[89] = 0xcb;
    __Vtable1_d[90] = 0xbe;
    __Vtable1_d[91] = 0x39;
    __Vtable1_d[92] = 0x4a;
    __Vtable1_d[93] = 0x4c;
    __Vtable1_d[94] = 0x58;
    __Vtable1_d[95] = 0xcf;
    __Vtable1_d[96] = 0xd0;
    __Vtable1_d[97] = 0xef;
    __Vtable1_d[98] = 0xaa;
    __Vtable1_d[99] = 0xfb;
    __Vtable1_d[100] = 0x43;
    __Vtable1_d[101] = 0x4d;
    __Vtable1_d[102] = 0x33;
    __Vtable1_d[103] = 0x85;
    __Vtable1_d[104] = 0x45;
    __Vtable1_d[105] = 0xf9;
    __Vtable1_d[106] = 2;
    __Vtable1_d[107] = 0x7f;
    __Vtable1_d[108] = 0x50;
    __Vtable1_d[109] = 0x3c;
    __Vtable1_d[110] = 0x9f;
    __Vtable1_d[111] = 0xa8;
    __Vtable1_d[112] = 0x51;
    __Vtable1_d[113] = 0xa3;
    __Vtable1_d[114] = 0x40;
    __Vtable1_d[115] = 0x8f;
    __Vtable1_d[116] = 0x92;
    __Vtable1_d[117] = 0x9d;
    __Vtable1_d[118] = 0x38;
    __Vtable1_d[119] = 0xf5;
    __Vtable1_d[120] = 0xbc;
    __Vtable1_d[121] = 0xb6;
    __Vtable1_d[122] = 0xda;
    __Vtable1_d[123] = 0x21;
    __Vtable1_d[124] = 0x10;
    __Vtable1_d[125] = 0xff;
    __Vtable1_d[126] = 0xf3;
    __Vtable1_d[127] = 0xd2;
    __Vtable1_d[128] = 0xcd;
    __Vtable1_d[129] = 0xc;
    __Vtable1_d[130] = 0x13;
    __Vtable1_d[131] = 0xec;
    __Vtable1_d[132] = 0x5f;
    __Vtable1_d[133] = 0x97;
    __Vtable1_d[134] = 0x44;
    __Vtable1_d[135] = 0x17;
    __Vtable1_d[136] = 0xc4;
    __Vtable1_d[137] = 0xa7;
    __Vtable1_d[138] = 0x7e;
    __Vtable1_d[139] = 0x3d;
    __Vtable1_d[140] = 0x64;
    __Vtable1_d[141] = 0x5d;
    __Vtable1_d[142] = 0x19;
    __Vtable1_d[143] = 0x73;
    __Vtable1_d[144] = 0x60;
    __Vtable1_d[145] = 0x81;
    __Vtable1_d[146] = 0x4f;
    __Vtable1_d[147] = 0xdc;
    __Vtable1_d[148] = 0x22;
    __Vtable1_d[149] = 0x2a;
    __Vtable1_d[150] = 0x90;
    __Vtable1_d[151] = 0x88;
    __Vtable1_d[152] = 0x46;
    __Vtable1_d[153] = 0xee;
    __Vtable1_d[154] = 0xb8;
    __Vtable1_d[155] = 0x14;
    __Vtable1_d[156] = 0xde;
    __Vtable1_d[157] = 0x5e;
    __Vtable1_d[158] = 0xb;
    __Vtable1_d[159] = 0xdb;
    __Vtable1_d[160] = 0xe0;
    __Vtable1_d[161] = 0x32;
    __Vtable1_d[162] = 0x3a;
    __Vtable1_d[163] = 0xa;
    __Vtable1_d[164] = 0x49;
    __Vtable1_d[165] = 6;
    __Vtable1_d[166] = 0x24;
    __Vtable1_d[167] = 0x5c;
    __Vtable1_d[168] = 0xc2;
    __Vtable1_d[169] = 0xd3;
    __Vtable1_d[170] = 0xac;
    __Vtable1_d[171] = 0x62;
    __Vtable1_d[172] = 0x91;
    __Vtable1_d[173] = 0x95;
    __Vtable1_d[174] = 0xe4;
    __Vtable1_d[175] = 0x79;
    __Vtable1_d[176] = 0xe7;
    __Vtable1_d[177] = 0xc8;
    __Vtable1_d[178] = 0x37;
    __Vtable1_d[179] = 0x6d;
    __Vtable1_d[180] = 0x8d;
    __Vtable1_d[181] = 0xd5;
    __Vtable1_d[182] = 0x4e;
    __Vtable1_d[183] = 0xa9;
    __Vtable1_d[184] = 0x6c;
    __Vtable1_d[185] = 0x56;
    __Vtable1_d[186] = 0xf4;
    __Vtable1_d[187] = 0xea;
    __Vtable1_d[188] = 0x65;
    __Vtable1_d[189] = 0x7a;
    __Vtable1_d[190] = 0xae;
    __Vtable1_d[191] = 8;
    __Vtable1_d[192] = 0xba;
    __Vtable1_d[193] = 0x78;
    __Vtable1_d[194] = 0x25;
    __Vtable1_d[195] = 0x2e;
    __Vtable1_d[196] = 0x1c;
    __Vtable1_d[197] = 0xa6;
    __Vtable1_d[198] = 0xb4;
    __Vtable1_d[199] = 0xc6;
    __Vtable1_d[200] = 0xe8;
    __Vtable1_d[201] = 0xdd;
    __Vtable1_d[202] = 0x74;
    __Vtable1_d[203] = 0x1f;
    __Vtable1_d[204] = 0x4b;
    __Vtable1_d[205] = 0xbd;
    __Vtable1_d[206] = 0x8b;
    __Vtable1_d[207] = 0x8a;
    __Vtable1_d[208] = 0x70;
    __Vtable1_d[209] = 0x3e;
    __Vtable1_d[210] = 0xb5;
    __Vtable1_d[211] = 0x66;
    __Vtable1_d[212] = 0x48;
    __Vtable1_d[213] = 3;
    __Vtable1_d[214] = 0xf6;
    __Vtable1_d[215] = 0xe;
    __Vtable1_d[216] = 0x61;
    __Vtable1_d[217] = 0x35;
    __Vtable1_d[218] = 0x57;
    __Vtable1_d[219] = 0xb9;
    __Vtable1_d[220] = 0x86;
    __Vtable1_d[221] = 0xc1;
    __Vtable1_d[222] = 0x1d;
    __Vtable1_d[223] = 0x9e;
    __Vtable1_d[224] = 0xe1;
    __Vtable1_d[225] = 0xf8;
    __Vtable1_d[226] = 0x98;
    __Vtable1_d[227] = 0x11;
    __Vtable1_d[228] = 0x69;
    __Vtable1_d[229] = 0xd9;
    __Vtable1_d[230] = 0x8e;
    __Vtable1_d[231] = 0x94;
    __Vtable1_d[232] = 0x9b;
    __Vtable1_d[233] = 0x1e;
    __Vtable1_d[234] = 0x87;
    __Vtable1_d[235] = 0xe9;
    __Vtable1_d[236] = 0xce;
    __Vtable1_d[237] = 0x55;
    __Vtable1_d[238] = 0x28;
    __Vtable1_d[239] = 0xdf;
    __Vtable1_d[240] = 0x8c;
    __Vtable1_d[241] = 0xa1;
    __Vtable1_d[242] = 0x89;
    __Vtable1_d[243] = 0xd;
    __Vtable1_d[244] = 0xbf;
    __Vtable1_d[245] = 0xe6;
    __Vtable1_d[246] = 0x42;
    __Vtable1_d[247] = 0x68;
    __Vtable1_d[248] = 0x41;
    __Vtable1_d[249] = 0x99;
    __Vtable1_d[250] = 0x2d;
    __Vtable1_d[251] = 0xf;
    __Vtable1_d[252] = 0xb0;
    __Vtable1_d[253] = 0x54;
    __Vtable1_d[254] = 0xbb;
    __Vtable1_d[255] = 0x16;
    __Vtableidx2 = VL_RAND_RESET_I(8);
    __Vtableidx3 = VL_RAND_RESET_I(8);
    __Vtableidx4 = VL_RAND_RESET_I(8);
    __Vtableidx5 = VL_RAND_RESET_I(8);
    __Vtableidx6 = VL_RAND_RESET_I(8);
    __Vtableidx7 = VL_RAND_RESET_I(8);
    __Vtableidx8 = VL_RAND_RESET_I(8);
    __Vtableidx9 = VL_RAND_RESET_I(8);
    __Vtableidx10 = VL_RAND_RESET_I(8);
    __Vtableidx11 = VL_RAND_RESET_I(8);
    __Vtableidx12 = VL_RAND_RESET_I(8);
    __Vtableidx13 = VL_RAND_RESET_I(8);
    __Vtableidx14 = VL_RAND_RESET_I(8);
    __Vtableidx15 = VL_RAND_RESET_I(8);
    __Vtableidx16 = VL_RAND_RESET_I(8);
    __Vtableidx17 = VL_RAND_RESET_I(8);
    __Vtableidx18 = VL_RAND_RESET_I(8);
    __Vtableidx19 = VL_RAND_RESET_I(8);
    __Vtableidx20 = VL_RAND_RESET_I(8);
}

void Vaes_cipher_top_aes_sbox::__Vconfigure(Vaes_cipher_top__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

Vaes_cipher_top_aes_sbox::~Vaes_cipher_top_aes_sbox() {
}

//--------------------
// Internal Methods

void Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us00__16(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us00__16\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at aes/aes_sbox.v:71
    vlSymsp->TOP__v__DOT__us00.__Vtableidx1 = vlTOPp->v__DOT__sa00;
    vlSymsp->TOP__v__DOT__us00.d = (IData)(vlSymsp->TOP__v__DOT__us00.__Vtable1_d)
	[(IData)(vlSymsp->TOP__v__DOT__us00.__Vtableidx1)];
}

void Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us01__12(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us01__12\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at aes/aes_sbox.v:71
    vlSymsp->TOP__v__DOT__us01.__Vtableidx2 = vlTOPp->v__DOT__sa01;
    vlSymsp->TOP__v__DOT__us01.d = (IData)(vlSymsp->TOP__v__DOT__us00.__Vtable1_d)
	[(IData)(vlSymsp->TOP__v__DOT__us01.__Vtableidx2)];
}

void Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us02__8(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us02__8\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at aes/aes_sbox.v:71
    vlSymsp->TOP__v__DOT__us02.__Vtableidx3 = vlTOPp->v__DOT__sa02;
    vlSymsp->TOP__v__DOT__us02.d = (IData)(vlSymsp->TOP__v__DOT__us00.__Vtable1_d)
	[(IData)(vlSymsp->TOP__v__DOT__us02.__Vtableidx3)];
}

void Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us03__4(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us03__4\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at aes/aes_sbox.v:71
    vlSymsp->TOP__v__DOT__us03.__Vtableidx4 = vlTOPp->v__DOT__sa03;
    vlSymsp->TOP__v__DOT__us03.d = (IData)(vlSymsp->TOP__v__DOT__us00.__Vtable1_d)
	[(IData)(vlSymsp->TOP__v__DOT__us03.__Vtableidx4)];
}

void Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us10__15(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us10__15\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at aes/aes_sbox.v:71
    vlSymsp->TOP__v__DOT__us10.__Vtableidx5 = vlTOPp->v__DOT__sa10;
    vlSymsp->TOP__v__DOT__us10.d = (IData)(vlSymsp->TOP__v__DOT__us00.__Vtable1_d)
	[(IData)(vlSymsp->TOP__v__DOT__us10.__Vtableidx5)];
}

void Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us11__11(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us11__11\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at aes/aes_sbox.v:71
    vlSymsp->TOP__v__DOT__us11.__Vtableidx6 = vlTOPp->v__DOT__sa11;
    vlSymsp->TOP__v__DOT__us11.d = (IData)(vlSymsp->TOP__v__DOT__us00.__Vtable1_d)
	[(IData)(vlSymsp->TOP__v__DOT__us11.__Vtableidx6)];
}

void Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us12__7(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us12__7\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at aes/aes_sbox.v:71
    vlSymsp->TOP__v__DOT__us12.__Vtableidx7 = vlTOPp->v__DOT__sa12;
    vlSymsp->TOP__v__DOT__us12.d = (IData)(vlSymsp->TOP__v__DOT__us00.__Vtable1_d)
	[(IData)(vlSymsp->TOP__v__DOT__us12.__Vtableidx7)];
}

void Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us13__3(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us13__3\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at aes/aes_sbox.v:71
    vlSymsp->TOP__v__DOT__us13.__Vtableidx8 = vlTOPp->v__DOT__sa13;
    vlSymsp->TOP__v__DOT__us13.d = (IData)(vlSymsp->TOP__v__DOT__us00.__Vtable1_d)
	[(IData)(vlSymsp->TOP__v__DOT__us13.__Vtableidx8)];
}

void Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us20__14(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us20__14\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at aes/aes_sbox.v:71
    vlSymsp->TOP__v__DOT__us20.__Vtableidx9 = vlTOPp->v__DOT__sa20;
    vlSymsp->TOP__v__DOT__us20.d = (IData)(vlSymsp->TOP__v__DOT__us00.__Vtable1_d)
	[(IData)(vlSymsp->TOP__v__DOT__us20.__Vtableidx9)];
}

void Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us21__10(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us21__10\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at aes/aes_sbox.v:71
    vlSymsp->TOP__v__DOT__us21.__Vtableidx10 = vlTOPp->v__DOT__sa21;
    vlSymsp->TOP__v__DOT__us21.d = (IData)(vlSymsp->TOP__v__DOT__us00.__Vtable1_d)
	[(IData)(vlSymsp->TOP__v__DOT__us21.__Vtableidx10)];
}

void Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us22__6(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us22__6\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at aes/aes_sbox.v:71
    vlSymsp->TOP__v__DOT__us22.__Vtableidx11 = vlTOPp->v__DOT__sa22;
    vlSymsp->TOP__v__DOT__us22.d = (IData)(vlSymsp->TOP__v__DOT__us00.__Vtable1_d)
	[(IData)(vlSymsp->TOP__v__DOT__us22.__Vtableidx11)];
}

void Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us23__2(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us23__2\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at aes/aes_sbox.v:71
    vlSymsp->TOP__v__DOT__us23.__Vtableidx12 = vlTOPp->v__DOT__sa23;
    vlSymsp->TOP__v__DOT__us23.d = (IData)(vlSymsp->TOP__v__DOT__us00.__Vtable1_d)
	[(IData)(vlSymsp->TOP__v__DOT__us23.__Vtableidx12)];
}

void Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us30__13(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us30__13\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at aes/aes_sbox.v:71
    vlSymsp->TOP__v__DOT__us30.__Vtableidx13 = vlTOPp->v__DOT__sa30;
    vlSymsp->TOP__v__DOT__us30.d = (IData)(vlSymsp->TOP__v__DOT__us00.__Vtable1_d)
	[(IData)(vlSymsp->TOP__v__DOT__us30.__Vtableidx13)];
}

void Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us31__9(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us31__9\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at aes/aes_sbox.v:71
    vlSymsp->TOP__v__DOT__us31.__Vtableidx14 = vlTOPp->v__DOT__sa31;
    vlSymsp->TOP__v__DOT__us31.d = (IData)(vlSymsp->TOP__v__DOT__us00.__Vtable1_d)
	[(IData)(vlSymsp->TOP__v__DOT__us31.__Vtableidx14)];
}

void Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us32__5(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us32__5\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at aes/aes_sbox.v:71
    vlSymsp->TOP__v__DOT__us32.__Vtableidx15 = vlTOPp->v__DOT__sa32;
    vlSymsp->TOP__v__DOT__us32.d = (IData)(vlSymsp->TOP__v__DOT__us00.__Vtable1_d)
	[(IData)(vlSymsp->TOP__v__DOT__us32.__Vtableidx15)];
}

void Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us33__1(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          Vaes_cipher_top_aes_sbox::_sequent__TOP__v__DOT__us33__1\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at aes/aes_sbox.v:71
    vlSymsp->TOP__v__DOT__us33.__Vtableidx16 = vlTOPp->v__DOT__sa33;
    vlSymsp->TOP__v__DOT__us33.d = (IData)(vlSymsp->TOP__v__DOT__us00.__Vtable1_d)
	[(IData)(vlSymsp->TOP__v__DOT__us33.__Vtableidx16)];
}

void Vaes_cipher_top_aes_sbox::_settle__TOP__v__DOT__u0__DOT__u0__33(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          Vaes_cipher_top_aes_sbox::_settle__TOP__v__DOT__u0__DOT__u0__33\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at aes/aes_sbox.v:71
    vlSymsp->TOP__v__DOT__u0__DOT__u0.__Vtableidx17 
	= (0xff & (vlTOPp->v__DOT__u0__DOT__tmp_w >> 0x10));
    vlSymsp->TOP__v__DOT__u0__DOT__u0.d = (IData)(vlSymsp->TOP__v__DOT__us00.__Vtable1_d)
	[(IData)(vlSymsp->TOP__v__DOT__u0__DOT__u0.__Vtableidx17)];
}

void Vaes_cipher_top_aes_sbox::_settle__TOP__v__DOT__u0__DOT__u1__34(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          Vaes_cipher_top_aes_sbox::_settle__TOP__v__DOT__u0__DOT__u1__34\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at aes/aes_sbox.v:71
    vlSymsp->TOP__v__DOT__u0__DOT__u1.__Vtableidx18 
	= (0xff & (vlTOPp->v__DOT__u0__DOT__tmp_w >> 8));
    vlSymsp->TOP__v__DOT__u0__DOT__u1.d = (IData)(vlSymsp->TOP__v__DOT__us00.__Vtable1_d)
	[(IData)(vlSymsp->TOP__v__DOT__u0__DOT__u1.__Vtableidx18)];
}

void Vaes_cipher_top_aes_sbox::_settle__TOP__v__DOT__u0__DOT__u2__35(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          Vaes_cipher_top_aes_sbox::_settle__TOP__v__DOT__u0__DOT__u2__35\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at aes/aes_sbox.v:71
    vlSymsp->TOP__v__DOT__u0__DOT__u2.__Vtableidx19 
	= (0xff & vlTOPp->v__DOT__u0__DOT__tmp_w);
    vlSymsp->TOP__v__DOT__u0__DOT__u2.d = (IData)(vlSymsp->TOP__v__DOT__us00.__Vtable1_d)
	[(IData)(vlSymsp->TOP__v__DOT__u0__DOT__u2.__Vtableidx19)];
}

void Vaes_cipher_top_aes_sbox::_settle__TOP__v__DOT__u0__DOT__u3__36(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          Vaes_cipher_top_aes_sbox::_settle__TOP__v__DOT__u0__DOT__u3__36\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at aes/aes_sbox.v:71
    vlSymsp->TOP__v__DOT__u0__DOT__u3.__Vtableidx20 
	= (0xff & (vlTOPp->v__DOT__u0__DOT__tmp_w >> 0x18));
    vlSymsp->TOP__v__DOT__u0__DOT__u3.d = (IData)(vlSymsp->TOP__v__DOT__us00.__Vtable1_d)
	[(IData)(vlSymsp->TOP__v__DOT__u0__DOT__u3.__Vtableidx20)];
}

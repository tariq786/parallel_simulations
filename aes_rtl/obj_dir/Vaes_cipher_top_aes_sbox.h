// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See Vaes_cipher_top.h for the primary calling header

#ifndef _Vaes_cipher_top_aes_sbox_H_
#define _Vaes_cipher_top_aes_sbox_H_

#include "verilated.h"
class Vaes_cipher_top__Syms;
class VerilatedVcd;

//----------

VL_MODULE(Vaes_cipher_top_aes_sbox) {
  public:
    // CELLS
    
    // PORTS
    VL_IN8(a,7,0);
    VL_OUT8(d,7,0);
    //char	__VpadToAlign2[2];
    
    // LOCAL SIGNALS
    
    // LOCAL VARIABLES
    static VL_ST_SIG8(__Vtable1_d[256],7,0);
    VL_SIG8(__Vtableidx1,7,0);
    VL_SIG8(__Vtableidx2,7,0);
    VL_SIG8(__Vtableidx3,7,0);
    VL_SIG8(__Vtableidx4,7,0);
    VL_SIG8(__Vtableidx5,7,0);
    VL_SIG8(__Vtableidx6,7,0);
    VL_SIG8(__Vtableidx7,7,0);
    VL_SIG8(__Vtableidx8,7,0);
    VL_SIG8(__Vtableidx9,7,0);
    VL_SIG8(__Vtableidx10,7,0);
    VL_SIG8(__Vtableidx11,7,0);
    VL_SIG8(__Vtableidx12,7,0);
    VL_SIG8(__Vtableidx13,7,0);
    VL_SIG8(__Vtableidx14,7,0);
    VL_SIG8(__Vtableidx15,7,0);
    VL_SIG8(__Vtableidx16,7,0);
    VL_SIG8(__Vtableidx17,7,0);
    VL_SIG8(__Vtableidx18,7,0);
    VL_SIG8(__Vtableidx19,7,0);
    VL_SIG8(__Vtableidx20,7,0);
    
    // INTERNAL VARIABLES
  private:
    //char	__VpadToAlign36[4];
    Vaes_cipher_top__Syms*	__VlSymsp;		// Symbol table
  public:
    
    // PARAMETERS
    
    // CONSTRUCTORS
  private:
    Vaes_cipher_top_aes_sbox& operator= (const Vaes_cipher_top_aes_sbox&);	///< Copying not allowed
    Vaes_cipher_top_aes_sbox(const Vaes_cipher_top_aes_sbox&);	///< Copying not allowed
  public:
    Vaes_cipher_top_aes_sbox(const char* name="TOP");
    ~Vaes_cipher_top_aes_sbox();
    void trace (VerilatedVcdC* tfp, int levels, int options=0);
    
    // USER METHODS
    
    // API METHODS
    
    // INTERNAL METHODS
    void __Vconfigure(Vaes_cipher_top__Syms* symsp, bool first);
    static void	_sequent__TOP__v__DOT__us00__16(Vaes_cipher_top__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__v__DOT__us01__12(Vaes_cipher_top__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__v__DOT__us02__8(Vaes_cipher_top__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__v__DOT__us03__4(Vaes_cipher_top__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__v__DOT__us10__15(Vaes_cipher_top__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__v__DOT__us11__11(Vaes_cipher_top__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__v__DOT__us12__7(Vaes_cipher_top__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__v__DOT__us13__3(Vaes_cipher_top__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__v__DOT__us20__14(Vaes_cipher_top__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__v__DOT__us21__10(Vaes_cipher_top__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__v__DOT__us22__6(Vaes_cipher_top__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__v__DOT__us23__2(Vaes_cipher_top__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__v__DOT__us30__13(Vaes_cipher_top__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__v__DOT__us31__9(Vaes_cipher_top__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__v__DOT__us32__5(Vaes_cipher_top__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__v__DOT__us33__1(Vaes_cipher_top__Syms* __restrict vlSymsp);
    static void	_settle__TOP__v__DOT__u0__DOT__u0__33(Vaes_cipher_top__Syms* __restrict vlSymsp);
    static void	_settle__TOP__v__DOT__u0__DOT__u1__34(Vaes_cipher_top__Syms* __restrict vlSymsp);
    static void	_settle__TOP__v__DOT__u0__DOT__u2__35(Vaes_cipher_top__Syms* __restrict vlSymsp);
    static void	_settle__TOP__v__DOT__u0__DOT__u3__36(Vaes_cipher_top__Syms* __restrict vlSymsp);
    static void traceInit (VerilatedVcd* vcdp, void* userthis, uint32_t code);
    static void traceFull (VerilatedVcd* vcdp, void* userthis, uint32_t code);
    static void traceChg  (VerilatedVcd* vcdp, void* userthis, uint32_t code);
} VL_ATTR_ALIGNED(128);

#endif  /*guard*/

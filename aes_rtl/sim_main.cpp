/*
 * =====================================================================================
 *
 *       Filename:  sim_main.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  11/10/2013 08:33:34 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */



#include<omp.h>
#include "Vaes_cipher_top.h"
#include "verilated.h"
#include "verilated_vcd_c.h"
#include <stdio.h>


vluint64_t main_time = 0;       // Current simulation time
 // This is a 64-bit integer to reduce wrap over issues and
 // allow modulus.  You can also use a double, if you wish.

  double sc_time_stamp ()
{       // Called by $time in Verilog
       return main_time;           // converts to double, to match
                                              // what SystemC does
 }



int main(int argc, char **argv, char **env) 
{
          Verilated::commandArgs(argc, argv);
          Vaes_cipher_top* top = new Vaes_cipher_top;
          int i = 0;
          unsigned int ld_set = 0;
          top->rst = 1;           // assert reset
	  
	   #ifdef VCD_DUMP
	   Verilated::traceEverOn(true);
           VL_PRINTF("Enabling waves...\n");
           VerilatedVcdC* tfp = new VerilatedVcdC;
           top->trace (tfp, 99);
           tfp->open ("simx.vcd");
 	   #endif
            //while (!Verilated::gotFinish()/*   && i < 65*/)
            while (main_time < 500)
            {
            //
          //  #ifdef OPEN_MP
	//	{
	  //  	printf("Executing Parallel Version of the code\n");
	//	}
	//    #else
	//    	printf("Executing Serial Version of the code\n");
	//    #endif
           // #pragma omp parallel for num_threads(4)
	   // for (i=0; i < 1; i++)
	   // {
              if (main_time > 10)
		{
                  top->rst = 0;   // Deassert reset
                }
              if ((main_time % 10) == 1)
                {
                  top->clk = 1;       // Toggle clock
		  
		  if(ld_set!=2 && main_time > 10)
                   {
                    top -> ld = 1;
                    top -> key =     {0x00000000,0x000000000,0x00000000,0x00000000};
                    top -> text_in = {0x00000000,0x00000000,0x00000000,0x00000000}; 
                    ld_set++;
                    }
		    else
		    {
			top -> ld = 0;
			
                        top -> key =     {0x00000000,0x000000000,0x00000000,0x00000000};
                        top -> text_in = {0x00000000,0x00000000,0x00000000,0x00000000}; 
		    }	
                }
              if ((main_time % 10) == 6)
                {
                  
                  top->clk = 0;
                  //setting DUT values
                  
		  i++;
                }

                top->eval();            // Evaluate model
		#ifdef VCD_DUMP
	        tfp->dump (main_time);  //signal dump
		#endif
                if(top->done) 
		{
	//	  printf("Done is %2d\n, rst=%2d",top->done,top->rst);
	//	#ifdef OPENMP 
	//	#ifdef OPENMP 
		   //printf("reset is %2d\n",top->rst);
	//	   if( (main_time % 10)== 1)
	//	    {	
                    printf("key=%2x%2x%2x%2x\n",top->key[3],top->key[2],top->key[1],top->key[0]);       // Read a output
                    printf("text_in=%2x%2x%2x%2x\n",top->text_in[3],top->text_in[2],top->text_in[1],top->text_in[0]);       // Read a output
                    printf("Time=%2d,text_out=%2x%2x%2x%2x, done=%2d\n",main_time,top->text_out[3],top->text_out[2],top->text_out[1],top->text_out[0],top->done);       // Read a output
	//	     } //if( (main_time % 10) == 1)
		} //if(top->done)
	//	#else
        //     printf("a=%2d,b=%2d,cin=%2d,sum =%2d,sum2=%2d,sum3=%2d,sum4=%2d, %2d of %2d \n",top->a,top->b,top->cin,top->sum,top->cout,top2->sum,top3->sum,top4->sum,omp_get_thread_num(),omp_get_num_threads());       // Read a output
        //     printf("a=%2d,b=%2d,cin=%2d,sum =%2d,sum2=%2d,sum3=%2d,sum4=%2d\n",top->a,top->b,top->cin,top->sum,top->cout,top2->sum,top3->sum,top4->sum);       // Read a output
	//	#endif
                main_time++;            // Time passes...
            } //end of while 
	    printf("\n Test Done\n");
            top->final();               // Done simulating
	    #ifdef VCD_DUMP
            tfp->close();
	    #endif
            //    // (Though this example doesn't get here)
            delete top;
	    
	return 0;
} //end of main

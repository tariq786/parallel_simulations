
`timescale 1 ns/1 ps

module test_bench;

  reg		clk;
  reg		rst;
  reg		kld;
  reg [127:0]   key; 
  reg [127:0]   text_in;

  wire [127:0]  text_out;
  wire		done;


  initial
    begin
      
      clk <=  0;
      rst = 0;
      kld = 0;
      repeat(1) @(posedge clk);
      rst =  1;
      repeat(6) @(posedge clk);
      rst =  0;

      repeat(1) @(negedge clk);
      kld 		=  1;                   
      key 		=  128'h0;
      text_in           =  128'h0112233445566778899aabbccddeeff;
      repeat(2) @(posedge clk);   
      kld               =  0;
     /* 
      repeat(11) @(posedge clk);
      kld 	       = #1 1;                   
      key 	       = #1 128'h11111111111111111111111111111111;
      text_in          = #1 128'h8899aabbccddeeff0011223344556677;
      repeat(1) @(posedge clk);
      kld 	       = #1 0;                   
      */
      
    end
  
  aes_cipher_top uut(
	             .clk(		clk		),
	             .rst(		rst		),
	             .ld(		kld		),
	             .done(		done		),
	             .key(		key		),
	             .text_in(	text_in		),
	             .text_out(	text_out	)
	             );

/*
  initial
     $sdf_annotate("aes_cipher_top.sdf",uut);
*/
  initial
    forever #5 clk = ~clk;

  initial
    begin
      $vcdpluson;	
      #500 $finish;
    end
  
  initial
    $monitor($time," TEXT_OUT is %h, DONE is %b\n",text_out,done);
  
endmodule



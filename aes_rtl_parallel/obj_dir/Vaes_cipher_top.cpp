// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vaes_cipher_top.h for the primary calling header

#include "Vaes_cipher_top.h"   // For This
#include "Vaes_cipher_top__Syms.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(Vaes_cipher_top) {
    Vaes_cipher_top__Syms* __restrict vlSymsp = __VlSymsp = new Vaes_cipher_top__Syms(this, name());
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    VL_CELL (__PVT__v__DOT__us00, Vaes_cipher_top_aes_sbox);
    VL_CELL (__PVT__v__DOT__us01, Vaes_cipher_top_aes_sbox);
    VL_CELL (__PVT__v__DOT__us02, Vaes_cipher_top_aes_sbox);
    VL_CELL (__PVT__v__DOT__us03, Vaes_cipher_top_aes_sbox);
    VL_CELL (__PVT__v__DOT__us10, Vaes_cipher_top_aes_sbox);
    VL_CELL (__PVT__v__DOT__us11, Vaes_cipher_top_aes_sbox);
    VL_CELL (__PVT__v__DOT__us12, Vaes_cipher_top_aes_sbox);
    VL_CELL (__PVT__v__DOT__us13, Vaes_cipher_top_aes_sbox);
    VL_CELL (__PVT__v__DOT__us20, Vaes_cipher_top_aes_sbox);
    VL_CELL (__PVT__v__DOT__us21, Vaes_cipher_top_aes_sbox);
    VL_CELL (__PVT__v__DOT__us22, Vaes_cipher_top_aes_sbox);
    VL_CELL (__PVT__v__DOT__us23, Vaes_cipher_top_aes_sbox);
    VL_CELL (__PVT__v__DOT__us30, Vaes_cipher_top_aes_sbox);
    VL_CELL (__PVT__v__DOT__us31, Vaes_cipher_top_aes_sbox);
    VL_CELL (__PVT__v__DOT__us32, Vaes_cipher_top_aes_sbox);
    VL_CELL (__PVT__v__DOT__us33, Vaes_cipher_top_aes_sbox);
    VL_CELL (__PVT__v__DOT__u0__DOT__u0, Vaes_cipher_top_aes_sbox);
    VL_CELL (__PVT__v__DOT__u0__DOT__u1, Vaes_cipher_top_aes_sbox);
    VL_CELL (__PVT__v__DOT__u0__DOT__u2, Vaes_cipher_top_aes_sbox);
    VL_CELL (__PVT__v__DOT__u0__DOT__u3, Vaes_cipher_top_aes_sbox);
    // Reset internal values
    
    // Reset structure values
    clk = VL_RAND_RESET_I(1);
    rst = VL_RAND_RESET_I(1);
    ld = VL_RAND_RESET_I(1);
    done = VL_RAND_RESET_I(1);
    VL_RAND_RESET_W(128,key);
    VL_RAND_RESET_W(128,text_in);
    VL_RAND_RESET_W(128,text_out);
    v__DOT__w0 = VL_RAND_RESET_I(32);
    v__DOT__w1 = VL_RAND_RESET_I(32);
    v__DOT__w2 = VL_RAND_RESET_I(32);
    v__DOT__w3 = VL_RAND_RESET_I(32);
    VL_RAND_RESET_W(128,v__DOT__text_in_r);
    v__DOT__sa00 = VL_RAND_RESET_I(8);
    v__DOT__sa01 = VL_RAND_RESET_I(8);
    v__DOT__sa02 = VL_RAND_RESET_I(8);
    v__DOT__sa03 = VL_RAND_RESET_I(8);
    v__DOT__sa10 = VL_RAND_RESET_I(8);
    v__DOT__sa11 = VL_RAND_RESET_I(8);
    v__DOT__sa12 = VL_RAND_RESET_I(8);
    v__DOT__sa13 = VL_RAND_RESET_I(8);
    v__DOT__sa20 = VL_RAND_RESET_I(8);
    v__DOT__sa21 = VL_RAND_RESET_I(8);
    v__DOT__sa22 = VL_RAND_RESET_I(8);
    v__DOT__sa23 = VL_RAND_RESET_I(8);
    v__DOT__sa30 = VL_RAND_RESET_I(8);
    v__DOT__sa31 = VL_RAND_RESET_I(8);
    v__DOT__sa32 = VL_RAND_RESET_I(8);
    v__DOT__sa33 = VL_RAND_RESET_I(8);
    v__DOT__sa00_mc = VL_RAND_RESET_I(8);
    v__DOT__sa01_mc = VL_RAND_RESET_I(8);
    v__DOT__sa02_mc = VL_RAND_RESET_I(8);
    v__DOT__sa03_mc = VL_RAND_RESET_I(8);
    v__DOT__sa10_mc = VL_RAND_RESET_I(8);
    v__DOT__sa11_mc = VL_RAND_RESET_I(8);
    v__DOT__sa12_mc = VL_RAND_RESET_I(8);
    v__DOT__sa13_mc = VL_RAND_RESET_I(8);
    v__DOT__sa20_mc = VL_RAND_RESET_I(8);
    v__DOT__sa21_mc = VL_RAND_RESET_I(8);
    v__DOT__sa22_mc = VL_RAND_RESET_I(8);
    v__DOT__sa23_mc = VL_RAND_RESET_I(8);
    v__DOT__sa30_mc = VL_RAND_RESET_I(8);
    v__DOT__sa31_mc = VL_RAND_RESET_I(8);
    v__DOT__sa32_mc = VL_RAND_RESET_I(8);
    v__DOT__sa33_mc = VL_RAND_RESET_I(8);
    v__DOT__ld_r = VL_RAND_RESET_I(1);
    v__DOT__dcnt = VL_RAND_RESET_I(4);
    { int __Vi0=0; for (; __Vi0<4; ++__Vi0) {
	    v__DOT__u0__DOT__w[__Vi0] = VL_RAND_RESET_I(32);
    }}
    v__DOT__u0__DOT__tmp_w = VL_RAND_RESET_I(32);
    v__DOT__u0__DOT__subword = VL_RAND_RESET_I(32);
    v__DOT__u0__DOT__rcon = VL_RAND_RESET_I(32);
    v__DOT__u0__DOT__r0__DOT__rcnt = VL_RAND_RESET_I(4);
    __Vfunc_v__DOT__mix_col__0__Vfuncout = VL_RAND_RESET_I(32);
    __Vfunc_v__DOT__mix_col__0__s0 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__0__s1 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__0__s2 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__0__s3 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__1__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__1__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__2__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__2__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__3__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__3__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__4__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__4__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__5__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__5__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__6__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__6__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__7__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__7__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__8__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__8__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__9__Vfuncout = VL_RAND_RESET_I(32);
    __Vfunc_v__DOT__mix_col__9__s0 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__9__s1 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__9__s2 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__9__s3 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__10__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__10__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__11__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__11__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__12__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__12__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__13__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__13__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__14__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__14__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__15__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__15__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__16__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__16__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__17__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__17__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__18__Vfuncout = VL_RAND_RESET_I(32);
    __Vfunc_v__DOT__mix_col__18__s0 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__18__s1 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__18__s2 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__18__s3 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__19__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__19__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__20__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__20__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__21__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__21__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__22__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__22__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__23__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__23__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__24__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__24__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__25__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__25__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__26__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__26__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__27__Vfuncout = VL_RAND_RESET_I(32);
    __Vfunc_v__DOT__mix_col__27__s0 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__27__s1 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__27__s2 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__27__s3 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__28__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__28__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__29__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__29__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__30__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__30__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__31__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__31__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__32__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__32__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__33__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__33__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__34__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__34__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__35__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__35__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__36__Vfuncout = VL_RAND_RESET_I(32);
    __Vfunc_v__DOT__mix_col__36__s0 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__36__s1 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__36__s2 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__36__s3 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__37__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__37__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__38__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__38__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__39__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__39__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__40__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__40__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__41__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__41__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__42__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__42__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__43__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__43__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__44__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__44__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__45__Vfuncout = VL_RAND_RESET_I(32);
    __Vfunc_v__DOT__mix_col__45__s0 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__45__s1 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__45__s2 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__45__s3 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__46__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__46__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__47__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__47__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__48__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__48__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__49__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__49__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__50__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__50__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__51__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__51__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__52__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__52__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__53__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__53__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__54__Vfuncout = VL_RAND_RESET_I(32);
    __Vfunc_v__DOT__mix_col__54__s0 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__54__s1 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__54__s2 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__54__s3 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__55__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__55__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__56__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__56__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__57__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__57__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__58__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__58__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__59__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__59__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__60__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__60__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__61__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__61__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__62__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__62__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__63__Vfuncout = VL_RAND_RESET_I(32);
    __Vfunc_v__DOT__mix_col__63__s0 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__63__s1 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__63__s2 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__63__s3 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__64__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__64__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__65__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__65__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__66__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__66__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__67__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__67__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__68__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__68__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__69__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__69__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__70__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__70__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__71__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__71__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__72__Vfuncout = VL_RAND_RESET_I(32);
    __Vfunc_v__DOT__mix_col__72__s0 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__72__s1 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__72__s2 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__72__s3 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__73__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__73__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__74__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__74__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__75__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__75__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__76__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__76__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__77__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__77__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__78__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__78__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__79__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__79__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__80__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__80__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__81__Vfuncout = VL_RAND_RESET_I(32);
    __Vfunc_v__DOT__mix_col__81__s0 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__81__s1 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__81__s2 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__81__s3 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__82__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__82__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__83__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__83__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__84__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__84__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__85__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__85__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__86__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__86__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__87__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__87__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__88__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__88__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__89__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__89__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__90__Vfuncout = VL_RAND_RESET_I(32);
    __Vfunc_v__DOT__mix_col__90__s0 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__90__s1 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__90__s2 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__90__s3 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__91__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__91__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__92__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__92__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__93__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__93__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__94__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__94__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__95__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__95__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__96__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__96__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__97__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__97__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__98__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__98__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__99__Vfuncout = VL_RAND_RESET_I(32);
    __Vfunc_v__DOT__mix_col__99__s0 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__99__s1 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__99__s2 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__99__s3 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__100__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__100__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__101__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__101__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__102__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__102__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__103__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__103__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__104__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__104__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__105__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__105__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__106__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__106__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__107__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__107__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__108__Vfuncout = VL_RAND_RESET_I(32);
    __Vfunc_v__DOT__mix_col__108__s0 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__108__s1 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__108__s2 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__108__s3 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__109__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__109__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__110__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__110__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__111__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__111__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__112__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__112__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__113__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__113__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__114__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__114__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__115__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__115__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__116__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__116__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__117__Vfuncout = VL_RAND_RESET_I(32);
    __Vfunc_v__DOT__mix_col__117__s0 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__117__s1 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__117__s2 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__117__s3 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__118__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__118__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__119__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__119__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__120__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__120__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__121__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__121__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__122__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__122__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__123__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__123__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__124__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__124__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__125__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__125__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__126__Vfuncout = VL_RAND_RESET_I(32);
    __Vfunc_v__DOT__mix_col__126__s0 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__126__s1 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__126__s2 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__126__s3 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__127__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__127__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__128__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__128__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__129__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__129__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__130__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__130__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__131__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__131__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__132__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__132__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__133__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__133__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__134__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__134__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__135__Vfuncout = VL_RAND_RESET_I(32);
    __Vfunc_v__DOT__mix_col__135__s0 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__135__s1 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__135__s2 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__mix_col__135__s3 = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__136__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__136__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__137__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__137__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__138__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__138__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__139__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__139__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__140__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__140__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__141__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__141__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__142__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__142__b = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__143__Vfuncout = VL_RAND_RESET_I(8);
    __Vfunc_v__DOT__xtime__143__b = VL_RAND_RESET_I(8);
    __Vclklast__TOP__clk = VL_RAND_RESET_I(1);
    __Vm_traceActivity = VL_RAND_RESET_I(32);
}

void Vaes_cipher_top::__Vconfigure(Vaes_cipher_top__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

Vaes_cipher_top::~Vaes_cipher_top() {
    delete __VlSymsp; __VlSymsp=NULL;
}

//--------------------


void Vaes_cipher_top::eval() {
    Vaes_cipher_top__Syms* __restrict vlSymsp = this->__VlSymsp; // Setup global symbol table
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    VL_DEBUG_IF(VL_PRINTF("\n----TOP Evaluate Vaes_cipher_top::eval\n"); );
    int __VclockLoop = 0;
    IData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	VL_DEBUG_IF(VL_PRINTF(" Clock loop\n"););
	vlSymsp->__Vm_activity = true;
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't converge");
    }
}

void Vaes_cipher_top::_eval_initial_loop(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    vlSymsp->__Vm_activity = true;
    int __VclockLoop = 0;
    IData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	_eval_settle(vlSymsp);
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't DC converge");
    }
}

//--------------------
// Internal Methods

void Vaes_cipher_top::_sequent__TOP__1(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vaes_cipher_top::_sequent__TOP__1\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    VL_SIG8(__Vfunc_v__DOT__u0__DOT__r0__DOT__frcon__144__i,3,0);
    VL_SIG8(__Vdly__v__DOT__dcnt,3,0);
    VL_SIG8(__Vdly__v__DOT__u0__DOT__r0__DOT__rcnt,3,0);
    //char	__VpadToAlign7[1];
    VL_SIG(__Vfunc_v__DOT__u0__DOT__r0__DOT__frcon__144__Vfuncout,31,0);
    VL_SIG(__Vdlyvval__v__DOT__u0__DOT__w__v0,31,0);
    VL_SIG(__Vdlyvval__v__DOT__u0__DOT__w__v1,31,0);
    VL_SIG(__Vdlyvval__v__DOT__u0__DOT__w__v2,31,0);
    VL_SIG(__Vdlyvval__v__DOT__u0__DOT__w__v3,31,0);
    // Body
    __Vdly__v__DOT__u0__DOT__r0__DOT__rcnt = vlTOPp->v__DOT__u0__DOT__r0__DOT__rcnt;
    __Vdly__v__DOT__dcnt = vlTOPp->v__DOT__dcnt;
    // ALWAYS at aes/aes_rcon.v:78
    __Vdly__v__DOT__u0__DOT__r0__DOT__rcnt = (0xf & 
					      ((IData)(vlTOPp->ld)
					        ? 0
					        : ((IData)(1) 
						   + (IData)(vlTOPp->v__DOT__u0__DOT__r0__DOT__rcnt))));
    // ALWAYS at aes/aes_cipher_top.v:123
    vlTOPp->done = (1 & (((~ (IData)((0 != (7 & ((IData)(vlTOPp->v__DOT__dcnt) 
						 >> 1))))) 
			  & (IData)(vlTOPp->v__DOT__dcnt)) 
			 & (~ (IData)(vlTOPp->ld))));
    // ALWAYS at aes/aes_cipher_top.v:116
    if (vlTOPp->rst) {
	__Vdly__v__DOT__dcnt = 0;
    } else {
	if (vlTOPp->ld) {
	    __Vdly__v__DOT__dcnt = 0xb;
	} else {
	    if ((0 != (IData)(vlTOPp->v__DOT__dcnt))) {
		__Vdly__v__DOT__dcnt = (0xf & ((IData)(vlTOPp->v__DOT__dcnt) 
					       - (IData)(1)));
	    }
	}
    }
    // ALWAYS at aes/aes_cipher_top.v:132
    vlTOPp->v__DOT__sa33 = (0xff & ((IData)(vlTOPp->v__DOT__ld_r)
				     ? (vlTOPp->v__DOT__text_in_r[0] 
					^ vlTOPp->v__DOT__w3)
				     : ((IData)(vlTOPp->v__DOT__sa33_mc) 
					^ vlTOPp->v__DOT__w3)));
    // ALWAYS at aes/aes_cipher_top.v:133
    vlTOPp->v__DOT__sa23 = (0xff & ((IData)(vlTOPp->v__DOT__ld_r)
				     ? (((vlTOPp->v__DOT__text_in_r[1] 
					  << 0x18) 
					 | (vlTOPp->v__DOT__text_in_r[0] 
					    >> 8)) 
					^ (vlTOPp->v__DOT__w3 
					   >> 8)) : 
				    ((IData)(vlTOPp->v__DOT__sa23_mc) 
				     ^ (vlTOPp->v__DOT__w3 
					>> 8))));
    // ALWAYS at aes/aes_cipher_top.v:134
    vlTOPp->v__DOT__sa13 = (0xff & ((IData)(vlTOPp->v__DOT__ld_r)
				     ? (((vlTOPp->v__DOT__text_in_r[1] 
					  << 0x10) 
					 | (vlTOPp->v__DOT__text_in_r[0] 
					    >> 0x10)) 
					^ (vlTOPp->v__DOT__w3 
					   >> 0x10))
				     : ((IData)(vlTOPp->v__DOT__sa13_mc) 
					^ (vlTOPp->v__DOT__w3 
					   >> 0x10))));
    // ALWAYS at aes/aes_cipher_top.v:135
    vlTOPp->v__DOT__sa03 = (0xff & ((IData)(vlTOPp->v__DOT__ld_r)
				     ? (((vlTOPp->v__DOT__text_in_r[1] 
					  << 8) | (
						   vlTOPp->v__DOT__text_in_r[0] 
						   >> 0x18)) 
					^ (vlTOPp->v__DOT__w3 
					   >> 0x18))
				     : ((IData)(vlTOPp->v__DOT__sa03_mc) 
					^ (vlTOPp->v__DOT__w3 
					   >> 0x18))));
    // ALWAYS at aes/aes_cipher_top.v:136
    vlTOPp->v__DOT__sa32 = (0xff & ((IData)(vlTOPp->v__DOT__ld_r)
				     ? (vlTOPp->v__DOT__text_in_r[1] 
					^ vlTOPp->v__DOT__w2)
				     : ((IData)(vlTOPp->v__DOT__sa32_mc) 
					^ vlTOPp->v__DOT__w2)));
    // ALWAYS at aes/aes_cipher_top.v:137
    vlTOPp->v__DOT__sa22 = (0xff & ((IData)(vlTOPp->v__DOT__ld_r)
				     ? (((vlTOPp->v__DOT__text_in_r[2] 
					  << 0x18) 
					 | (vlTOPp->v__DOT__text_in_r[1] 
					    >> 8)) 
					^ (vlTOPp->v__DOT__w2 
					   >> 8)) : 
				    ((IData)(vlTOPp->v__DOT__sa22_mc) 
				     ^ (vlTOPp->v__DOT__w2 
					>> 8))));
    // ALWAYS at aes/aes_cipher_top.v:138
    vlTOPp->v__DOT__sa12 = (0xff & ((IData)(vlTOPp->v__DOT__ld_r)
				     ? (((vlTOPp->v__DOT__text_in_r[2] 
					  << 0x10) 
					 | (vlTOPp->v__DOT__text_in_r[1] 
					    >> 0x10)) 
					^ (vlTOPp->v__DOT__w2 
					   >> 0x10))
				     : ((IData)(vlTOPp->v__DOT__sa12_mc) 
					^ (vlTOPp->v__DOT__w2 
					   >> 0x10))));
    // ALWAYS at aes/aes_cipher_top.v:139
    vlTOPp->v__DOT__sa02 = (0xff & ((IData)(vlTOPp->v__DOT__ld_r)
				     ? (((vlTOPp->v__DOT__text_in_r[2] 
					  << 8) | (
						   vlTOPp->v__DOT__text_in_r[1] 
						   >> 0x18)) 
					^ (vlTOPp->v__DOT__w2 
					   >> 0x18))
				     : ((IData)(vlTOPp->v__DOT__sa02_mc) 
					^ (vlTOPp->v__DOT__w2 
					   >> 0x18))));
    // ALWAYS at aes/aes_cipher_top.v:140
    vlTOPp->v__DOT__sa31 = (0xff & ((IData)(vlTOPp->v__DOT__ld_r)
				     ? (vlTOPp->v__DOT__text_in_r[2] 
					^ vlTOPp->v__DOT__w1)
				     : ((IData)(vlTOPp->v__DOT__sa31_mc) 
					^ vlTOPp->v__DOT__w1)));
    // ALWAYS at aes/aes_cipher_top.v:141
    vlTOPp->v__DOT__sa21 = (0xff & ((IData)(vlTOPp->v__DOT__ld_r)
				     ? (((vlTOPp->v__DOT__text_in_r[3] 
					  << 0x18) 
					 | (vlTOPp->v__DOT__text_in_r[2] 
					    >> 8)) 
					^ (vlTOPp->v__DOT__w1 
					   >> 8)) : 
				    ((IData)(vlTOPp->v__DOT__sa21_mc) 
				     ^ (vlTOPp->v__DOT__w1 
					>> 8))));
    // ALWAYS at aes/aes_cipher_top.v:142
    vlTOPp->v__DOT__sa11 = (0xff & ((IData)(vlTOPp->v__DOT__ld_r)
				     ? (((vlTOPp->v__DOT__text_in_r[3] 
					  << 0x10) 
					 | (vlTOPp->v__DOT__text_in_r[2] 
					    >> 0x10)) 
					^ (vlTOPp->v__DOT__w1 
					   >> 0x10))
				     : ((IData)(vlTOPp->v__DOT__sa11_mc) 
					^ (vlTOPp->v__DOT__w1 
					   >> 0x10))));
    // ALWAYS at aes/aes_cipher_top.v:143
    vlTOPp->v__DOT__sa01 = (0xff & ((IData)(vlTOPp->v__DOT__ld_r)
				     ? (((vlTOPp->v__DOT__text_in_r[3] 
					  << 8) | (
						   vlTOPp->v__DOT__text_in_r[2] 
						   >> 0x18)) 
					^ (vlTOPp->v__DOT__w1 
					   >> 0x18))
				     : ((IData)(vlTOPp->v__DOT__sa01_mc) 
					^ (vlTOPp->v__DOT__w1 
					   >> 0x18))));
    // ALWAYS at aes/aes_cipher_top.v:144
    vlTOPp->v__DOT__sa30 = (0xff & ((IData)(vlTOPp->v__DOT__ld_r)
				     ? (vlTOPp->v__DOT__text_in_r[3] 
					^ vlTOPp->v__DOT__w0)
				     : ((IData)(vlTOPp->v__DOT__sa30_mc) 
					^ vlTOPp->v__DOT__w0)));
    // ALWAYS at aes/aes_cipher_top.v:145
    vlTOPp->v__DOT__sa20 = (0xff & ((IData)(vlTOPp->v__DOT__ld_r)
				     ? ((vlTOPp->v__DOT__text_in_r[3] 
					 >> 8) ^ (vlTOPp->v__DOT__w0 
						  >> 8))
				     : ((IData)(vlTOPp->v__DOT__sa20_mc) 
					^ (vlTOPp->v__DOT__w0 
					   >> 8))));
    // ALWAYS at aes/aes_cipher_top.v:146
    vlTOPp->v__DOT__sa10 = (0xff & ((IData)(vlTOPp->v__DOT__ld_r)
				     ? ((vlTOPp->v__DOT__text_in_r[3] 
					 >> 0x10) ^ 
					(vlTOPp->v__DOT__w0 
					 >> 0x10)) : 
				    ((IData)(vlTOPp->v__DOT__sa10_mc) 
				     ^ (vlTOPp->v__DOT__w0 
					>> 0x10))));
    // ALWAYS at aes/aes_cipher_top.v:147
    vlTOPp->v__DOT__sa00 = (0xff & ((IData)(vlTOPp->v__DOT__ld_r)
				     ? ((vlTOPp->v__DOT__text_in_r[3] 
					 >> 0x18) ^ 
					(vlTOPp->v__DOT__w0 
					 >> 0x18)) : 
				    ((IData)(vlTOPp->v__DOT__sa00_mc) 
				     ^ (vlTOPp->v__DOT__w0 
					>> 0x18))));
    // ALWAYS at aes/aes_key_expand_128.v:79
    __Vdlyvval__v__DOT__u0__DOT__w__v0 = ((IData)(vlTOPp->ld)
					   ? vlTOPp->key[3]
					   : ((vlTOPp->v__DOT__u0__DOT__w
					       [0] 
					       ^ vlTOPp->v__DOT__u0__DOT__subword) 
					      ^ vlTOPp->v__DOT__u0__DOT__rcon));
    // ALWAYS at aes/aes_key_expand_128.v:80
    __Vdlyvval__v__DOT__u0__DOT__w__v1 = ((IData)(vlTOPp->ld)
					   ? vlTOPp->key[2]
					   : (((vlTOPp->v__DOT__u0__DOT__w
						[0] 
						^ vlTOPp->v__DOT__u0__DOT__w
						[1]) 
					       ^ vlTOPp->v__DOT__u0__DOT__subword) 
					      ^ vlTOPp->v__DOT__u0__DOT__rcon));
    // ALWAYS at aes/aes_key_expand_128.v:81
    __Vdlyvval__v__DOT__u0__DOT__w__v2 = ((IData)(vlTOPp->ld)
					   ? vlTOPp->key[1]
					   : ((((vlTOPp->v__DOT__u0__DOT__w
						 [0] 
						 ^ 
						 vlTOPp->v__DOT__u0__DOT__w
						 [2]) 
						^ vlTOPp->v__DOT__u0__DOT__w
						[1]) 
					       ^ vlTOPp->v__DOT__u0__DOT__subword) 
					      ^ vlTOPp->v__DOT__u0__DOT__rcon));
    // ALWAYS at aes/aes_key_expand_128.v:82
    __Vdlyvval__v__DOT__u0__DOT__w__v3 = ((IData)(vlTOPp->ld)
					   ? vlTOPp->key[0]
					   : (((((vlTOPp->v__DOT__u0__DOT__w
						  [0] 
						  ^ 
						  vlTOPp->v__DOT__u0__DOT__w
						  [3]) 
						 ^ 
						 vlTOPp->v__DOT__u0__DOT__w
						 [2]) 
						^ vlTOPp->v__DOT__u0__DOT__w
						[1]) 
					       ^ vlTOPp->v__DOT__u0__DOT__subword) 
					      ^ vlTOPp->v__DOT__u0__DOT__rcon));
    // ALWAYS at aes/aes_cipher_top.v:196
    vlTOPp->text_out[3] = ((0xffffff & vlTOPp->text_out[3]) 
			   | (0xff000000 & (((IData)(vlSymsp->TOP__v__DOT__us00.d) 
					     << 0x18) 
					    ^ (0xff000000 
					       & vlTOPp->v__DOT__w0))));
    // ALWAYS at aes/aes_cipher_top.v:197
    vlTOPp->text_out[2] = ((0xffffff & vlTOPp->text_out[2]) 
			   | (0xff000000 & (((IData)(vlSymsp->TOP__v__DOT__us01.d) 
					     << 0x18) 
					    ^ (0xff000000 
					       & vlTOPp->v__DOT__w1))));
    // ALWAYS at aes/aes_cipher_top.v:198
    vlTOPp->text_out[1] = ((0xffffff & vlTOPp->text_out[1]) 
			   | (0xff000000 & (((IData)(vlSymsp->TOP__v__DOT__us02.d) 
					     << 0x18) 
					    ^ (0xff000000 
					       & vlTOPp->v__DOT__w2))));
    // ALWAYS at aes/aes_cipher_top.v:199
    vlTOPp->text_out[0] = ((0xffffff & vlTOPp->text_out[0]) 
			   | (0xff000000 & (((IData)(vlSymsp->TOP__v__DOT__us03.d) 
					     << 0x18) 
					    ^ (0xff000000 
					       & vlTOPp->v__DOT__w3))));
    // ALWAYS at aes/aes_cipher_top.v:200
    vlTOPp->text_out[3] = ((0xff00ffff & vlTOPp->text_out[3]) 
			   | (0xff0000 & (((IData)(vlSymsp->TOP__v__DOT__us11.d) 
					   << 0x10) 
					  ^ (0xffff0000 
					     & vlTOPp->v__DOT__w0))));
    // ALWAYS at aes/aes_cipher_top.v:201
    vlTOPp->text_out[2] = ((0xff00ffff & vlTOPp->text_out[2]) 
			   | (0xff0000 & (((IData)(vlSymsp->TOP__v__DOT__us12.d) 
					   << 0x10) 
					  ^ (0xffff0000 
					     & vlTOPp->v__DOT__w1))));
    // ALWAYS at aes/aes_cipher_top.v:202
    vlTOPp->text_out[1] = ((0xff00ffff & vlTOPp->text_out[1]) 
			   | (0xff0000 & (((IData)(vlSymsp->TOP__v__DOT__us13.d) 
					   << 0x10) 
					  ^ (0xffff0000 
					     & vlTOPp->v__DOT__w2))));
    // ALWAYS at aes/aes_cipher_top.v:203
    vlTOPp->text_out[0] = ((0xff00ffff & vlTOPp->text_out[0]) 
			   | (0xff0000 & (((IData)(vlSymsp->TOP__v__DOT__us10.d) 
					   << 0x10) 
					  ^ (0xffff0000 
					     & vlTOPp->v__DOT__w3))));
    // ALWAYS at aes/aes_cipher_top.v:204
    vlTOPp->text_out[3] = ((0xffff00ff & vlTOPp->text_out[3]) 
			   | (0xff00 & (((IData)(vlSymsp->TOP__v__DOT__us22.d) 
					 << 8) ^ (0xffffff00 
						  & vlTOPp->v__DOT__w0))));
    // ALWAYS at aes/aes_cipher_top.v:205
    vlTOPp->text_out[2] = ((0xffff00ff & vlTOPp->text_out[2]) 
			   | (0xff00 & (((IData)(vlSymsp->TOP__v__DOT__us23.d) 
					 << 8) ^ (0xffffff00 
						  & vlTOPp->v__DOT__w1))));
    // ALWAYS at aes/aes_cipher_top.v:206
    vlTOPp->text_out[1] = ((0xffff00ff & vlTOPp->text_out[1]) 
			   | (0xff00 & (((IData)(vlSymsp->TOP__v__DOT__us20.d) 
					 << 8) ^ (0xffffff00 
						  & vlTOPp->v__DOT__w2))));
    // ALWAYS at aes/aes_cipher_top.v:207
    vlTOPp->text_out[0] = ((0xffff00ff & vlTOPp->text_out[0]) 
			   | (0xff00 & (((IData)(vlSymsp->TOP__v__DOT__us21.d) 
					 << 8) ^ (0xffffff00 
						  & vlTOPp->v__DOT__w3))));
    // ALWAYS at aes/aes_cipher_top.v:208
    vlTOPp->text_out[3] = ((0xffffff00 & vlTOPp->text_out[3]) 
			   | (0xff & ((IData)(vlSymsp->TOP__v__DOT__us33.d) 
				      ^ vlTOPp->v__DOT__w0)));
    // ALWAYS at aes/aes_cipher_top.v:209
    vlTOPp->text_out[2] = ((0xffffff00 & vlTOPp->text_out[2]) 
			   | (0xff & ((IData)(vlSymsp->TOP__v__DOT__us30.d) 
				      ^ vlTOPp->v__DOT__w1)));
    // ALWAYS at aes/aes_cipher_top.v:210
    vlTOPp->text_out[1] = ((0xffffff00 & vlTOPp->text_out[1]) 
			   | (0xff & ((IData)(vlSymsp->TOP__v__DOT__us31.d) 
				      ^ vlTOPp->v__DOT__w2)));
    // ALWAYS at aes/aes_cipher_top.v:211
    vlTOPp->text_out[0] = ((0xffffff00 & vlTOPp->text_out[0]) 
			   | (0xff & ((IData)(vlSymsp->TOP__v__DOT__us32.d) 
				      ^ vlTOPp->v__DOT__w3)));
    vlTOPp->v__DOT__dcnt = __Vdly__v__DOT__dcnt;
    // ALWAYSPOST at aes/aes_key_expand_128.v:79
    vlTOPp->v__DOT__u0__DOT__w[0] = __Vdlyvval__v__DOT__u0__DOT__w__v0;
    vlTOPp->v__DOT__u0__DOT__w[1] = __Vdlyvval__v__DOT__u0__DOT__w__v1;
    vlTOPp->v__DOT__u0__DOT__w[2] = __Vdlyvval__v__DOT__u0__DOT__w__v2;
    vlTOPp->v__DOT__u0__DOT__w[3] = __Vdlyvval__v__DOT__u0__DOT__w__v3;
    // ALWAYS at aes/aes_cipher_top.v:125
    vlTOPp->v__DOT__ld_r = vlTOPp->ld;
    // ALWAYS at aes/aes_cipher_top.v:124
    if (vlTOPp->ld) {
	vlTOPp->v__DOT__text_in_r[0] = vlTOPp->text_in[0];
	vlTOPp->v__DOT__text_in_r[1] = vlTOPp->text_in[1];
	vlTOPp->v__DOT__text_in_r[2] = vlTOPp->text_in[2];
	vlTOPp->v__DOT__text_in_r[3] = vlTOPp->text_in[3];
    }
    // ALWAYS at aes/aes_rcon.v:75
    __Vfunc_v__DOT__u0__DOT__r0__DOT__frcon__144__i 
	= (0xf & ((IData)(1) + (IData)(vlTOPp->v__DOT__u0__DOT__r0__DOT__rcnt)));
    __Vfunc_v__DOT__u0__DOT__r0__DOT__frcon__144__Vfuncout 
	= ((8 & (IData)(__Vfunc_v__DOT__u0__DOT__r0__DOT__frcon__144__i))
	    ? ((4 & (IData)(__Vfunc_v__DOT__u0__DOT__r0__DOT__frcon__144__i))
	        ? 0 : ((2 & (IData)(__Vfunc_v__DOT__u0__DOT__r0__DOT__frcon__144__i))
		        ? 0 : ((1 & (IData)(__Vfunc_v__DOT__u0__DOT__r0__DOT__frcon__144__i))
			        ? 0x36000000 : 0x1b000000)))
	    : ((4 & (IData)(__Vfunc_v__DOT__u0__DOT__r0__DOT__frcon__144__i))
	        ? ((2 & (IData)(__Vfunc_v__DOT__u0__DOT__r0__DOT__frcon__144__i))
		    ? ((1 & (IData)(__Vfunc_v__DOT__u0__DOT__r0__DOT__frcon__144__i))
		        ? 0x80000000 : 0x40000000) : 
		   ((1 & (IData)(__Vfunc_v__DOT__u0__DOT__r0__DOT__frcon__144__i))
		     ? 0x20000000 : 0x10000000)) : 
	       ((2 & (IData)(__Vfunc_v__DOT__u0__DOT__r0__DOT__frcon__144__i))
		 ? ((1 & (IData)(__Vfunc_v__DOT__u0__DOT__r0__DOT__frcon__144__i))
		     ? 0x8000000 : 0x4000000) : ((1 
						  & (IData)(__Vfunc_v__DOT__u0__DOT__r0__DOT__frcon__144__i))
						  ? 0x2000000
						  : 0x1000000))));
    vlTOPp->v__DOT__u0__DOT__rcon = ((IData)(vlTOPp->ld)
				      ? 0x1000000 : __Vfunc_v__DOT__u0__DOT__r0__DOT__frcon__144__Vfuncout);
    vlTOPp->v__DOT__w0 = vlTOPp->v__DOT__u0__DOT__w
	[0];
    vlTOPp->v__DOT__w1 = vlTOPp->v__DOT__u0__DOT__w
	[1];
    vlTOPp->v__DOT__w2 = vlTOPp->v__DOT__u0__DOT__w
	[2];
    vlTOPp->v__DOT__w3 = vlTOPp->v__DOT__u0__DOT__w
	[3];
    vlTOPp->v__DOT__u0__DOT__tmp_w = vlTOPp->v__DOT__u0__DOT__w
	[3];
    vlTOPp->v__DOT__u0__DOT__r0__DOT__rcnt = __Vdly__v__DOT__u0__DOT__r0__DOT__rcnt;
}

void Vaes_cipher_top::_settle__TOP__2(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vaes_cipher_top::_settle__TOP__2\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__w0 = vlTOPp->v__DOT__u0__DOT__w
	[0];
    vlTOPp->v__DOT__w1 = vlTOPp->v__DOT__u0__DOT__w
	[1];
    vlTOPp->v__DOT__w2 = vlTOPp->v__DOT__u0__DOT__w
	[2];
    vlTOPp->v__DOT__w3 = vlTOPp->v__DOT__u0__DOT__w
	[3];
    vlTOPp->v__DOT__u0__DOT__tmp_w = vlTOPp->v__DOT__u0__DOT__w
	[3];
    // ALWAYS at aes/aes_cipher_top.v:171
    vlTOPp->__Vfunc_v__DOT__mix_col__36__s3 = vlSymsp->TOP__v__DOT__us30.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__36__s2 = vlSymsp->TOP__v__DOT__us23.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__36__s1 = vlSymsp->TOP__v__DOT__us12.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__36__s0 = vlSymsp->TOP__v__DOT__us01.d;
    vlTOPp->__Vfunc_v__DOT__xtime__38__b = vlTOPp->__Vfunc_v__DOT__mix_col__36__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__38__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__38__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__38__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__37__b = vlTOPp->__Vfunc_v__DOT__mix_col__36__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__37__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__37__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__37__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__36__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__36__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__37__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__38__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__40__b = vlTOPp->__Vfunc_v__DOT__mix_col__36__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__40__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__40__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__40__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__39__b = vlTOPp->__Vfunc_v__DOT__mix_col__36__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__39__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__39__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__39__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__36__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__36__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__39__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__40__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__42__b = vlTOPp->__Vfunc_v__DOT__mix_col__36__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__42__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__42__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__42__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__41__b = vlTOPp->__Vfunc_v__DOT__mix_col__36__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__41__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__41__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__41__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__36__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__36__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__41__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__42__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__44__b = vlTOPp->__Vfunc_v__DOT__mix_col__36__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__44__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__44__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__44__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__43__b = vlTOPp->__Vfunc_v__DOT__mix_col__36__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__43__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__43__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__43__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__36__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__36__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__43__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__44__Vfuncout)));
    vlTOPp->v__DOT__sa01_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__36__Vfuncout 
				       >> 0x18));
    // ALWAYS at aes/aes_cipher_top.v:171
    vlTOPp->__Vfunc_v__DOT__mix_col__45__s3 = vlSymsp->TOP__v__DOT__us30.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__45__s2 = vlSymsp->TOP__v__DOT__us23.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__45__s1 = vlSymsp->TOP__v__DOT__us12.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__45__s0 = vlSymsp->TOP__v__DOT__us01.d;
    vlTOPp->__Vfunc_v__DOT__xtime__47__b = vlTOPp->__Vfunc_v__DOT__mix_col__45__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__47__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__47__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__47__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__46__b = vlTOPp->__Vfunc_v__DOT__mix_col__45__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__46__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__46__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__46__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__45__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__45__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__46__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__47__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__49__b = vlTOPp->__Vfunc_v__DOT__mix_col__45__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__49__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__49__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__49__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__48__b = vlTOPp->__Vfunc_v__DOT__mix_col__45__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__48__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__48__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__48__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__45__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__45__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__48__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__49__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__51__b = vlTOPp->__Vfunc_v__DOT__mix_col__45__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__51__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__51__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__51__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__50__b = vlTOPp->__Vfunc_v__DOT__mix_col__45__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__50__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__50__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__50__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__45__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__45__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__50__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__51__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__53__b = vlTOPp->__Vfunc_v__DOT__mix_col__45__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__53__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__53__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__53__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__52__b = vlTOPp->__Vfunc_v__DOT__mix_col__45__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__52__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__52__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__52__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__45__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__45__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__52__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__53__Vfuncout)));
    vlTOPp->v__DOT__sa11_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__45__Vfuncout 
				       >> 0x10));
    // ALWAYS at aes/aes_cipher_top.v:171
    vlTOPp->__Vfunc_v__DOT__mix_col__54__s3 = vlSymsp->TOP__v__DOT__us30.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__54__s2 = vlSymsp->TOP__v__DOT__us23.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__54__s1 = vlSymsp->TOP__v__DOT__us12.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__54__s0 = vlSymsp->TOP__v__DOT__us01.d;
    vlTOPp->__Vfunc_v__DOT__xtime__56__b = vlTOPp->__Vfunc_v__DOT__mix_col__54__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__56__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__56__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__56__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__55__b = vlTOPp->__Vfunc_v__DOT__mix_col__54__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__55__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__55__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__55__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__54__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__54__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__55__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__56__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__58__b = vlTOPp->__Vfunc_v__DOT__mix_col__54__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__58__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__58__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__58__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__57__b = vlTOPp->__Vfunc_v__DOT__mix_col__54__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__57__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__57__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__57__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__54__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__54__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__57__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__58__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__60__b = vlTOPp->__Vfunc_v__DOT__mix_col__54__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__60__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__60__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__60__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__59__b = vlTOPp->__Vfunc_v__DOT__mix_col__54__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__59__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__59__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__59__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__54__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__54__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__59__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__60__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__62__b = vlTOPp->__Vfunc_v__DOT__mix_col__54__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__62__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__62__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__62__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__61__b = vlTOPp->__Vfunc_v__DOT__mix_col__54__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__61__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__61__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__61__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__54__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__54__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__61__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__62__Vfuncout)));
    vlTOPp->v__DOT__sa21_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__54__Vfuncout 
				       >> 8));
    // ALWAYS at aes/aes_cipher_top.v:171
    vlTOPp->__Vfunc_v__DOT__mix_col__63__s3 = vlSymsp->TOP__v__DOT__us30.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__63__s2 = vlSymsp->TOP__v__DOT__us23.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__63__s1 = vlSymsp->TOP__v__DOT__us12.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__63__s0 = vlSymsp->TOP__v__DOT__us01.d;
    vlTOPp->__Vfunc_v__DOT__xtime__65__b = vlTOPp->__Vfunc_v__DOT__mix_col__63__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__65__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__65__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__65__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__64__b = vlTOPp->__Vfunc_v__DOT__mix_col__63__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__64__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__64__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__64__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__63__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__63__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__64__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__65__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__67__b = vlTOPp->__Vfunc_v__DOT__mix_col__63__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__67__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__67__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__67__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__66__b = vlTOPp->__Vfunc_v__DOT__mix_col__63__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__66__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__66__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__66__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__63__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__63__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__66__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__67__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__69__b = vlTOPp->__Vfunc_v__DOT__mix_col__63__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__69__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__69__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__69__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__68__b = vlTOPp->__Vfunc_v__DOT__mix_col__63__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__68__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__68__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__68__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__63__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__63__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__68__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__69__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__71__b = vlTOPp->__Vfunc_v__DOT__mix_col__63__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__71__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__71__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__71__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__70__b = vlTOPp->__Vfunc_v__DOT__mix_col__63__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__70__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__70__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__70__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__63__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__63__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__70__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__71__Vfuncout)));
    vlTOPp->v__DOT__sa31_mc = (0xff & vlTOPp->__Vfunc_v__DOT__mix_col__63__Vfuncout);
    // ALWAYS at aes/aes_cipher_top.v:172
    vlTOPp->__Vfunc_v__DOT__mix_col__72__s3 = vlSymsp->TOP__v__DOT__us31.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__72__s2 = vlSymsp->TOP__v__DOT__us20.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__72__s1 = vlSymsp->TOP__v__DOT__us13.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__72__s0 = vlSymsp->TOP__v__DOT__us02.d;
    vlTOPp->__Vfunc_v__DOT__xtime__74__b = vlTOPp->__Vfunc_v__DOT__mix_col__72__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__74__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__74__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__74__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__73__b = vlTOPp->__Vfunc_v__DOT__mix_col__72__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__73__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__73__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__73__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__72__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__72__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__73__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__74__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__76__b = vlTOPp->__Vfunc_v__DOT__mix_col__72__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__76__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__76__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__76__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__75__b = vlTOPp->__Vfunc_v__DOT__mix_col__72__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__75__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__75__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__75__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__72__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__72__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__75__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__76__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__78__b = vlTOPp->__Vfunc_v__DOT__mix_col__72__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__78__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__78__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__78__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__77__b = vlTOPp->__Vfunc_v__DOT__mix_col__72__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__77__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__77__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__77__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__72__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__72__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__77__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__78__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__80__b = vlTOPp->__Vfunc_v__DOT__mix_col__72__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__80__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__80__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__80__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__79__b = vlTOPp->__Vfunc_v__DOT__mix_col__72__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__79__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__79__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__79__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__72__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__72__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__79__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__80__Vfuncout)));
    vlTOPp->v__DOT__sa02_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__72__Vfuncout 
				       >> 0x18));
    // ALWAYS at aes/aes_cipher_top.v:172
    vlTOPp->__Vfunc_v__DOT__mix_col__81__s3 = vlSymsp->TOP__v__DOT__us31.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__81__s2 = vlSymsp->TOP__v__DOT__us20.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__81__s1 = vlSymsp->TOP__v__DOT__us13.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__81__s0 = vlSymsp->TOP__v__DOT__us02.d;
    vlTOPp->__Vfunc_v__DOT__xtime__83__b = vlTOPp->__Vfunc_v__DOT__mix_col__81__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__83__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__83__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__83__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__82__b = vlTOPp->__Vfunc_v__DOT__mix_col__81__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__82__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__82__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__82__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__81__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__81__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__82__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__83__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__85__b = vlTOPp->__Vfunc_v__DOT__mix_col__81__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__85__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__85__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__85__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__84__b = vlTOPp->__Vfunc_v__DOT__mix_col__81__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__84__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__84__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__84__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__81__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__81__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__84__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__85__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__87__b = vlTOPp->__Vfunc_v__DOT__mix_col__81__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__87__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__87__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__87__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__86__b = vlTOPp->__Vfunc_v__DOT__mix_col__81__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__86__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__86__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__86__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__81__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__81__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__86__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__87__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__89__b = vlTOPp->__Vfunc_v__DOT__mix_col__81__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__89__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__89__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__89__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__88__b = vlTOPp->__Vfunc_v__DOT__mix_col__81__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__88__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__88__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__88__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__81__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__81__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__88__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__89__Vfuncout)));
    vlTOPp->v__DOT__sa12_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__81__Vfuncout 
				       >> 0x10));
    // ALWAYS at aes/aes_cipher_top.v:172
    vlTOPp->__Vfunc_v__DOT__mix_col__90__s3 = vlSymsp->TOP__v__DOT__us31.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__90__s2 = vlSymsp->TOP__v__DOT__us20.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__90__s1 = vlSymsp->TOP__v__DOT__us13.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__90__s0 = vlSymsp->TOP__v__DOT__us02.d;
    vlTOPp->__Vfunc_v__DOT__xtime__92__b = vlTOPp->__Vfunc_v__DOT__mix_col__90__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__92__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__92__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__92__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__91__b = vlTOPp->__Vfunc_v__DOT__mix_col__90__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__91__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__91__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__91__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__90__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__90__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__91__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__92__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__94__b = vlTOPp->__Vfunc_v__DOT__mix_col__90__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__94__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__94__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__94__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__93__b = vlTOPp->__Vfunc_v__DOT__mix_col__90__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__93__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__93__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__93__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__90__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__90__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__93__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__94__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__96__b = vlTOPp->__Vfunc_v__DOT__mix_col__90__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__96__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__96__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__96__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__95__b = vlTOPp->__Vfunc_v__DOT__mix_col__90__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__95__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__95__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__95__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__90__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__90__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__95__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__96__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__98__b = vlTOPp->__Vfunc_v__DOT__mix_col__90__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__98__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__98__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__98__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__97__b = vlTOPp->__Vfunc_v__DOT__mix_col__90__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__97__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__97__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__97__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__90__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__90__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__97__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__98__Vfuncout)));
    vlTOPp->v__DOT__sa22_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__90__Vfuncout 
				       >> 8));
    // ALWAYS at aes/aes_cipher_top.v:172
    vlTOPp->__Vfunc_v__DOT__mix_col__99__s3 = vlSymsp->TOP__v__DOT__us31.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__99__s2 = vlSymsp->TOP__v__DOT__us20.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__99__s1 = vlSymsp->TOP__v__DOT__us13.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__99__s0 = vlSymsp->TOP__v__DOT__us02.d;
    vlTOPp->__Vfunc_v__DOT__xtime__101__b = vlTOPp->__Vfunc_v__DOT__mix_col__99__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__101__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__101__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__101__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__100__b = vlTOPp->__Vfunc_v__DOT__mix_col__99__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__100__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__100__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__100__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__99__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__99__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__100__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__101__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__103__b = vlTOPp->__Vfunc_v__DOT__mix_col__99__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__103__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__103__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__103__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__102__b = vlTOPp->__Vfunc_v__DOT__mix_col__99__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__102__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__102__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__102__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__99__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__99__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__102__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__103__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__105__b = vlTOPp->__Vfunc_v__DOT__mix_col__99__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__105__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__105__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__105__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__104__b = vlTOPp->__Vfunc_v__DOT__mix_col__99__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__104__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__104__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__104__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__99__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__99__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__104__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__105__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__107__b = vlTOPp->__Vfunc_v__DOT__mix_col__99__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__107__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__107__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__107__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__106__b = vlTOPp->__Vfunc_v__DOT__mix_col__99__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__106__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__106__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__106__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__99__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__99__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__106__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__107__Vfuncout)));
    vlTOPp->v__DOT__sa32_mc = (0xff & vlTOPp->__Vfunc_v__DOT__mix_col__99__Vfuncout);
    // ALWAYS at aes/aes_cipher_top.v:173
    vlTOPp->__Vfunc_v__DOT__mix_col__108__s3 = vlSymsp->TOP__v__DOT__us32.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__108__s2 = vlSymsp->TOP__v__DOT__us21.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__108__s1 = vlSymsp->TOP__v__DOT__us10.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__108__s0 = vlSymsp->TOP__v__DOT__us03.d;
    vlTOPp->__Vfunc_v__DOT__xtime__110__b = vlTOPp->__Vfunc_v__DOT__mix_col__108__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__110__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__110__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__110__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__109__b = vlTOPp->__Vfunc_v__DOT__mix_col__108__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__109__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__109__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__109__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__108__Vfuncout 
	= ((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__108__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__109__Vfuncout) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__110__Vfuncout)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s1)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s2)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s3)) 
	      << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__112__b = vlTOPp->__Vfunc_v__DOT__mix_col__108__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__112__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__112__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__112__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__111__b = vlTOPp->__Vfunc_v__DOT__mix_col__108__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__111__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__111__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__111__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__108__Vfuncout 
	= ((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__108__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s0) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__111__Vfuncout)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__112__Vfuncout)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s2)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s3)) 
	      << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__114__b = vlTOPp->__Vfunc_v__DOT__mix_col__108__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__114__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__114__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__114__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__113__b = vlTOPp->__Vfunc_v__DOT__mix_col__108__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__113__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__113__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__113__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__108__Vfuncout 
	= ((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__108__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s0) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s1)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__113__Vfuncout)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__114__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s3)) 
	      << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__116__b = vlTOPp->__Vfunc_v__DOT__mix_col__108__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__116__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__116__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__116__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__115__b = vlTOPp->__Vfunc_v__DOT__mix_col__108__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__115__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__115__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__115__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__108__Vfuncout 
	= ((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__108__Vfuncout) 
	   | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__115__Vfuncout) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s0)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s2)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__116__Vfuncout)));
    vlTOPp->v__DOT__sa03_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__108__Vfuncout 
				       >> 0x18));
    // ALWAYS at aes/aes_cipher_top.v:173
    vlTOPp->__Vfunc_v__DOT__mix_col__117__s3 = vlSymsp->TOP__v__DOT__us32.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__117__s2 = vlSymsp->TOP__v__DOT__us21.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__117__s1 = vlSymsp->TOP__v__DOT__us10.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__117__s0 = vlSymsp->TOP__v__DOT__us03.d;
    vlTOPp->__Vfunc_v__DOT__xtime__119__b = vlTOPp->__Vfunc_v__DOT__mix_col__117__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__119__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__119__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__119__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__118__b = vlTOPp->__Vfunc_v__DOT__mix_col__117__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__118__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__118__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__118__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__117__Vfuncout 
	= ((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__117__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__118__Vfuncout) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__119__Vfuncout)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s1)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s2)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s3)) 
	      << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__121__b = vlTOPp->__Vfunc_v__DOT__mix_col__117__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__121__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__121__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__121__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__120__b = vlTOPp->__Vfunc_v__DOT__mix_col__117__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__120__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__120__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__120__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__117__Vfuncout 
	= ((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__117__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s0) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__120__Vfuncout)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__121__Vfuncout)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s2)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s3)) 
	      << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__123__b = vlTOPp->__Vfunc_v__DOT__mix_col__117__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__123__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__123__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__123__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__122__b = vlTOPp->__Vfunc_v__DOT__mix_col__117__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__122__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__122__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__122__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__117__Vfuncout 
	= ((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__117__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s0) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s1)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__122__Vfuncout)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__123__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s3)) 
	      << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__125__b = vlTOPp->__Vfunc_v__DOT__mix_col__117__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__125__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__125__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__125__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__124__b = vlTOPp->__Vfunc_v__DOT__mix_col__117__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__124__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__124__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__124__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__117__Vfuncout 
	= ((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__117__Vfuncout) 
	   | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__124__Vfuncout) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s0)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s2)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__125__Vfuncout)));
    vlTOPp->v__DOT__sa13_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__117__Vfuncout 
				       >> 0x10));
    // ALWAYS at aes/aes_cipher_top.v:173
    vlTOPp->__Vfunc_v__DOT__mix_col__126__s3 = vlSymsp->TOP__v__DOT__us32.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__126__s2 = vlSymsp->TOP__v__DOT__us21.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__126__s1 = vlSymsp->TOP__v__DOT__us10.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__126__s0 = vlSymsp->TOP__v__DOT__us03.d;
    vlTOPp->__Vfunc_v__DOT__xtime__128__b = vlTOPp->__Vfunc_v__DOT__mix_col__126__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__128__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__128__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__128__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__127__b = vlTOPp->__Vfunc_v__DOT__mix_col__126__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__127__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__127__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__127__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__126__Vfuncout 
	= ((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__126__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__127__Vfuncout) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__128__Vfuncout)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s1)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s2)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s3)) 
	      << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__130__b = vlTOPp->__Vfunc_v__DOT__mix_col__126__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__130__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__130__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__130__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__129__b = vlTOPp->__Vfunc_v__DOT__mix_col__126__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__129__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__129__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__129__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__126__Vfuncout 
	= ((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__126__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s0) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__129__Vfuncout)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__130__Vfuncout)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s2)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s3)) 
	      << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__132__b = vlTOPp->__Vfunc_v__DOT__mix_col__126__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__132__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__132__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__132__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__131__b = vlTOPp->__Vfunc_v__DOT__mix_col__126__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__131__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__131__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__131__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__126__Vfuncout 
	= ((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__126__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s0) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s1)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__131__Vfuncout)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__132__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s3)) 
	      << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__134__b = vlTOPp->__Vfunc_v__DOT__mix_col__126__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__134__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__134__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__134__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__133__b = vlTOPp->__Vfunc_v__DOT__mix_col__126__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__133__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__133__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__133__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__126__Vfuncout 
	= ((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__126__Vfuncout) 
	   | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__133__Vfuncout) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s0)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s2)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__134__Vfuncout)));
    vlTOPp->v__DOT__sa23_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__126__Vfuncout 
				       >> 8));
    // ALWAYS at aes/aes_cipher_top.v:173
    vlTOPp->__Vfunc_v__DOT__mix_col__135__s3 = vlSymsp->TOP__v__DOT__us32.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__135__s2 = vlSymsp->TOP__v__DOT__us21.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__135__s1 = vlSymsp->TOP__v__DOT__us10.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__135__s0 = vlSymsp->TOP__v__DOT__us03.d;
    vlTOPp->__Vfunc_v__DOT__xtime__137__b = vlTOPp->__Vfunc_v__DOT__mix_col__135__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__137__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__137__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__137__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__136__b = vlTOPp->__Vfunc_v__DOT__mix_col__135__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__136__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__136__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__136__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__135__Vfuncout 
	= ((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__135__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__136__Vfuncout) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__137__Vfuncout)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s1)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s2)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s3)) 
	      << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__139__b = vlTOPp->__Vfunc_v__DOT__mix_col__135__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__139__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__139__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__139__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__138__b = vlTOPp->__Vfunc_v__DOT__mix_col__135__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__138__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__138__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__138__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__135__Vfuncout 
	= ((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__135__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s0) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__138__Vfuncout)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__139__Vfuncout)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s2)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s3)) 
	      << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__141__b = vlTOPp->__Vfunc_v__DOT__mix_col__135__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__141__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__141__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__141__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__140__b = vlTOPp->__Vfunc_v__DOT__mix_col__135__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__140__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__140__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__140__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__135__Vfuncout 
	= ((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__135__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s0) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s1)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__140__Vfuncout)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__141__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s3)) 
	      << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__143__b = vlTOPp->__Vfunc_v__DOT__mix_col__135__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__143__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__143__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__143__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__142__b = vlTOPp->__Vfunc_v__DOT__mix_col__135__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__142__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__142__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__142__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__135__Vfuncout 
	= ((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__135__Vfuncout) 
	   | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__142__Vfuncout) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s0)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s2)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__143__Vfuncout)));
    vlTOPp->v__DOT__sa33_mc = (0xff & vlTOPp->__Vfunc_v__DOT__mix_col__135__Vfuncout);
    // ALWAYS at aes/aes_cipher_top.v:170
    vlTOPp->__Vfunc_v__DOT__mix_col__0__s3 = vlSymsp->TOP__v__DOT__us33.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__0__s2 = vlSymsp->TOP__v__DOT__us22.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__0__s1 = vlSymsp->TOP__v__DOT__us11.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__0__s0 = vlSymsp->TOP__v__DOT__us00.d;
    vlTOPp->__Vfunc_v__DOT__xtime__2__b = vlTOPp->__Vfunc_v__DOT__mix_col__0__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__2__Vfuncout = ((0xfe 
						   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__2__b) 
						      << 1)) 
						  ^ 
						  (0x1b 
						   & VL_NEGATE_I((IData)(
									 (1 
									  & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__2__b) 
									     >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__1__b = vlTOPp->__Vfunc_v__DOT__mix_col__0__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__1__Vfuncout = ((0xfe 
						   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__1__b) 
						      << 1)) 
						  ^ 
						  (0x1b 
						   & VL_NEGATE_I((IData)(
									 (1 
									  & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__1__b) 
									     >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__0__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__0__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__1__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__2__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__4__b = vlTOPp->__Vfunc_v__DOT__mix_col__0__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__4__Vfuncout = ((0xfe 
						   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__4__b) 
						      << 1)) 
						  ^ 
						  (0x1b 
						   & VL_NEGATE_I((IData)(
									 (1 
									  & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__4__b) 
									     >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__3__b = vlTOPp->__Vfunc_v__DOT__mix_col__0__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__3__Vfuncout = ((0xfe 
						   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__3__b) 
						      << 1)) 
						  ^ 
						  (0x1b 
						   & VL_NEGATE_I((IData)(
									 (1 
									  & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__3__b) 
									     >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__0__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__0__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__3__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__4__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__6__b = vlTOPp->__Vfunc_v__DOT__mix_col__0__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__6__Vfuncout = ((0xfe 
						   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__6__b) 
						      << 1)) 
						  ^ 
						  (0x1b 
						   & VL_NEGATE_I((IData)(
									 (1 
									  & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__6__b) 
									     >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__5__b = vlTOPp->__Vfunc_v__DOT__mix_col__0__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__5__Vfuncout = ((0xfe 
						   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__5__b) 
						      << 1)) 
						  ^ 
						  (0x1b 
						   & VL_NEGATE_I((IData)(
									 (1 
									  & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__5__b) 
									     >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__0__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__0__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__5__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__6__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__8__b = vlTOPp->__Vfunc_v__DOT__mix_col__0__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__8__Vfuncout = ((0xfe 
						   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__8__b) 
						      << 1)) 
						  ^ 
						  (0x1b 
						   & VL_NEGATE_I((IData)(
									 (1 
									  & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__8__b) 
									     >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__7__b = vlTOPp->__Vfunc_v__DOT__mix_col__0__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__7__Vfuncout = ((0xfe 
						   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__7__b) 
						      << 1)) 
						  ^ 
						  (0x1b 
						   & VL_NEGATE_I((IData)(
									 (1 
									  & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__7__b) 
									     >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__0__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__0__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__7__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__8__Vfuncout)));
    vlTOPp->v__DOT__sa00_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__0__Vfuncout 
				       >> 0x18));
    // ALWAYS at aes/aes_cipher_top.v:170
    vlTOPp->__Vfunc_v__DOT__mix_col__9__s3 = vlSymsp->TOP__v__DOT__us33.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__9__s2 = vlSymsp->TOP__v__DOT__us22.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__9__s1 = vlSymsp->TOP__v__DOT__us11.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__9__s0 = vlSymsp->TOP__v__DOT__us00.d;
    vlTOPp->__Vfunc_v__DOT__xtime__11__b = vlTOPp->__Vfunc_v__DOT__mix_col__9__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__11__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__11__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__11__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__10__b = vlTOPp->__Vfunc_v__DOT__mix_col__9__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__10__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__10__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__10__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__9__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__9__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__10__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__11__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__13__b = vlTOPp->__Vfunc_v__DOT__mix_col__9__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__13__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__13__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__13__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__12__b = vlTOPp->__Vfunc_v__DOT__mix_col__9__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__12__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__12__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__12__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__9__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__9__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__12__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__13__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__15__b = vlTOPp->__Vfunc_v__DOT__mix_col__9__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__15__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__15__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__15__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__14__b = vlTOPp->__Vfunc_v__DOT__mix_col__9__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__14__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__14__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__14__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__9__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__9__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__14__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__15__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__17__b = vlTOPp->__Vfunc_v__DOT__mix_col__9__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__17__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__17__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__17__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__16__b = vlTOPp->__Vfunc_v__DOT__mix_col__9__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__16__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__16__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__16__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__9__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__9__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__16__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__17__Vfuncout)));
    vlTOPp->v__DOT__sa10_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__9__Vfuncout 
				       >> 0x10));
    // ALWAYS at aes/aes_cipher_top.v:170
    vlTOPp->__Vfunc_v__DOT__mix_col__18__s3 = vlSymsp->TOP__v__DOT__us33.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__18__s2 = vlSymsp->TOP__v__DOT__us22.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__18__s1 = vlSymsp->TOP__v__DOT__us11.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__18__s0 = vlSymsp->TOP__v__DOT__us00.d;
    vlTOPp->__Vfunc_v__DOT__xtime__20__b = vlTOPp->__Vfunc_v__DOT__mix_col__18__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__20__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__20__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__20__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__19__b = vlTOPp->__Vfunc_v__DOT__mix_col__18__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__19__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__19__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__19__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__18__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__18__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__19__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__20__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__22__b = vlTOPp->__Vfunc_v__DOT__mix_col__18__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__22__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__22__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__22__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__21__b = vlTOPp->__Vfunc_v__DOT__mix_col__18__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__21__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__21__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__21__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__18__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__18__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__21__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__22__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__24__b = vlTOPp->__Vfunc_v__DOT__mix_col__18__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__24__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__24__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__24__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__23__b = vlTOPp->__Vfunc_v__DOT__mix_col__18__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__23__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__23__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__23__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__18__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__18__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__23__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__24__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__26__b = vlTOPp->__Vfunc_v__DOT__mix_col__18__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__26__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__26__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__26__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__25__b = vlTOPp->__Vfunc_v__DOT__mix_col__18__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__25__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__25__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__25__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__18__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__18__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__25__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__26__Vfuncout)));
    vlTOPp->v__DOT__sa20_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__18__Vfuncout 
				       >> 8));
    // ALWAYS at aes/aes_cipher_top.v:170
    vlTOPp->__Vfunc_v__DOT__mix_col__27__s3 = vlSymsp->TOP__v__DOT__us33.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__27__s2 = vlSymsp->TOP__v__DOT__us22.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__27__s1 = vlSymsp->TOP__v__DOT__us11.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__27__s0 = vlSymsp->TOP__v__DOT__us00.d;
    vlTOPp->__Vfunc_v__DOT__xtime__29__b = vlTOPp->__Vfunc_v__DOT__mix_col__27__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__29__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__29__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__29__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__28__b = vlTOPp->__Vfunc_v__DOT__mix_col__27__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__28__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__28__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__28__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__27__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__27__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__28__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__29__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__31__b = vlTOPp->__Vfunc_v__DOT__mix_col__27__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__31__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__31__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__31__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__30__b = vlTOPp->__Vfunc_v__DOT__mix_col__27__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__30__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__30__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__30__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__27__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__27__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__30__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__31__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__33__b = vlTOPp->__Vfunc_v__DOT__mix_col__27__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__33__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__33__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__33__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__32__b = vlTOPp->__Vfunc_v__DOT__mix_col__27__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__32__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__32__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__32__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__27__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__27__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__32__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__33__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__35__b = vlTOPp->__Vfunc_v__DOT__mix_col__27__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__35__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__35__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__35__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__34__b = vlTOPp->__Vfunc_v__DOT__mix_col__27__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__34__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__34__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__34__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__27__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__27__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__34__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__35__Vfuncout)));
    vlTOPp->v__DOT__sa30_mc = (0xff & vlTOPp->__Vfunc_v__DOT__mix_col__27__Vfuncout);
}

void Vaes_cipher_top::_sequent__TOP__3(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vaes_cipher_top::_sequent__TOP__3\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at aes/aes_cipher_top.v:171
    vlTOPp->__Vfunc_v__DOT__mix_col__36__s3 = vlSymsp->TOP__v__DOT__us30.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__36__s2 = vlSymsp->TOP__v__DOT__us23.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__36__s1 = vlSymsp->TOP__v__DOT__us12.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__36__s0 = vlSymsp->TOP__v__DOT__us01.d;
    vlTOPp->__Vfunc_v__DOT__xtime__38__b = vlTOPp->__Vfunc_v__DOT__mix_col__36__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__38__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__38__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__38__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__37__b = vlTOPp->__Vfunc_v__DOT__mix_col__36__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__37__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__37__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__37__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__36__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__36__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__37__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__38__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__40__b = vlTOPp->__Vfunc_v__DOT__mix_col__36__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__40__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__40__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__40__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__39__b = vlTOPp->__Vfunc_v__DOT__mix_col__36__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__39__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__39__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__39__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__36__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__36__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__39__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__40__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__42__b = vlTOPp->__Vfunc_v__DOT__mix_col__36__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__42__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__42__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__42__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__41__b = vlTOPp->__Vfunc_v__DOT__mix_col__36__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__41__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__41__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__41__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__36__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__36__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__41__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__42__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__44__b = vlTOPp->__Vfunc_v__DOT__mix_col__36__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__44__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__44__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__44__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__43__b = vlTOPp->__Vfunc_v__DOT__mix_col__36__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__43__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__43__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__43__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__36__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__36__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__43__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__36__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__44__Vfuncout)));
    vlTOPp->v__DOT__sa01_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__36__Vfuncout 
				       >> 0x18));
    // ALWAYS at aes/aes_cipher_top.v:171
    vlTOPp->__Vfunc_v__DOT__mix_col__45__s3 = vlSymsp->TOP__v__DOT__us30.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__45__s2 = vlSymsp->TOP__v__DOT__us23.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__45__s1 = vlSymsp->TOP__v__DOT__us12.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__45__s0 = vlSymsp->TOP__v__DOT__us01.d;
    vlTOPp->__Vfunc_v__DOT__xtime__47__b = vlTOPp->__Vfunc_v__DOT__mix_col__45__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__47__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__47__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__47__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__46__b = vlTOPp->__Vfunc_v__DOT__mix_col__45__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__46__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__46__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__46__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__45__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__45__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__46__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__47__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__49__b = vlTOPp->__Vfunc_v__DOT__mix_col__45__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__49__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__49__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__49__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__48__b = vlTOPp->__Vfunc_v__DOT__mix_col__45__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__48__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__48__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__48__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__45__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__45__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__48__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__49__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__51__b = vlTOPp->__Vfunc_v__DOT__mix_col__45__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__51__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__51__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__51__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__50__b = vlTOPp->__Vfunc_v__DOT__mix_col__45__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__50__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__50__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__50__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__45__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__45__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__50__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__51__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__53__b = vlTOPp->__Vfunc_v__DOT__mix_col__45__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__53__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__53__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__53__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__52__b = vlTOPp->__Vfunc_v__DOT__mix_col__45__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__52__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__52__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__52__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__45__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__45__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__52__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__45__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__53__Vfuncout)));
    vlTOPp->v__DOT__sa11_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__45__Vfuncout 
				       >> 0x10));
    // ALWAYS at aes/aes_cipher_top.v:171
    vlTOPp->__Vfunc_v__DOT__mix_col__54__s3 = vlSymsp->TOP__v__DOT__us30.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__54__s2 = vlSymsp->TOP__v__DOT__us23.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__54__s1 = vlSymsp->TOP__v__DOT__us12.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__54__s0 = vlSymsp->TOP__v__DOT__us01.d;
    vlTOPp->__Vfunc_v__DOT__xtime__56__b = vlTOPp->__Vfunc_v__DOT__mix_col__54__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__56__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__56__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__56__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__55__b = vlTOPp->__Vfunc_v__DOT__mix_col__54__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__55__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__55__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__55__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__54__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__54__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__55__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__56__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__58__b = vlTOPp->__Vfunc_v__DOT__mix_col__54__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__58__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__58__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__58__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__57__b = vlTOPp->__Vfunc_v__DOT__mix_col__54__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__57__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__57__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__57__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__54__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__54__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__57__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__58__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__60__b = vlTOPp->__Vfunc_v__DOT__mix_col__54__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__60__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__60__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__60__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__59__b = vlTOPp->__Vfunc_v__DOT__mix_col__54__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__59__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__59__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__59__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__54__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__54__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__59__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__60__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__62__b = vlTOPp->__Vfunc_v__DOT__mix_col__54__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__62__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__62__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__62__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__61__b = vlTOPp->__Vfunc_v__DOT__mix_col__54__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__61__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__61__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__61__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__54__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__54__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__61__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__54__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__62__Vfuncout)));
    vlTOPp->v__DOT__sa21_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__54__Vfuncout 
				       >> 8));
    // ALWAYS at aes/aes_cipher_top.v:171
    vlTOPp->__Vfunc_v__DOT__mix_col__63__s3 = vlSymsp->TOP__v__DOT__us30.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__63__s2 = vlSymsp->TOP__v__DOT__us23.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__63__s1 = vlSymsp->TOP__v__DOT__us12.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__63__s0 = vlSymsp->TOP__v__DOT__us01.d;
    vlTOPp->__Vfunc_v__DOT__xtime__65__b = vlTOPp->__Vfunc_v__DOT__mix_col__63__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__65__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__65__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__65__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__64__b = vlTOPp->__Vfunc_v__DOT__mix_col__63__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__64__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__64__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__64__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__63__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__63__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__64__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__65__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__67__b = vlTOPp->__Vfunc_v__DOT__mix_col__63__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__67__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__67__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__67__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__66__b = vlTOPp->__Vfunc_v__DOT__mix_col__63__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__66__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__66__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__66__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__63__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__63__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__66__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__67__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__69__b = vlTOPp->__Vfunc_v__DOT__mix_col__63__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__69__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__69__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__69__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__68__b = vlTOPp->__Vfunc_v__DOT__mix_col__63__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__68__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__68__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__68__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__63__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__63__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__68__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__69__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__71__b = vlTOPp->__Vfunc_v__DOT__mix_col__63__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__71__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__71__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__71__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__70__b = vlTOPp->__Vfunc_v__DOT__mix_col__63__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__70__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__70__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__70__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__63__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__63__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__70__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__63__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__71__Vfuncout)));
    vlTOPp->v__DOT__sa31_mc = (0xff & vlTOPp->__Vfunc_v__DOT__mix_col__63__Vfuncout);
    // ALWAYS at aes/aes_cipher_top.v:172
    vlTOPp->__Vfunc_v__DOT__mix_col__72__s3 = vlSymsp->TOP__v__DOT__us31.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__72__s2 = vlSymsp->TOP__v__DOT__us20.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__72__s1 = vlSymsp->TOP__v__DOT__us13.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__72__s0 = vlSymsp->TOP__v__DOT__us02.d;
    vlTOPp->__Vfunc_v__DOT__xtime__74__b = vlTOPp->__Vfunc_v__DOT__mix_col__72__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__74__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__74__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__74__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__73__b = vlTOPp->__Vfunc_v__DOT__mix_col__72__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__73__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__73__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__73__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__72__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__72__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__73__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__74__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__76__b = vlTOPp->__Vfunc_v__DOT__mix_col__72__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__76__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__76__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__76__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__75__b = vlTOPp->__Vfunc_v__DOT__mix_col__72__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__75__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__75__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__75__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__72__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__72__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__75__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__76__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__78__b = vlTOPp->__Vfunc_v__DOT__mix_col__72__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__78__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__78__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__78__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__77__b = vlTOPp->__Vfunc_v__DOT__mix_col__72__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__77__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__77__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__77__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__72__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__72__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__77__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__78__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__80__b = vlTOPp->__Vfunc_v__DOT__mix_col__72__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__80__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__80__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__80__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__79__b = vlTOPp->__Vfunc_v__DOT__mix_col__72__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__79__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__79__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__79__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__72__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__72__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__79__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__72__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__80__Vfuncout)));
    vlTOPp->v__DOT__sa02_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__72__Vfuncout 
				       >> 0x18));
    // ALWAYS at aes/aes_cipher_top.v:172
    vlTOPp->__Vfunc_v__DOT__mix_col__81__s3 = vlSymsp->TOP__v__DOT__us31.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__81__s2 = vlSymsp->TOP__v__DOT__us20.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__81__s1 = vlSymsp->TOP__v__DOT__us13.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__81__s0 = vlSymsp->TOP__v__DOT__us02.d;
    vlTOPp->__Vfunc_v__DOT__xtime__83__b = vlTOPp->__Vfunc_v__DOT__mix_col__81__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__83__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__83__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__83__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__82__b = vlTOPp->__Vfunc_v__DOT__mix_col__81__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__82__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__82__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__82__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__81__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__81__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__82__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__83__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__85__b = vlTOPp->__Vfunc_v__DOT__mix_col__81__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__85__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__85__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__85__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__84__b = vlTOPp->__Vfunc_v__DOT__mix_col__81__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__84__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__84__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__84__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__81__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__81__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__84__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__85__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__87__b = vlTOPp->__Vfunc_v__DOT__mix_col__81__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__87__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__87__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__87__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__86__b = vlTOPp->__Vfunc_v__DOT__mix_col__81__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__86__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__86__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__86__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__81__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__81__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__86__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__87__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__89__b = vlTOPp->__Vfunc_v__DOT__mix_col__81__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__89__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__89__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__89__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__88__b = vlTOPp->__Vfunc_v__DOT__mix_col__81__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__88__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__88__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__88__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__81__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__81__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__88__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__81__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__89__Vfuncout)));
    vlTOPp->v__DOT__sa12_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__81__Vfuncout 
				       >> 0x10));
    // ALWAYS at aes/aes_cipher_top.v:172
    vlTOPp->__Vfunc_v__DOT__mix_col__90__s3 = vlSymsp->TOP__v__DOT__us31.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__90__s2 = vlSymsp->TOP__v__DOT__us20.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__90__s1 = vlSymsp->TOP__v__DOT__us13.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__90__s0 = vlSymsp->TOP__v__DOT__us02.d;
    vlTOPp->__Vfunc_v__DOT__xtime__92__b = vlTOPp->__Vfunc_v__DOT__mix_col__90__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__92__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__92__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__92__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__91__b = vlTOPp->__Vfunc_v__DOT__mix_col__90__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__91__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__91__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__91__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__90__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__90__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__91__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__92__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__94__b = vlTOPp->__Vfunc_v__DOT__mix_col__90__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__94__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__94__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__94__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__93__b = vlTOPp->__Vfunc_v__DOT__mix_col__90__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__93__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__93__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__93__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__90__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__90__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__93__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__94__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__96__b = vlTOPp->__Vfunc_v__DOT__mix_col__90__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__96__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__96__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__96__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__95__b = vlTOPp->__Vfunc_v__DOT__mix_col__90__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__95__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__95__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__95__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__90__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__90__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__95__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__96__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__98__b = vlTOPp->__Vfunc_v__DOT__mix_col__90__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__98__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__98__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__98__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__97__b = vlTOPp->__Vfunc_v__DOT__mix_col__90__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__97__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__97__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__97__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__90__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__90__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__97__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__90__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__98__Vfuncout)));
    vlTOPp->v__DOT__sa22_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__90__Vfuncout 
				       >> 8));
    // ALWAYS at aes/aes_cipher_top.v:172
    vlTOPp->__Vfunc_v__DOT__mix_col__99__s3 = vlSymsp->TOP__v__DOT__us31.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__99__s2 = vlSymsp->TOP__v__DOT__us20.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__99__s1 = vlSymsp->TOP__v__DOT__us13.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__99__s0 = vlSymsp->TOP__v__DOT__us02.d;
    vlTOPp->__Vfunc_v__DOT__xtime__101__b = vlTOPp->__Vfunc_v__DOT__mix_col__99__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__101__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__101__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__101__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__100__b = vlTOPp->__Vfunc_v__DOT__mix_col__99__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__100__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__100__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__100__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__99__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__99__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__100__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__101__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__103__b = vlTOPp->__Vfunc_v__DOT__mix_col__99__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__103__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__103__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__103__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__102__b = vlTOPp->__Vfunc_v__DOT__mix_col__99__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__102__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__102__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__102__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__99__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__99__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__102__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__103__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__105__b = vlTOPp->__Vfunc_v__DOT__mix_col__99__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__105__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__105__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__105__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__104__b = vlTOPp->__Vfunc_v__DOT__mix_col__99__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__104__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__104__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__104__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__99__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__99__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__104__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__105__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__107__b = vlTOPp->__Vfunc_v__DOT__mix_col__99__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__107__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__107__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__107__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__106__b = vlTOPp->__Vfunc_v__DOT__mix_col__99__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__106__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__106__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__106__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__99__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__99__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__106__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__99__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__107__Vfuncout)));
    vlTOPp->v__DOT__sa32_mc = (0xff & vlTOPp->__Vfunc_v__DOT__mix_col__99__Vfuncout);
    // ALWAYS at aes/aes_cipher_top.v:173
    vlTOPp->__Vfunc_v__DOT__mix_col__108__s3 = vlSymsp->TOP__v__DOT__us32.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__108__s2 = vlSymsp->TOP__v__DOT__us21.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__108__s1 = vlSymsp->TOP__v__DOT__us10.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__108__s0 = vlSymsp->TOP__v__DOT__us03.d;
    vlTOPp->__Vfunc_v__DOT__xtime__110__b = vlTOPp->__Vfunc_v__DOT__mix_col__108__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__110__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__110__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__110__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__109__b = vlTOPp->__Vfunc_v__DOT__mix_col__108__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__109__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__109__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__109__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__108__Vfuncout 
	= ((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__108__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__109__Vfuncout) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__110__Vfuncout)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s1)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s2)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s3)) 
	      << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__112__b = vlTOPp->__Vfunc_v__DOT__mix_col__108__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__112__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__112__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__112__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__111__b = vlTOPp->__Vfunc_v__DOT__mix_col__108__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__111__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__111__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__111__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__108__Vfuncout 
	= ((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__108__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s0) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__111__Vfuncout)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__112__Vfuncout)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s2)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s3)) 
	      << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__114__b = vlTOPp->__Vfunc_v__DOT__mix_col__108__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__114__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__114__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__114__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__113__b = vlTOPp->__Vfunc_v__DOT__mix_col__108__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__113__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__113__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__113__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__108__Vfuncout 
	= ((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__108__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s0) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s1)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__113__Vfuncout)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__114__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s3)) 
	      << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__116__b = vlTOPp->__Vfunc_v__DOT__mix_col__108__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__116__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__116__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__116__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__115__b = vlTOPp->__Vfunc_v__DOT__mix_col__108__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__115__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__115__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__115__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__108__Vfuncout 
	= ((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__108__Vfuncout) 
	   | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__115__Vfuncout) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s0)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__108__s2)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__116__Vfuncout)));
    vlTOPp->v__DOT__sa03_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__108__Vfuncout 
				       >> 0x18));
    // ALWAYS at aes/aes_cipher_top.v:173
    vlTOPp->__Vfunc_v__DOT__mix_col__117__s3 = vlSymsp->TOP__v__DOT__us32.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__117__s2 = vlSymsp->TOP__v__DOT__us21.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__117__s1 = vlSymsp->TOP__v__DOT__us10.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__117__s0 = vlSymsp->TOP__v__DOT__us03.d;
    vlTOPp->__Vfunc_v__DOT__xtime__119__b = vlTOPp->__Vfunc_v__DOT__mix_col__117__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__119__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__119__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__119__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__118__b = vlTOPp->__Vfunc_v__DOT__mix_col__117__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__118__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__118__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__118__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__117__Vfuncout 
	= ((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__117__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__118__Vfuncout) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__119__Vfuncout)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s1)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s2)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s3)) 
	      << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__121__b = vlTOPp->__Vfunc_v__DOT__mix_col__117__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__121__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__121__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__121__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__120__b = vlTOPp->__Vfunc_v__DOT__mix_col__117__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__120__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__120__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__120__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__117__Vfuncout 
	= ((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__117__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s0) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__120__Vfuncout)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__121__Vfuncout)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s2)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s3)) 
	      << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__123__b = vlTOPp->__Vfunc_v__DOT__mix_col__117__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__123__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__123__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__123__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__122__b = vlTOPp->__Vfunc_v__DOT__mix_col__117__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__122__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__122__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__122__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__117__Vfuncout 
	= ((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__117__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s0) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s1)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__122__Vfuncout)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__123__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s3)) 
	      << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__125__b = vlTOPp->__Vfunc_v__DOT__mix_col__117__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__125__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__125__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__125__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__124__b = vlTOPp->__Vfunc_v__DOT__mix_col__117__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__124__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__124__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__124__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__117__Vfuncout 
	= ((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__117__Vfuncout) 
	   | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__124__Vfuncout) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s0)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__117__s2)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__125__Vfuncout)));
    vlTOPp->v__DOT__sa13_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__117__Vfuncout 
				       >> 0x10));
    // ALWAYS at aes/aes_cipher_top.v:173
    vlTOPp->__Vfunc_v__DOT__mix_col__126__s3 = vlSymsp->TOP__v__DOT__us32.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__126__s2 = vlSymsp->TOP__v__DOT__us21.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__126__s1 = vlSymsp->TOP__v__DOT__us10.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__126__s0 = vlSymsp->TOP__v__DOT__us03.d;
    vlTOPp->__Vfunc_v__DOT__xtime__128__b = vlTOPp->__Vfunc_v__DOT__mix_col__126__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__128__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__128__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__128__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__127__b = vlTOPp->__Vfunc_v__DOT__mix_col__126__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__127__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__127__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__127__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__126__Vfuncout 
	= ((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__126__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__127__Vfuncout) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__128__Vfuncout)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s1)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s2)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s3)) 
	      << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__130__b = vlTOPp->__Vfunc_v__DOT__mix_col__126__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__130__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__130__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__130__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__129__b = vlTOPp->__Vfunc_v__DOT__mix_col__126__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__129__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__129__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__129__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__126__Vfuncout 
	= ((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__126__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s0) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__129__Vfuncout)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__130__Vfuncout)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s2)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s3)) 
	      << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__132__b = vlTOPp->__Vfunc_v__DOT__mix_col__126__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__132__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__132__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__132__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__131__b = vlTOPp->__Vfunc_v__DOT__mix_col__126__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__131__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__131__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__131__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__126__Vfuncout 
	= ((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__126__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s0) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s1)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__131__Vfuncout)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__132__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s3)) 
	      << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__134__b = vlTOPp->__Vfunc_v__DOT__mix_col__126__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__134__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__134__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__134__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__133__b = vlTOPp->__Vfunc_v__DOT__mix_col__126__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__133__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__133__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__133__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__126__Vfuncout 
	= ((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__126__Vfuncout) 
	   | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__133__Vfuncout) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s0)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__126__s2)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__134__Vfuncout)));
    vlTOPp->v__DOT__sa23_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__126__Vfuncout 
				       >> 8));
    // ALWAYS at aes/aes_cipher_top.v:173
    vlTOPp->__Vfunc_v__DOT__mix_col__135__s3 = vlSymsp->TOP__v__DOT__us32.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__135__s2 = vlSymsp->TOP__v__DOT__us21.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__135__s1 = vlSymsp->TOP__v__DOT__us10.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__135__s0 = vlSymsp->TOP__v__DOT__us03.d;
    vlTOPp->__Vfunc_v__DOT__xtime__137__b = vlTOPp->__Vfunc_v__DOT__mix_col__135__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__137__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__137__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__137__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__136__b = vlTOPp->__Vfunc_v__DOT__mix_col__135__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__136__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__136__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__136__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__135__Vfuncout 
	= ((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__135__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__136__Vfuncout) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__137__Vfuncout)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s1)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s2)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s3)) 
	      << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__139__b = vlTOPp->__Vfunc_v__DOT__mix_col__135__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__139__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__139__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__139__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__138__b = vlTOPp->__Vfunc_v__DOT__mix_col__135__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__138__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__138__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__138__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__135__Vfuncout 
	= ((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__135__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s0) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__138__Vfuncout)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__139__Vfuncout)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s2)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s3)) 
	      << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__141__b = vlTOPp->__Vfunc_v__DOT__mix_col__135__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__141__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__141__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__141__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__140__b = vlTOPp->__Vfunc_v__DOT__mix_col__135__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__140__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__140__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__140__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__135__Vfuncout 
	= ((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__135__Vfuncout) 
	   | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s0) 
		  ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s1)) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__140__Vfuncout)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__141__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s3)) 
	      << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__143__b = vlTOPp->__Vfunc_v__DOT__mix_col__135__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__143__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__143__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__143__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__142__b = vlTOPp->__Vfunc_v__DOT__mix_col__135__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__142__Vfuncout = 
	((0xfe & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__142__b) 
		  << 1)) ^ (0x1b & VL_NEGATE_I((IData)(
						       (1 
							& ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__142__b) 
							   >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__135__Vfuncout 
	= ((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__135__Vfuncout) 
	   | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__142__Vfuncout) 
		 ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s0)) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__135__s2)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__143__Vfuncout)));
    vlTOPp->v__DOT__sa33_mc = (0xff & vlTOPp->__Vfunc_v__DOT__mix_col__135__Vfuncout);
    // ALWAYS at aes/aes_cipher_top.v:170
    vlTOPp->__Vfunc_v__DOT__mix_col__0__s3 = vlSymsp->TOP__v__DOT__us33.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__0__s2 = vlSymsp->TOP__v__DOT__us22.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__0__s1 = vlSymsp->TOP__v__DOT__us11.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__0__s0 = vlSymsp->TOP__v__DOT__us00.d;
    vlTOPp->__Vfunc_v__DOT__xtime__2__b = vlTOPp->__Vfunc_v__DOT__mix_col__0__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__2__Vfuncout = ((0xfe 
						   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__2__b) 
						      << 1)) 
						  ^ 
						  (0x1b 
						   & VL_NEGATE_I((IData)(
									 (1 
									  & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__2__b) 
									     >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__1__b = vlTOPp->__Vfunc_v__DOT__mix_col__0__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__1__Vfuncout = ((0xfe 
						   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__1__b) 
						      << 1)) 
						  ^ 
						  (0x1b 
						   & VL_NEGATE_I((IData)(
									 (1 
									  & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__1__b) 
									     >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__0__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__0__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__1__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__2__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__4__b = vlTOPp->__Vfunc_v__DOT__mix_col__0__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__4__Vfuncout = ((0xfe 
						   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__4__b) 
						      << 1)) 
						  ^ 
						  (0x1b 
						   & VL_NEGATE_I((IData)(
									 (1 
									  & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__4__b) 
									     >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__3__b = vlTOPp->__Vfunc_v__DOT__mix_col__0__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__3__Vfuncout = ((0xfe 
						   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__3__b) 
						      << 1)) 
						  ^ 
						  (0x1b 
						   & VL_NEGATE_I((IData)(
									 (1 
									  & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__3__b) 
									     >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__0__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__0__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__3__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__4__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__6__b = vlTOPp->__Vfunc_v__DOT__mix_col__0__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__6__Vfuncout = ((0xfe 
						   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__6__b) 
						      << 1)) 
						  ^ 
						  (0x1b 
						   & VL_NEGATE_I((IData)(
									 (1 
									  & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__6__b) 
									     >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__5__b = vlTOPp->__Vfunc_v__DOT__mix_col__0__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__5__Vfuncout = ((0xfe 
						   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__5__b) 
						      << 1)) 
						  ^ 
						  (0x1b 
						   & VL_NEGATE_I((IData)(
									 (1 
									  & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__5__b) 
									     >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__0__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__0__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__5__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__6__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__8__b = vlTOPp->__Vfunc_v__DOT__mix_col__0__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__8__Vfuncout = ((0xfe 
						   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__8__b) 
						      << 1)) 
						  ^ 
						  (0x1b 
						   & VL_NEGATE_I((IData)(
									 (1 
									  & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__8__b) 
									     >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__7__b = vlTOPp->__Vfunc_v__DOT__mix_col__0__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__7__Vfuncout = ((0xfe 
						   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__7__b) 
						      << 1)) 
						  ^ 
						  (0x1b 
						   & VL_NEGATE_I((IData)(
									 (1 
									  & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__7__b) 
									     >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__0__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__0__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__7__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__0__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__8__Vfuncout)));
    vlTOPp->v__DOT__sa00_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__0__Vfuncout 
				       >> 0x18));
    // ALWAYS at aes/aes_cipher_top.v:170
    vlTOPp->__Vfunc_v__DOT__mix_col__9__s3 = vlSymsp->TOP__v__DOT__us33.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__9__s2 = vlSymsp->TOP__v__DOT__us22.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__9__s1 = vlSymsp->TOP__v__DOT__us11.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__9__s0 = vlSymsp->TOP__v__DOT__us00.d;
    vlTOPp->__Vfunc_v__DOT__xtime__11__b = vlTOPp->__Vfunc_v__DOT__mix_col__9__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__11__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__11__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__11__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__10__b = vlTOPp->__Vfunc_v__DOT__mix_col__9__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__10__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__10__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__10__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__9__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__9__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__10__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__11__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__13__b = vlTOPp->__Vfunc_v__DOT__mix_col__9__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__13__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__13__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__13__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__12__b = vlTOPp->__Vfunc_v__DOT__mix_col__9__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__12__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__12__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__12__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__9__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__9__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__12__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__13__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__15__b = vlTOPp->__Vfunc_v__DOT__mix_col__9__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__15__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__15__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__15__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__14__b = vlTOPp->__Vfunc_v__DOT__mix_col__9__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__14__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__14__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__14__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__9__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__9__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__14__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__15__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__17__b = vlTOPp->__Vfunc_v__DOT__mix_col__9__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__17__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__17__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__17__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__16__b = vlTOPp->__Vfunc_v__DOT__mix_col__9__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__16__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__16__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__16__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__9__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__9__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__16__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__9__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__17__Vfuncout)));
    vlTOPp->v__DOT__sa10_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__9__Vfuncout 
				       >> 0x10));
    // ALWAYS at aes/aes_cipher_top.v:170
    vlTOPp->__Vfunc_v__DOT__mix_col__18__s3 = vlSymsp->TOP__v__DOT__us33.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__18__s2 = vlSymsp->TOP__v__DOT__us22.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__18__s1 = vlSymsp->TOP__v__DOT__us11.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__18__s0 = vlSymsp->TOP__v__DOT__us00.d;
    vlTOPp->__Vfunc_v__DOT__xtime__20__b = vlTOPp->__Vfunc_v__DOT__mix_col__18__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__20__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__20__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__20__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__19__b = vlTOPp->__Vfunc_v__DOT__mix_col__18__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__19__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__19__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__19__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__18__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__18__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__19__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__20__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__22__b = vlTOPp->__Vfunc_v__DOT__mix_col__18__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__22__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__22__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__22__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__21__b = vlTOPp->__Vfunc_v__DOT__mix_col__18__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__21__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__21__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__21__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__18__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__18__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__21__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__22__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__24__b = vlTOPp->__Vfunc_v__DOT__mix_col__18__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__24__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__24__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__24__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__23__b = vlTOPp->__Vfunc_v__DOT__mix_col__18__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__23__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__23__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__23__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__18__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__18__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__23__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__24__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__26__b = vlTOPp->__Vfunc_v__DOT__mix_col__18__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__26__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__26__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__26__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__25__b = vlTOPp->__Vfunc_v__DOT__mix_col__18__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__25__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__25__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__25__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__18__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__18__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__25__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__18__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__26__Vfuncout)));
    vlTOPp->v__DOT__sa20_mc = (0xff & (vlTOPp->__Vfunc_v__DOT__mix_col__18__Vfuncout 
				       >> 8));
    // ALWAYS at aes/aes_cipher_top.v:170
    vlTOPp->__Vfunc_v__DOT__mix_col__27__s3 = vlSymsp->TOP__v__DOT__us33.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__27__s2 = vlSymsp->TOP__v__DOT__us22.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__27__s1 = vlSymsp->TOP__v__DOT__us11.d;
    vlTOPp->__Vfunc_v__DOT__mix_col__27__s0 = vlSymsp->TOP__v__DOT__us00.d;
    vlTOPp->__Vfunc_v__DOT__xtime__29__b = vlTOPp->__Vfunc_v__DOT__mix_col__27__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__29__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__29__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__29__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__28__b = vlTOPp->__Vfunc_v__DOT__mix_col__27__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__28__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__28__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__28__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__27__Vfuncout = 
	((0xffffff & vlTOPp->__Vfunc_v__DOT__mix_col__27__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__28__Vfuncout) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__29__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s1)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s3)) 
	    << 0x18));
    vlTOPp->__Vfunc_v__DOT__xtime__31__b = vlTOPp->__Vfunc_v__DOT__mix_col__27__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__31__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__31__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__31__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__30__b = vlTOPp->__Vfunc_v__DOT__mix_col__27__s1;
    vlTOPp->__Vfunc_v__DOT__xtime__30__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__30__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__30__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__27__Vfuncout = 
	((0xff00ffff & vlTOPp->__Vfunc_v__DOT__mix_col__27__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__30__Vfuncout)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__31__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s2)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s3)) 
	    << 0x10));
    vlTOPp->__Vfunc_v__DOT__xtime__33__b = vlTOPp->__Vfunc_v__DOT__mix_col__27__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__33__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__33__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__33__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__32__b = vlTOPp->__Vfunc_v__DOT__mix_col__27__s2;
    vlTOPp->__Vfunc_v__DOT__xtime__32__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__32__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__32__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__27__Vfuncout = 
	((0xffff00ff & vlTOPp->__Vfunc_v__DOT__mix_col__27__Vfuncout) 
	 | ((((((IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s0) 
		^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s1)) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__32__Vfuncout)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__33__Vfuncout)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s3)) 
	    << 8));
    vlTOPp->__Vfunc_v__DOT__xtime__35__b = vlTOPp->__Vfunc_v__DOT__mix_col__27__s3;
    vlTOPp->__Vfunc_v__DOT__xtime__35__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__35__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__35__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__xtime__34__b = vlTOPp->__Vfunc_v__DOT__mix_col__27__s0;
    vlTOPp->__Vfunc_v__DOT__xtime__34__Vfuncout = (
						   (0xfe 
						    & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__34__b) 
						       << 1)) 
						   ^ 
						   (0x1b 
						    & VL_NEGATE_I((IData)(
									  (1 
									   & ((IData)(vlTOPp->__Vfunc_v__DOT__xtime__34__b) 
									      >> 7))))));
    vlTOPp->__Vfunc_v__DOT__mix_col__27__Vfuncout = 
	((0xffffff00 & vlTOPp->__Vfunc_v__DOT__mix_col__27__Vfuncout) 
	 | (((((IData)(vlTOPp->__Vfunc_v__DOT__xtime__34__Vfuncout) 
	       ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s0)) 
	      ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s1)) 
	     ^ (IData)(vlTOPp->__Vfunc_v__DOT__mix_col__27__s2)) 
	    ^ (IData)(vlTOPp->__Vfunc_v__DOT__xtime__35__Vfuncout)));
    vlTOPp->v__DOT__sa30_mc = (0xff & vlTOPp->__Vfunc_v__DOT__mix_col__27__Vfuncout);
}

void Vaes_cipher_top::_sequent__TOP__4(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vaes_cipher_top::_sequent__TOP__4\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__u0__DOT__subword = ((0xffffff & vlTOPp->v__DOT__u0__DOT__subword) 
					| ((IData)(vlSymsp->TOP__v__DOT__u0__DOT__u0.d) 
					   << 0x18));
    vlTOPp->v__DOT__u0__DOT__subword = ((0xff00ffff 
					 & vlTOPp->v__DOT__u0__DOT__subword) 
					| ((IData)(vlSymsp->TOP__v__DOT__u0__DOT__u1.d) 
					   << 0x10));
    vlTOPp->v__DOT__u0__DOT__subword = ((0xffff00ff 
					 & vlTOPp->v__DOT__u0__DOT__subword) 
					| ((IData)(vlSymsp->TOP__v__DOT__u0__DOT__u2.d) 
					   << 8));
    vlTOPp->v__DOT__u0__DOT__subword = ((0xffffff00 
					 & vlTOPp->v__DOT__u0__DOT__subword) 
					| (IData)(vlSymsp->TOP__v__DOT__u0__DOT__u3.d));
}

void Vaes_cipher_top::_eval(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vaes_cipher_top::_eval\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (((IData)(vlTOPp->clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__clk)))) {
	vlTOPp->_sequent__TOP__1(vlSymsp);
	vlTOPp->__Vm_traceActivity = (2 | vlTOPp->__Vm_traceActivity);
	vlSymsp->TOP__v__DOT__us33._sequent__TOP__v__DOT__us33__1(vlSymsp);
	vlSymsp->TOP__v__DOT__us23._sequent__TOP__v__DOT__us23__2(vlSymsp);
	vlSymsp->TOP__v__DOT__us13._sequent__TOP__v__DOT__us13__3(vlSymsp);
	vlSymsp->TOP__v__DOT__us03._sequent__TOP__v__DOT__us03__4(vlSymsp);
	vlSymsp->TOP__v__DOT__us32._sequent__TOP__v__DOT__us32__5(vlSymsp);
	vlSymsp->TOP__v__DOT__us22._sequent__TOP__v__DOT__us22__6(vlSymsp);
	vlSymsp->TOP__v__DOT__us12._sequent__TOP__v__DOT__us12__7(vlSymsp);
	vlSymsp->TOP__v__DOT__us02._sequent__TOP__v__DOT__us02__8(vlSymsp);
	vlSymsp->TOP__v__DOT__us31._sequent__TOP__v__DOT__us31__9(vlSymsp);
	vlSymsp->TOP__v__DOT__us21._sequent__TOP__v__DOT__us21__10(vlSymsp);
	vlSymsp->TOP__v__DOT__us11._sequent__TOP__v__DOT__us11__11(vlSymsp);
	vlSymsp->TOP__v__DOT__us01._sequent__TOP__v__DOT__us01__12(vlSymsp);
	vlSymsp->TOP__v__DOT__us30._sequent__TOP__v__DOT__us30__13(vlSymsp);
	vlSymsp->TOP__v__DOT__us20._sequent__TOP__v__DOT__us20__14(vlSymsp);
	vlSymsp->TOP__v__DOT__us10._sequent__TOP__v__DOT__us10__15(vlSymsp);
	vlSymsp->TOP__v__DOT__us00._sequent__TOP__v__DOT__us00__16(vlSymsp);
	vlTOPp->_sequent__TOP__3(vlSymsp);
	vlSymsp->TOP__v__DOT__u0__DOT__u0._settle__TOP__v__DOT__u0__DOT__u0__33(vlSymsp);
	vlSymsp->TOP__v__DOT__u0__DOT__u1._settle__TOP__v__DOT__u0__DOT__u1__34(vlSymsp);
	vlSymsp->TOP__v__DOT__u0__DOT__u2._settle__TOP__v__DOT__u0__DOT__u2__35(vlSymsp);
	vlSymsp->TOP__v__DOT__u0__DOT__u3._settle__TOP__v__DOT__u0__DOT__u3__36(vlSymsp);
	vlTOPp->_sequent__TOP__4(vlSymsp);
    }
    // Final
    vlTOPp->__Vclklast__TOP__clk = vlTOPp->clk;
}

void Vaes_cipher_top::_eval_initial(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vaes_cipher_top::_eval_initial\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void Vaes_cipher_top::final() {
    VL_DEBUG_IF(VL_PRINTF("    Vaes_cipher_top::final\n"); );
    // Variables
    Vaes_cipher_top__Syms* __restrict vlSymsp = this->__VlSymsp;
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void Vaes_cipher_top::_eval_settle(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vaes_cipher_top::_eval_settle\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlSymsp->TOP__v__DOT__us33._sequent__TOP__v__DOT__us33__1(vlSymsp);
    vlTOPp->__Vm_traceActivity = (1 | vlTOPp->__Vm_traceActivity);
    vlSymsp->TOP__v__DOT__us23._sequent__TOP__v__DOT__us23__2(vlSymsp);
    vlSymsp->TOP__v__DOT__us13._sequent__TOP__v__DOT__us13__3(vlSymsp);
    vlSymsp->TOP__v__DOT__us03._sequent__TOP__v__DOT__us03__4(vlSymsp);
    vlSymsp->TOP__v__DOT__us32._sequent__TOP__v__DOT__us32__5(vlSymsp);
    vlSymsp->TOP__v__DOT__us22._sequent__TOP__v__DOT__us22__6(vlSymsp);
    vlSymsp->TOP__v__DOT__us12._sequent__TOP__v__DOT__us12__7(vlSymsp);
    vlSymsp->TOP__v__DOT__us02._sequent__TOP__v__DOT__us02__8(vlSymsp);
    vlSymsp->TOP__v__DOT__us31._sequent__TOP__v__DOT__us31__9(vlSymsp);
    vlSymsp->TOP__v__DOT__us21._sequent__TOP__v__DOT__us21__10(vlSymsp);
    vlSymsp->TOP__v__DOT__us11._sequent__TOP__v__DOT__us11__11(vlSymsp);
    vlSymsp->TOP__v__DOT__us01._sequent__TOP__v__DOT__us01__12(vlSymsp);
    vlSymsp->TOP__v__DOT__us30._sequent__TOP__v__DOT__us30__13(vlSymsp);
    vlSymsp->TOP__v__DOT__us20._sequent__TOP__v__DOT__us20__14(vlSymsp);
    vlSymsp->TOP__v__DOT__us10._sequent__TOP__v__DOT__us10__15(vlSymsp);
    vlSymsp->TOP__v__DOT__us00._sequent__TOP__v__DOT__us00__16(vlSymsp);
    vlTOPp->_settle__TOP__2(vlSymsp);
    vlSymsp->TOP__v__DOT__u0__DOT__u0._settle__TOP__v__DOT__u0__DOT__u0__33(vlSymsp);
    vlSymsp->TOP__v__DOT__u0__DOT__u1._settle__TOP__v__DOT__u0__DOT__u1__34(vlSymsp);
    vlSymsp->TOP__v__DOT__u0__DOT__u2._settle__TOP__v__DOT__u0__DOT__u2__35(vlSymsp);
    vlSymsp->TOP__v__DOT__u0__DOT__u3._settle__TOP__v__DOT__u0__DOT__u3__36(vlSymsp);
    vlTOPp->_sequent__TOP__4(vlSymsp);
}

IData Vaes_cipher_top::_change_request(Vaes_cipher_top__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vaes_cipher_top::_change_request\n"); );
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    IData __req = false;  // Logically a bool
    return __req;
}

// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_vcd_c.h"
#include "Vaes_cipher_top__Syms.h"


//======================

void Vaes_cipher_top::traceChg(VerilatedVcd* vcdp, void* userthis, uint32_t code) {
    // Callback from vcd->dump()
    Vaes_cipher_top* t=(Vaes_cipher_top*)userthis;
    Vaes_cipher_top__Syms* __restrict vlSymsp = t->__VlSymsp; // Setup global symbol table
    if (vlSymsp->getClearActivity()) {
	t->traceChgThis (vlSymsp, vcdp, code);
    }
}

//======================


void Vaes_cipher_top::traceChgThis(Vaes_cipher_top__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	if (VL_UNLIKELY((1 & (vlTOPp->__Vm_traceActivity 
			      | (vlTOPp->__Vm_traceActivity 
				 >> 1))))) {
	    vlTOPp->traceChgThis__2(vlSymsp, vcdp, code);
	}
	if (VL_UNLIKELY((2 & vlTOPp->__Vm_traceActivity))) {
	    vlTOPp->traceChgThis__3(vlSymsp, vcdp, code);
	}
	vlTOPp->traceChgThis__4(vlSymsp, vcdp, code);
    }
    // Final
    vlTOPp->__Vm_traceActivity = 0;
}

void Vaes_cipher_top::traceChgThis__2(Vaes_cipher_top__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vcdp->chgBus  (c+1,(vlTOPp->v__DOT__w0),32);
	vcdp->chgBus  (c+2,(vlTOPp->v__DOT__w1),32);
	vcdp->chgBus  (c+3,(vlTOPp->v__DOT__w2),32);
	vcdp->chgBus  (c+4,(vlTOPp->v__DOT__w3),32);
	vcdp->chgBus  (c+5,((0xff & ((IData)(vlTOPp->v__DOT__sa00_mc) 
				     ^ (vlTOPp->v__DOT__w0 
					>> 0x18)))),8);
	vcdp->chgBus  (c+6,((0xff & ((IData)(vlTOPp->v__DOT__sa01_mc) 
				     ^ (vlTOPp->v__DOT__w1 
					>> 0x18)))),8);
	vcdp->chgBus  (c+7,((0xff & ((IData)(vlTOPp->v__DOT__sa02_mc) 
				     ^ (vlTOPp->v__DOT__w2 
					>> 0x18)))),8);
	vcdp->chgBus  (c+8,((0xff & ((IData)(vlTOPp->v__DOT__sa03_mc) 
				     ^ (vlTOPp->v__DOT__w3 
					>> 0x18)))),8);
	vcdp->chgBus  (c+9,((0xff & ((IData)(vlTOPp->v__DOT__sa10_mc) 
				     ^ (vlTOPp->v__DOT__w0 
					>> 0x10)))),8);
	vcdp->chgBus  (c+10,((0xff & ((IData)(vlTOPp->v__DOT__sa11_mc) 
				      ^ (vlTOPp->v__DOT__w1 
					 >> 0x10)))),8);
	vcdp->chgBus  (c+11,((0xff & ((IData)(vlTOPp->v__DOT__sa12_mc) 
				      ^ (vlTOPp->v__DOT__w2 
					 >> 0x10)))),8);
	vcdp->chgBus  (c+12,((0xff & ((IData)(vlTOPp->v__DOT__sa13_mc) 
				      ^ (vlTOPp->v__DOT__w3 
					 >> 0x10)))),8);
	vcdp->chgBus  (c+13,((0xff & ((IData)(vlTOPp->v__DOT__sa20_mc) 
				      ^ (vlTOPp->v__DOT__w0 
					 >> 8)))),8);
	vcdp->chgBus  (c+14,((0xff & ((IData)(vlTOPp->v__DOT__sa21_mc) 
				      ^ (vlTOPp->v__DOT__w1 
					 >> 8)))),8);
	vcdp->chgBus  (c+15,((0xff & ((IData)(vlTOPp->v__DOT__sa22_mc) 
				      ^ (vlTOPp->v__DOT__w2 
					 >> 8)))),8);
	vcdp->chgBus  (c+16,((0xff & ((IData)(vlTOPp->v__DOT__sa23_mc) 
				      ^ (vlTOPp->v__DOT__w3 
					 >> 8)))),8);
	vcdp->chgBus  (c+17,((0xff & ((IData)(vlTOPp->v__DOT__sa30_mc) 
				      ^ vlTOPp->v__DOT__w0))),8);
	vcdp->chgBus  (c+18,((0xff & ((IData)(vlTOPp->v__DOT__sa31_mc) 
				      ^ vlTOPp->v__DOT__w1))),8);
	vcdp->chgBus  (c+19,((0xff & ((IData)(vlTOPp->v__DOT__sa32_mc) 
				      ^ vlTOPp->v__DOT__w2))),8);
	vcdp->chgBus  (c+20,((0xff & ((IData)(vlTOPp->v__DOT__sa33_mc) 
				      ^ vlTOPp->v__DOT__w3))),8);
	vcdp->chgBus  (c+21,(vlSymsp->TOP__v__DOT__us00.d),8);
	vcdp->chgBus  (c+22,(vlSymsp->TOP__v__DOT__us01.d),8);
	vcdp->chgBus  (c+23,(vlSymsp->TOP__v__DOT__us02.d),8);
	vcdp->chgBus  (c+24,(vlSymsp->TOP__v__DOT__us03.d),8);
	vcdp->chgBus  (c+25,(vlSymsp->TOP__v__DOT__us10.d),8);
	vcdp->chgBus  (c+26,(vlSymsp->TOP__v__DOT__us11.d),8);
	vcdp->chgBus  (c+27,(vlSymsp->TOP__v__DOT__us12.d),8);
	vcdp->chgBus  (c+28,(vlSymsp->TOP__v__DOT__us13.d),8);
	vcdp->chgBus  (c+29,(vlSymsp->TOP__v__DOT__us20.d),8);
	vcdp->chgBus  (c+30,(vlSymsp->TOP__v__DOT__us21.d),8);
	vcdp->chgBus  (c+31,(vlSymsp->TOP__v__DOT__us22.d),8);
	vcdp->chgBus  (c+32,(vlSymsp->TOP__v__DOT__us23.d),8);
	vcdp->chgBus  (c+33,(vlSymsp->TOP__v__DOT__us30.d),8);
	vcdp->chgBus  (c+34,(vlSymsp->TOP__v__DOT__us31.d),8);
	vcdp->chgBus  (c+35,(vlSymsp->TOP__v__DOT__us32.d),8);
	vcdp->chgBus  (c+36,(vlSymsp->TOP__v__DOT__us33.d),8);
	vcdp->chgBus  (c+37,(vlTOPp->v__DOT__sa00_mc),8);
	vcdp->chgBus  (c+38,(vlTOPp->v__DOT__sa01_mc),8);
	vcdp->chgBus  (c+39,(vlTOPp->v__DOT__sa02_mc),8);
	vcdp->chgBus  (c+40,(vlTOPp->v__DOT__sa03_mc),8);
	vcdp->chgBus  (c+41,(vlTOPp->v__DOT__sa10_mc),8);
	vcdp->chgBus  (c+42,(vlTOPp->v__DOT__sa11_mc),8);
	vcdp->chgBus  (c+43,(vlTOPp->v__DOT__sa12_mc),8);
	vcdp->chgBus  (c+44,(vlTOPp->v__DOT__sa13_mc),8);
	vcdp->chgBus  (c+45,(vlTOPp->v__DOT__sa20_mc),8);
	vcdp->chgBus  (c+46,(vlTOPp->v__DOT__sa21_mc),8);
	vcdp->chgBus  (c+47,(vlTOPp->v__DOT__sa22_mc),8);
	vcdp->chgBus  (c+48,(vlTOPp->v__DOT__sa23_mc),8);
	vcdp->chgBus  (c+49,(vlTOPp->v__DOT__sa30_mc),8);
	vcdp->chgBus  (c+50,(vlTOPp->v__DOT__sa31_mc),8);
	vcdp->chgBus  (c+51,(vlTOPp->v__DOT__sa32_mc),8);
	vcdp->chgBus  (c+52,(vlTOPp->v__DOT__sa33_mc),8);
	vcdp->chgBus  (c+53,(vlTOPp->v__DOT__u0__DOT__tmp_w),32);
	vcdp->chgBus  (c+54,(vlTOPp->v__DOT__u0__DOT__subword),32);
    }
}

void Vaes_cipher_top::traceChgThis__3(Vaes_cipher_top__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vcdp->chgArray(c+55,(vlTOPp->v__DOT__text_in_r),128);
	vcdp->chgBus  (c+59,(vlTOPp->v__DOT__sa00),8);
	vcdp->chgBus  (c+60,(vlTOPp->v__DOT__sa01),8);
	vcdp->chgBus  (c+61,(vlTOPp->v__DOT__sa02),8);
	vcdp->chgBus  (c+62,(vlTOPp->v__DOT__sa03),8);
	vcdp->chgBus  (c+63,(vlTOPp->v__DOT__sa10),8);
	vcdp->chgBus  (c+64,(vlTOPp->v__DOT__sa11),8);
	vcdp->chgBus  (c+65,(vlTOPp->v__DOT__sa12),8);
	vcdp->chgBus  (c+66,(vlTOPp->v__DOT__sa13),8);
	vcdp->chgBus  (c+67,(vlTOPp->v__DOT__sa20),8);
	vcdp->chgBus  (c+68,(vlTOPp->v__DOT__sa21),8);
	vcdp->chgBus  (c+69,(vlTOPp->v__DOT__sa22),8);
	vcdp->chgBus  (c+70,(vlTOPp->v__DOT__sa23),8);
	vcdp->chgBus  (c+71,(vlTOPp->v__DOT__sa30),8);
	vcdp->chgBus  (c+72,(vlTOPp->v__DOT__sa31),8);
	vcdp->chgBus  (c+73,(vlTOPp->v__DOT__sa32),8);
	vcdp->chgBus  (c+74,(vlTOPp->v__DOT__sa33),8);
	vcdp->chgBit  (c+75,(vlTOPp->v__DOT__ld_r));
	vcdp->chgBus  (c+76,(vlTOPp->v__DOT__dcnt),4);
	vcdp->chgBus  (c+77,(vlTOPp->v__DOT__u0__DOT__w[0]),32);
	vcdp->chgBus  (c+78,(vlTOPp->v__DOT__u0__DOT__w[1]),32);
	vcdp->chgBus  (c+79,(vlTOPp->v__DOT__u0__DOT__w[2]),32);
	vcdp->chgBus  (c+80,(vlTOPp->v__DOT__u0__DOT__w[3]),32);
	vcdp->chgBus  (c+81,(vlTOPp->v__DOT__u0__DOT__rcon),32);
    }
}

void Vaes_cipher_top::traceChgThis__4(Vaes_cipher_top__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vaes_cipher_top* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vcdp->chgBit  (c+83,(vlTOPp->rst));
	vcdp->chgBit  (c+85,(vlTOPp->done));
	vcdp->chgArray(c+90,(vlTOPp->text_in),128);
	vcdp->chgArray(c+94,(vlTOPp->text_out),128);
	vcdp->chgBit  (c+82,(vlTOPp->clk));
	vcdp->chgBit  (c+84,(vlTOPp->ld));
	vcdp->chgArray(c+86,(vlTOPp->key),128);
    }
}

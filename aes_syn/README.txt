--exe
Generate an executable. You will also need to pass additional .cpp files on
the command line that implement the main loop for your simulation.

--cc
Specifies C++ without SystemC output mode; see also --sc and --sp.

verilator -Wall --cc rca128_rtl.v --exe sim_main.cpp


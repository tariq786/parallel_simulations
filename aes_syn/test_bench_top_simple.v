/////////////////////////////////////////////////////////////////////
////                                                             ////
////  	 Test Bench  AES128-ENC           ////
////                           ////
////                                                             ////
////  Author: Tariq Bashir Ahmad		                 ////
////          tariq.bashir@gmail.com                             ////
////     	                                                 ////
////                                                             ////
/////////////////////////////////////////////////////////////////////
////                                                             ////
//// Copyright (C) 2010 	 Tariq Bashir Ahmad 		 ////	
////                         http://www.ecs.umass.edu/~tbashir   ////
////                                                		 ////
////                                                             ////
//// This source file may be used and distributed without        ////
//// restriction provided that this copyright statement is not   ////
//// removed from the file and that any derivative work contains ////
//// the original copyright notice and the associated disclaimer.////
////                                                             ////
////     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ////
//// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ////
//// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ////
//// FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ////
//// OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ////
//// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ////
//// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ////
//// GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ////
//// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ////
//// LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ////
//// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ////
//// OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ////
//// POSSIBILITY OF SUCH DAMAGE.                                 ////
////                                                             ////
/////////////////////////////////////////////////////////////////////


`timescale 1 ns/1 ps



module test;

  reg		clk;
  reg		rst;
  reg		kld;
  reg [127:0]   key; 
  reg [127:0]   text_in;
  wire [127:0]  text_out;
  wire		done;

  wire [127:0]  text_out_c;

  initial
    begin
      
      clk <=  0;
      kld = 0;
      repeat(1) @(posedge clk);
      rst =  1;
      repeat(5) @(posedge clk);
      rst =  0;
      
      repeat(1) @(posedge clk);
      kld 	      =  1'b1;                   
      key 	      =    128'h99887766554433221100ffeeddccbbaa;
      text_in         =    128'h00112233445566778899aabbccddeeff;
      repeat(2) @(posedge clk);  
      kld             =  1'b0;


      //pushing second plain text in the pipeline
      repeat(11) @(posedge clk);  //note the 11 clock cycle gap
      kld             =  1;
      key 	      =    128'h00112233445566778899aabbccddeeff;
      text_in         =    128'h0;
      repeat(1) @(posedge clk);
      kld             =  0;

    end


  //DUT AES
  aes_cipher_top uut
    (
     .clk(		clk		),
     .rst(		rst		),
     .ld(		kld		),
     .done(		done		),
     .key(		key		),
     .text_in(	text_in		),
     .text_out(	text_out	)
     );
  
  


  initial
    forever #5 clk = ~clk;

  initial
    #5000 $finish;
 
 
  initial
    $monitor($time,": PT=%h, Key=%h, CT=%h, DONE is %b\n",text_in,key,text_out,done);

/*  initial
    $vcdpluson;
*/
/*
   initial
      $print_hierarchy(test.uut.u0);  //printing hierarchy of key expand only
*/
endmodule
// Local Variables:
// verilog-library-directories:("." "../")
// End:


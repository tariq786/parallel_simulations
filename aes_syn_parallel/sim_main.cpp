/*
 * =====================================================================================
 *
 *       Filename:  sim_main.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  11/10/2013 08:33:34 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Tariq Bashir Ahmad, 
 *   Organization:  
 *
 * =====================================================================================
 */



#include <omp.h>
#include "Vaes_cipher_top_syn.h"
#include "verilated.h"
#include "verilated_vcd_c.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


  
  //#pragma omp threadprivate(top)

vluint64_t main_time = 0;       // Current simulation time
 // This is a 64-bit integer to reduce wrap over issues and
 // allow modulus.  You can also use a double, if you wish.

  double sc_time_stamp ()
{       // Called by $time in Verilog
       return main_time;           // converts to double, to match
                                              // what SystemC does
 }



int main(int argc, char **argv, char **env) 
{
      Verilated::commandArgs(argc, argv);
      srand(time(NULL));
      
	    
          unsigned int set_done = 0;
		  unsigned int i = 0;
		  unsigned int ld_set = 0;
		  
	#ifdef OMP
      #pragma omp parallel default(none) firstprivate(i,set_done,ld_set,main_time)
      {
           Vaes_cipher_top_syn* top = new Vaes_cipher_top_syn;  // this is the aes object that will do the enc
           top->rst = 1;           // assert reset
           double tstart, tend;
           tstart = omp_get_wtime();
  	#endif
		
	    while (i < 2*(65000*100))		//2*(65000*100)
	    {
    	  if (main_time > 10)
	    	  {
                  top->rst = 0;   // Deassert reset
	    	  }
              if ((main_time % 10) == 1)
              {
                  top->clk = 1;       // Toggle clock (posedge)
              }
              if ((main_time % 10) == 6)
              {
                  
                  top->clk = 0;
                  //setting DUT values
                  
			  if(ld_set!=1 && main_time > 10)
			   {
				  top -> ld = 1;
				  
				  //unsigned int rand_state = time(NULL) + 1337*omp_get_thread_num();
				  //unsigned int rnd[4];	  
				  //rnd[0] = rand_r(&rand_state);
				  //rnd[1] = rand_r(&rand_state);
				  //rnd[2] = rand_r(&rand_state);
				  //rnd[3] = rand_r(&rand_state);
				  top -> key     =   {rand(),rand(),rand(),rand()}; // {0x00000000,0x000000000,0x00000000,0x00000000};
				  top -> text_in =   {rand(),rand(),rand(),rand()}; //{0x00000000,0x00000000,0x00000000,0x00000000};  
				  //top -> key     =   {0x00000000,0x00000000,0x00000000,0x00000000};
				  //top -> text_in =   {0x00000000,0x00000000,0x00000000,0x00000000};
				  ld_set++;
				}
				else if(ld_set == 1 && main_time > 10)
				{
				top -> ld = 0;
				set_done = 0;
				}	
		
                } //(main_time % 10) == 6)
              
                top->eval();            // Evaluate model
				
                if(top->done == 1 && set_done == 0)
                {
                	if(i == 2*(65000*100)-1)
					{
						tend = omp_get_wtime() -tstart;
						printf("Encryption took %.6f seconds\n", tend);
					}
                	/*
				#ifdef OMP
	               printf("Time=%2d, key=%2x%2x%2x%2x,text_in=%2x%2x%2x%2x,text_out=%2x%2x%2x%2x on %2d of %2d\n",   \
	            		   main_time,top->key[3],top->key[2],top->key[1],top->key[0],								\
	            		   top->text_in[3],top->text_in[2],top->text_in[1],top->text_in[0], 				\
	            		   top->text_out[3],top->text_out[2],top->text_out[1],top->text_out[0],top->done, \
	            		   omp_get_thread_num(),omp_get_num_threads()
	            		 );
				#else
	               printf("Time=%2d, key=%2x%2x%2x%2x,text_in=%2x%2x%2x%2x,text_out=%2x%2x%2x%2x\n" ,   \
						   main_time,top->key[3],top->key[2],top->key[1],top->key[0],								\
						   top->text_in[3],top->text_in[2],top->text_in[1],top->text_in[0], 				\
						   top->text_out[3],top->text_out[2],top->text_out[1],top->text_out[0],top->done
	            	     );
				#endif
                   */
				  ld_set = 0; //reset the value	
                    i++;
                  // printf("i=%2d\n",i);
                   set_done  = 1;
                } //if(top->done)
             
                main_time++;
                
          } //end of while

	  
	    top->final();               // Done simulating
	    delete top;
      } //pragma omp
      
	    printf("\n Test Done\n");
         
	    
	return 0;
} //end of main
